import axios from 'axios';

const apiRoot = process.env.API_ROOT;
const version = process.env.API_VERSION;

const api = axios.create({ baseURL: `${apiRoot}/${version}` });

export const headersConfigNoAuth = {
  headers: {
    'Content-Type': 'application/json',
  }
};
export const headersConfigAuth = jwt => (
  {
    headers: {
      'Authorization': `Bearer ${jwt}`,
      'Content-Type': 'application/json',
    }
  }
);

const facade = {};
facade.request = config => api.request(config);
["get", "head"].forEach(method => {
  facade[method] = (url, config) => facade.request({ ...config, method, url });
});
["delete", "post", "put", "patch"].forEach(method => {
  facade[method] = (url, data, config) =>
    facade.request({ ...config, method, url, data });
});

export { facade }