import { facade, headersConfigNoAuth, headersConfigAuth } from '../facade';

const loginPath = "/auth/login/";
const signUpPath = "/auth/registration/"
const facebookPath = "/auth/facebook/";
const twitterPath = "/auth/twitter/"
const selfPath = "/profile/";
const profilePath = "/profile/public/"
const myStatsForCurrentVideoPath = (videoId) => (
  `/videos_analytic/${videoId}/stats/`
)
const notificationsPath = (username) => (
  `/history/?af_user=${username}&page_size=10&page=1`
);
const clearNotificationsPath = "/history/read_notifications/";
const subscribePath = "/subscribe/"
const blockPath = "/block/";
const mutePath = (video_id) => (
  `videos_mute_user/${video_id}/mute/`
)
const likePath = (video_id) => (
  `/videos_action/${video_id}/like/`
)
const favouritePath = (video_id) => (
  `/videos_action/${video_id}/favourite/`
)
const flagPath = (video_id) => (
  `/videos_action/${video_id}/flag/`
)
const signaturePath = "/signature/"
const verifyEmailPath = "/auth/registration/verify_email/"

class userApi {  
  static emailLogin(email, password) {
    return facade.post(loginPath, { email, password }, headersConfigNoAuth);
  }

  static facebookLogin(token) {
    return facade.post(facebookPath, {access_token: token}, headersConfigNoAuth)
  }

  static twitterLogin(data) {
    return facade.post(twitterPath, {...data}, headersConfigNoAuth)
  }

  static signUp(username, email, password1, password2) {
    return facade.post(signUpPath, { username, email, password1, password2 }, headersConfigNoAuth);
  }

  static getSelf(jwt) {
    return facade.get(selfPath, headersConfigAuth(jwt));
  }

  static getPublicProfile(username, jwt) {
    return facade.post(profilePath, {username}, jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }

  static getCurrentVideoSelfStats(videoId, jwt) {
    return facade.get(myStatsForCurrentVideoPath(videoId), headersConfigAuth(jwt));
  }

  static getNotifications(username, jwt) {
    return facade.get(notificationsPath(username), headersConfigAuth(jwt))
  }

  static clearNotifications(jwt) {
    return facade.post(clearNotificationsPath, {}, headersConfigAuth(jwt))
  }

  static loadMoreNotifications(next, jwt) {
    return facade.get(next, headersConfigAuth(jwt))
  }

  static mute(video_id, user_id, jwt) {
    return facade.post(mutePath(video_id), {user_id}, headersConfigAuth(jwt))
  }

  static unmute(video_id, user_id, jwt) {
    return facade.delete(mutePath(video_id), {user_id}, headersConfigAuth(jwt))
  }

  static getMutedList(video_id, jwt) {
    return facade.get(mutePath(video_id), headersConfigAuth(jwt))
  }

  static block(user_id, jwt) {
    return facade.post(blockPath, {user_id}, headersConfigAuth(jwt))
  }

  static unblock(user_id, jwt) {
    return facade.delete(blockPath, {user_id}, headersConfigAuth(jwt))
  }

  static getBlockedList(jwt) {
    return facade.get(blockPath, headersConfigAuth(jwt))
  }

  static subscribe(user_id, jwt) {
    return facade.post(subscribePath, {user_id}, headersConfigAuth(jwt))
  }

  static unsubscribe(user_id, jwt) {
    return facade.delete(subscribePath, {user_id}, headersConfigAuth(jwt))
  }

  static like(video_id, timestamp, jwt) {
    return facade.post(likePath(video_id), {timestamp}, headersConfigAuth(jwt))
  }

  static unlike(video_id, timestamp, jwt) {
    return facade.delete(likePath(video_id), {timestamp}, headersConfigAuth(jwt))
  }

  static favourite(video_id, timestamp, jwt) {
    return facade.post(favouritePath(video_id), {timestamp}, headersConfigAuth(jwt))
  }

  static unfavourite(video_id, timestamp, jwt) {
    return facade.delete(favouritePath(video_id), {timestamp}, headersConfigAuth(jwt))
  }

  static flag(video_id, jwt) {
    return facade.post(flagPath(video_id), {}, headersConfigAuth(jwt))
  }

  static unflag(video_id, jwt) {
    return facade.delete(flagPath(video_id), {}, headersConfigAuth(jwt))
  }

  static getAWSSignature(jwt) {
    return facade.get(signaturePath, headersConfigAuth(jwt))
  }

  static editProfile(newData, jwt) {
    let data;
    if (newData.username || newData.name_display || newData.photo) {
      data = {
        ...newData,
      };
    } else {
      data = newData;
    }
    return facade.patch(selfPath, data, headersConfigAuth(jwt))
  }

  static verifyEmail(key) {
    return facade.post(verifyEmailPath, {key}, headersConfigNoAuth)
  }

}

export default userApi;
