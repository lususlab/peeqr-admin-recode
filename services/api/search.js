import { facade, headersConfigNoAuth } from '../facade';

const searchDropdownPath = (search, size) => (
  `/videos/multisearch/?search=${ search }&size=${ size }`
);

class videoApi {  
  static getSearchDropdown(search, size) {
    return facade.get(searchDropdownPath(search, size), headersConfigNoAuth);
  }
  static getSearchResults(search, size) {
    return facade.get(searchDropdownPath(search, size), headersConfigNoAuth);
  }
}

export default videoApi;