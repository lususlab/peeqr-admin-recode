import { facade, headersConfigNoAuth, headersConfigAuth } from '../facade';

const bannerPath = "/banners";
const collectionsPath = "/collection/"
const allVideosPath = (page, size) => (
  `/videos/?page_size=${size}&page=${page}`
)
const singleVideoPath = (videoId) => (
  `/videos/${videoId}/`
)
const categoryVideosPath = (categoryId, page, size) => (
  `/videos/?category_id=${categoryId}&page_size=${size}&page=${page}`
)
const categoriesPath = "/categories/";
const staffPicksPath = "/videos/staffpicks/";
const discoverPath = (page, size) => (
  `/videos/discover/?page=${page}&section_size=${size}`
);

const relatedPath = (videoId, page, size) => (
  `/videos/${videoId}/related/?page_size=${size}&page=${page}`
)

const commentsPath = (videoId, page, size) => (
  `/videos_comment/${videoId}/comments/?page=${page}&page_size=${size}`
)

const postCommentPath = (videoId) => (
  `/videos_comment/${videoId}/comments/`
)

const viewPath = videoId => (
  `/videos_view/${videoId}/view/`
)

const agoraKeyPath = "/agora_dynamic_key/"

const currentViewersPath = (videoId, page, size) => (
  `/videos_analytic/${videoId}/viewer_photo/?page=${page}&page_size=${size}`
)

const annotationsPath = videoId => (
  `/videos_annotation/${videoId}/annotations/`
)

const paidContentPath = videoId => (
  `/videos_play_url/${videoId}/signed_play_url/`
)

const videosByUserPath = (username, page, size) => (
  `/videos/?user=${username}&page=${page}&page_size=${size}`
)

class videoApi {  
  static getBanners() {
    return facade.get(bannerPath, headersConfigNoAuth);
  }
  static getCollections() {
    return facade.get(collectionsPath, headersConfigNoAuth);
  }
  static getAllVideos(page, size, jwt) { // Fresh Videos
    return facade.get(allVideosPath(page, size), jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }
  static getSingleVideo(videoId, jwt) {
    return facade.get(singleVideoPath(videoId), jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }
  static deleteSingleVideo(videoId, jwt) {
    return facade.delete(singleVideoPath(videoId), {}, headersConfigAuth(jwt));
  }
  static getCategories() {
    return facade.get(categoriesPath, headersConfigNoAuth);
  }
  static getStaffPicks(jwt) {
    return facade.get(staffPicksPath, jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }
  static getDiscover(page, size) {
    return facade.get(discoverPath(page, size), headersConfigNoAuth);
  }
  static loadMoreVideos(url, jwt) {
    return facade.get(url, jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }
  static getCategoryVideos(categoryId, page, size, jwt) {
    return facade.get(categoryVideosPath(categoryId, page, size), jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }
  static getRelatedVideos(videoId, page, size, lat, long, jwt) {
    return facade.post(relatedPath(videoId, page, size), {lat, long}, jwt ? headersConfigAuth(jwt) : headersConfigNoAuth);
  }
  static getComments(videoId, page, size) {
    return facade.get(commentsPath(videoId, page, size), headersConfigNoAuth);
  }
  static postComment(videoId, userComment, timestamp, jwt) {
    return facade.post(postCommentPath(videoId), {text: userComment, duration: timestamp}, headersConfigAuth(jwt));
  }
  static loadMoreComments(url) {
    return facade.get(url, headersConfigNoAuth);
  }
  static startView(videoId, timestamp, live, jwt) {
    if (jwt) {
      return facade.post(viewPath(videoId), {duration: timestamp, is_live: live}, headersConfigAuth(jwt))
    }
    return facade.post(viewPath(videoId), {duration: timestamp, is_live: live}, headersConfigNoAuth)
  }
  static endView(videoId, jwt) {
    if (jwt) {
      return facade.delete(viewPath(videoId), {}, headersConfigAuth(jwt))
    }
    return facade.delete(viewPath(videoId), {}, headersConfigNoAuth)
  }
  static getAgoraKey(videoId) {
    return facade.post(agoraKeyPath, {
      'channel_name': `${videoId}`,
      'device_type': 'web'
    }, headersConfigNoAuth)
  }
  static getCurrentViewers(videoId, page, size, jwt) {
    return facade.get(currentViewersPath(videoId, page, size), headersConfigAuth(jwt));
  }
  static getAnnotations(videoId) {
    return facade.get(annotationsPath(videoId), headersConfigNoAuth);
  }
  static getPaidContent(videoId, jwt) {
    return facade.get(paidContentPath(videoId), headersConfigAuth(jwt));
  }
  static getVideosByUser(username, page, size) {
    return facade.get(videosByUserPath(username, page, size), headersConfigNoAuth);
  }
  
}

export default videoApi;