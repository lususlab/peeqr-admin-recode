import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Head from "next/head";
import { Router } from "routes";
import { 
  redirectIfNotAuthenticated 
} from "helpers/session";

import {logOutRequest} from "store/user/actions";

import FullScreenLoader from "components/FullScreenLoader";
import { getJwt } from 'helpers/session';

import {
  LOGGING_YOU_OUT,
  WE_HATE_TO_SEE_YOU_GO,
  COME_BACK_SOON
} from "helpers/text";

class Signout extends Component {
  static async getInitialProps(ctx) {
    const jwt = await getJwt(ctx);
    const me = await ctx.store.getState().getIn(['user', 'me'])
    
    if (me.size === 0) {
      redirectIfNotAuthenticated(ctx)
    }

    return {jwt}
  }
  constructor() {
    super();
    this.state = {
      selectedText: '',
      run: false,
      text: [
        WE_HATE_TO_SEE_YOU_GO,
        COME_BACK_SOON,
      ]
    }
  }

  componentDidMount() {
    const { me, jwt } = this.props;
    const { text } = this.state;
    this.setState({selectedText: text[Math.floor(Math.random() * text.length)]});
    setTimeout(() => this.setState({ run: true }), 2000);
    
  }

  async componentDidUpdate(prevProps, prevState) {
    const { me, logOutRequest } = this.props;
    const { run } = this.state;
    
    if (run && prevState.run !== run) {
      await logOutRequest();
    }

    if (me.size === 0 && prevProps.me !== me) {
      Router.pushRoute("/")
    }
  }
  render() {
    const {selectedText} = this.state;
    return (
      <div>
        <Head>
          <title>{LOGGING_YOU_OUT}</title>
        </Head>
        <FullScreenLoader>
          <h2>{LOGGING_YOU_OUT}</h2>
          <p>{selectedText}</p>
        </FullScreenLoader>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  me: state.getIn(['user', 'me'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      logOutRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Signout)
