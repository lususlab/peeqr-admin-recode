import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Sticky from 'react-stickynode';
import { faLemon } from '@fortawesome/free-regular-svg-icons';

import {
  loadUpHomePageRequest,  
} from "store/pages/actions"

import {
  loadMoreFreshVideosRequest
} from "store/video/actions"

import Layout from "components/Layout";
import CarouselParent from "components/CarouselParent";
import SideVideoList from "components/SideVideoList";
import StaffPicks from "components/StaffPicks";
import Collection from "components/Collection";
import Footer from "components/Footer";
import Spinner from "components/Spinner";

import {
  FRESH
} from "helpers/text";

import { getJwt } from 'helpers/session'

class Index extends Component {
  static async getInitialProps(ctx) {

    const { isServer, store } = ctx;
    
    const jwt = await getJwt(ctx);
    const page = 1;
    const size = 10;

    store.dispatch(loadUpHomePageRequest(page, size, jwt))

    return {jwt}
  }

  constructor() {
    super()

    this.loadMoreFreshVideos = this.loadMoreFreshVideos.bind(this)
  }

  loadMoreFreshVideos(next) {
    const { loadMoreFreshVideosRequest, jwt } = this.props;
    loadMoreFreshVideosRequest(next, jwt);
  }

  render() {
    const { 
      banners, 
      gettingBanners,
      collections, 
      gettingCollections,
      freshVideos,
      gettingFreshVideos,
      categories,
      gettingCategories,
      staffPicks, 
      gettingStaffPicks,
      jwt,
      loadingHomePage
    } = this.props;

    console.log("==collections==", collections.toJS())
    return (
      <Layout>
        {
          loadingHomePage ? (
            <Spinner type="relative" color="grey" />
          ) : (
            <div>
              {
                gettingBanners ? (
                  <Spinner type="relative" color="grey" />
                ) : (
                  <CarouselParent banners={banners} />
                )
              }
              {
                gettingStaffPicks ? (
                  <Spinner type="relative" color="grey" />
                ) : (
                  <StaffPicks staffPicks={staffPicks} />
                )
              }
              <main className="home">
                <div className="left-side">
                {
                  gettingFreshVideos ? (
                    <Spinner type="relative" color="grey" />
                  ) : (
                    <Sticky top={16}>
                      <SideVideoList 
                        heading={FRESH} 
                        iconColor="gold" 
                        icon={faLemon} 
                        videoList={freshVideos.get('results')} 
                        gettingVideos={gettingFreshVideos} 
                        loadMore={this.loadMoreFreshVideos}
                        next={freshVideos.get('next')}
                      />
                    </Sticky>
                  )
                }
                </div>
                <section className="main-side">
                  <div className="main-body">
                  {
                    gettingCollections ? (
                      <Spinner type="relative" color="grey" />
                    ) : (
                      collections.get('results').map(collection => {
                        if (collection.get('video').size > 0) {
                          return <Collection key={collection.get('id')} videoType="collection" collection={collection} jwt={jwt} />
                        }
                      })
                    )
                  }
                  {
                    gettingCategories ? (
                      <Spinner type="relative" color="grey" />
                    ) : (
                      categories.get('results').map(category => (
                        <Collection key={category.get('id')} videoType="category" collection={category} jwt={jwt} />
                      ))
                    )
                  }
                  </div>
                  <Footer />
                </section>
              </main>
            </div>
          )
        }
          
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  loadingHomePage: state.getIn(['page', 'loadingHomePage']),
  gettingBanners: state.getIn(['video', 'gettingBanners']),
  banners: state.getIn(['video', 'banners']),
  gettingCollections: state.getIn(['video', 'gettingCollections']),
  collections: state.getIn(['video', 'collections']),
  gettingFreshVideos: state.getIn(['video', 'gettingFreshVideos']),
  freshVideos: state.getIn(['video', 'freshVideos']),
  gettingCategories: state.getIn(['video', 'gettingCategories']),
  categories: state.getIn(['video', 'categories']),
  gettingStaffPicks: state.getIn(['video', 'gettingStaffPicks']),
  staffPicks: state.getIn(['video', 'staffPicks']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadMoreFreshVideosRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Index)