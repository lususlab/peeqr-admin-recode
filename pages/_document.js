import React from "react";
import Document, { Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";
import renderHTML from "react-render-html";

import stylesheet from "static/css/master.scss";
import toastStyle from 'react-toastify/dist/ReactToastify.css';
import playerStyle from 'video-react/dist/video-react.css';

export default class MyDocument extends Document {

  static getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const page = ctx.renderPage(App => props =>
      sheet.collectStyles(<App {...props} />)
    );
    
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags};
  }

  render() {
    const { styleTags } = this.props;
    return (
      <html lang="en">
        <Head>
          <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
          {/* TODO: Get canonical link */}
          {/* <link rel="canonical" href={`${query}${pathname}`} /> */}
          <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
          <meta name="HandheldFriendly" content="true" />
          {styleTags}
          <style dangerouslySetInnerHTML={{ __html: renderHTML(stylesheet) }} />
          <style dangerouslySetInnerHTML={{ __html: renderHTML(toastStyle) }} />
          <style dangerouslySetInnerHTML={{ __html: renderHTML(playerStyle) }} />
          <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500" rel="stylesheet" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
