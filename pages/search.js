import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { 
  redirectIfNotAuthenticated 
} from "helpers/session";

import Layout from "components/Layout";
import NotificationsList from "components/NotificationsList";

import { SEARCH } from 'helpers/text';
import redirect from "helpers/redirect";
import { getJwt, getMe } from 'helpers/session';

import {
} from 'store/user/actions'

import { Router } from "routes";

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }

  render() {
    
    return (
      <Layout {...this.props} metaTitle={SEARCH}>
        <div />
      </Layout>
    )
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Search)
