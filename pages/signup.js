import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Router } from 'routes';
import { 
  redirectIfAuthenticated,
  getJwt
} from "helpers/session";

import Layout from "components/Layout";
import SignUpForm from "components/SignUpForm";

import { SIGN_UP_WITH_PEEQR } from 'helpers/text';

class SignUp extends Component {
  static async getInitialProps(ctx) {
    const jwt = await getJwt(ctx);
    const me = await ctx.store.getState().getIn(['user', 'me'])

    if (me.size > 0) {
      redirectIfAuthenticated(ctx)
    }

    return {jwt}
  }
  componentDidUpdate(prevProps) {
    const { me } = this.props;
    if (me && prevProps.me.size !== me.size > 0) {
      Router.back();
    }
  }

  render() {
    const {jwt} = this.props;
    return (
      <Layout metaTitle={SIGN_UP_WITH_PEEQR}>
        <section id="sign-up" className="modal-type">
          <div className="box small">
            <div className="box-content">
              <SignUpForm />
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}
const mapStateToProps = state => ({
  me: state.getIn(['user', 'me']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
