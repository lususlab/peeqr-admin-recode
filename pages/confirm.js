import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Link } from "routes";
import { 
  redirectIfAuthenticated 
} from "helpers/session";

import FullScreenLoader from "components/FullScreenLoader";
import Spinner from "components/Spinner";
import Button from "components/Button";

import { verifyEmailRequest } from "store/user/actions"

import {
  EMAIL_VERIFIED,
  BACK_TO_HOME,
} from 'helpers/text';

import {
  EMAIL_ERROR,
  EMAIL_ALT,
} from 'helpers/errors'

class EmailConfirmation extends Component {
  static async getInitialProps(ctx) {
    const { query, store } = ctx;
    const me = await store.getState().getIn(['user', 'me'])
    
    if (me.size > 0) {
      redirectIfAuthenticated(ctx)
    }

    return {query}
  }

  componentDidMount() {
    const {query, verifyEmailRequest} = this.props;
    verifyEmailRequest(query.key)
  }
  
  render() {
    const {verificationStatus, verifyingEmail} = this.props;

    return (
      <div>
        <FullScreenLoader>
          {
            !verifyingEmail ? (
              verificationStatus === 'ok' ? (
                <div className="centered">
                  <h2>{EMAIL_VERIFIED}</h2>
                  <Button color="white" background="bgdarkpurple">
                    <Link route="/">
                      <a><span>{ BACK_TO_HOME }</span></a>
                    </Link>
                  </Button>
                </div>
              ) : (
                <div className="centered">
                  <h2>{EMAIL_ERROR}</h2>
                  <p>{EMAIL_ALT}</p>
                  <Button color="white" background="bgdarkpurple">
                    <Link route="/">
                      <a><span>{ BACK_TO_HOME }</span></a>
                    </Link>
                  </Button>
                </div>
              )
            ) : (
              <div className="centered">
                <Spinner type="relative" color="white"/>
              </div>
            )
          }
        </FullScreenLoader>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  me: state.getIn(['user', 'me']),
  verificationStatus: state.getIn(['user', 'verificationStatus']),
  verifyingEmail: state.getIn(['user', 'verifyingEmail']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      verifyEmailRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(EmailConfirmation)
