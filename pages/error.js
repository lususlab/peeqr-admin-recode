import React, { Component } from "react";
import Head from "next/head";

import FullScreenLoader from "components/FullScreenLoader"

import {
  NOT_FOUND,
} from "helpers/text";

class ErrorPage extends Component{
  static async getInitialProps(ctx) {
    if (error) { // define your app error logic here
      ctx.res.statusCode = 404;
    }

    return { error }
  }
  render() {
    const { statusCode, error } = this.props;

    console.log("error", error)
    return (
      <div>
        <Head>
          <title>{statusCode - NOT_FOUND}</title>
        </Head>
        <FullScreenLoader>
          <h2>{statusCode}</h2>
          <p>{NOT_FOUND}</p>
        </FullScreenLoader>
      </div>
    )
  }
} 

export default ErrorPage
