import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import PropTypes from "prop-types";
import dynamic from 'next/dynamic';
import Sticky from 'react-stickynode';
import io from 'socket.io-client';
import { Link } from 'routes';
import { detect } from 'detect-browser';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDna, faHeart as faHeartFilled } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import {
  loadUpVideoPageRequest,
} from "store/pages/actions";

import { 
  endViewRequest,
  getSingleVideoStatsRequest,
  getPaidContentRequest,
  showModal
} from "store/video/actions";

import Layout from "components/Layout";
import Footer from "components/Footer";
import SideVideoList from "components/SideVideoList";
import Avatar from "components/Avatar";
import MergeStatus from "components/MergeStatus";
import SubscribeButton from "components/SubscribeButton";
import ChannelInfo from "components/ChannelInfo";
import LikeButton from "components/LikeButton";
import FavouriteButton from "components/FavouriteButton";
import ViewCounter from "components/ViewCounter";
import GenericCounter from "components/GenericCounter";
import ShareButton from "components/ShareButton";
import FlagButton from "components/FlagButton";
import Comments from "components/Comments";
import ViewerList from "components/ViewerList";
import Spinner from "components/Spinner";
import VideoPlayer from "components/VideoPlayer";

import {
  RELATED,
  TOTAL_PEARLS,
} from "helpers/text";

import {
  BROWSER_NO_SUPPORT,
  ERROR_LOADING,
  ERROR_BLOCKED
} from 'helpers/errors'

import { getJwt, getCookie } from 'helpers/session'

import redirect from 'helpers/redirect'

import pearl from 'static/img/icon-pearl-black-small.png';
import errorImage from 'static/img/girl-spinner/spinner-6.png';

const SOCKET_ENDPOINT = process.env.SOCKET_ENDPOINT;

const LoadableAgoraProvider = dynamic(
  import("components/VideoPlayer/agoraProvider").then(m => {
    const AgoraProvider = m;
    AgoraProvider.__webpackChunkName = m.__webpackChunkName;
    return AgoraProvider;
  }),
  {
    ssr: false,
  }
)

const LoadableVideoPlayer = dynamic(
  import("components/VideoPlayer").then(m => {
    const VideoPlayer = m;
    VideoPlayer.__webpackChunkName = m.__webpackChunkName;
    return VideoPlayer;
  }),
  {
    ssr: false,
  }
)

class Video extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isSelf: props.me.get('id') === props.currentVideo.getIn(['user', 'id']),
      socket: io(SOCKET_ENDPOINT),
      socketConnected: false,
      isConverting: false,
      playerState: {},
    }

    this.agora = React.createRef()
    this.sendPlayerState = this.sendPlayerState.bind(this);
    this.videoUrl = this.videoUrl.bind(this)
  }

  static async getInitialProps(ctx) {
    const { store, isServer, query, req, res } = ctx;

    const { id } = query;
    const { statusCode } = res || {};
    const jwt = await getJwt(ctx);
    const lat = await getCookie('lat', req) || 0;
    const long = await getCookie('long', req) || 0;

    const viewerSize = 20;
    const size = 10;
    const page = 1;

    store.dispatch(loadUpVideoPageRequest(id, page, size, viewerSize, lat, long, jwt))

    if (!id || (statusCode && statusCode !== 200)) {
      redirect('error')
    }
    return {jwt, id, statusCode, viewerSize}
  }

  async componentDidMount() {
    const {currentVideo, isShow, hasAccess, getPaidContentRequest, showModal, jwt} = this.props;
    const {socket, isSelf} = this.state;

    this.joinSocketClient(socket, currentVideo.get('uuid'));
    
    socket.on('connect', () => {
      this.setState({ socket: socket, socketConnected: socket.connected })
    });

    if (    
      currentVideo.get('merge_status') === 1 || 
      currentVideo.get('merge_status') === 4 || 
      currentVideo.get('merge_status') === 5 || 
      currentVideo.get('merge_status') === 6 || 
      currentVideo.get('merge_status') === 7 || 
      currentVideo.get('merge_status') === 8
    ) {
      this.setState({ isConverting: true })
    }

    if (isShow && hasAccess && !currentVideo.get('live')) {
      await getPaidContentRequest(currentVideo.get('id'), jwt)
    } else if (isShow && !hasAccess) {
      await showModal(currentVideo, 'access')
    }
  }

  componentWillUnmount() {
    const { currentVideo, endViewRequest, jwt } = this.props;
    const { isSelf } = this.state;
    if (currentVideo.get('id') && !isSelf) {
      endViewRequest(currentVideo.get('id'), jwt);
    }
  }

  joinSocketClient(socket, uuid) {
    socket.emit('join', uuid);
  }

  sendPlayerState(playerState) {
    this.setState({ playerState });
  }

  runRefresh() {
    this.agora.current.checkAgora();
  }

  videoUrl() {
    const {isShow, hasAccess, currentVideo, paidContent} = this.props;

    if (isShow && hasAccess && !currentVideo.get('live')) {
      return paidContent.getIn(['results', 'signed_url'])
    } else if (isShow && !hasAccess) {
      return ''
    }
    return currentVideo.get('play_url')
  } 

  render() {
    const {
      id,
      agoraKey,
      relatedVideos,
      gettingRelatedVideos,
      currentVideo,
      gettingVideo,
      jwt,
      me,
      gettingVideoStats,
      currentVideoStats,
      gettingMyCurrentVideoStats,
      myCurrentVideoStats,
      gettingComments,
      comments,
      gettingAgoraKey,
      gettingCurrentViewers,
      currentViewers,
      viewerSize,
      loadingVideoPage,
      error,
      gettingPaidContent,
      isShow,
      hasAccess,
      currentUser, 
      loadingUser,
      router
    } = this.props;
    const {
      isSelf,
      userComment,
      socket,
      socketConnected,
      socketJoined,
      isConverting,
      playerState,
    } = this.state;

    const fullUrl = `${process.env.HOSTNAME}${router.asPath}`;
    return (
      <Layout
        metaTitle={currentVideo.get('title')}
        metaType="video.other"
        metaImage={currentVideo.get('thumbnail')}
        metaDescription={currentVideo.getIn(['user', 'name_display'])}
      >
        <main className="video">
          <div className="left-side">
            {
              gettingRelatedVideos ? (
                <Spinner type="relative" color="grey" />
              ) : (
                <Sticky top={16}>
                  <SideVideoList heading={RELATED} iconColor="purple" icon={faDna} videoList={relatedVideos} gettingVideos={gettingRelatedVideos}/>
                </Sticky>
              )
            }
          </div>
          <section className="main-side">
            <div id={`video-${id}`} className="video-body">
              <ul className="user-meta">
                <li>
                  {
                    gettingVideo ? (
                      <Spinner type="relative" color="grey" />
                    ) : (
                      currentVideo.get('user') ? (
                        <Link route="profile" params={{slug: currentVideo.getIn(['user', 'username'])}}>
                          <a target="_blank" title={currentVideo.getIn(['user', 'name_display'])}>
                            <Avatar size="medium" username={currentVideo.getIn(['user', 'name_display'])} photo={currentVideo.getIn(['user', 'photo'])} />
                            <span className="username" >{currentVideo.getIn(['user', 'name_display'])}</span>
                          </a>
                        </Link>
                      ) : null
                    )
                  }
                </li>
                {
                  !isSelf && !loadingUser ? (
                    <li>
                      <SubscribeButton 
                        userId={currentVideo.getIn(['user', 'id'])}
                        username={currentVideo.getIn(['user', 'username'])}
                        stat={currentUser.get('subscribe')} 
                        jwt={jwt}
                      />
                    </li>
                  ) : null
                }
                {
                  isShow ? (
                    <li>
                      <ChannelInfo 
                        item={currentVideo}
                      />
                    </li>
                  ) : null
                }
              </ul>
                {
                  !loadingVideoPage && !error ? (
                    currentVideo.get('orientation') === 0 ? (
                      <div className="video-wrapper portrait">
                        <LoadableAgoraProvider
                          ref={this.agora}
                          video={currentVideo}
                        >
                          {
                            gettingAgoraKey ? (
                              <Spinner type="relative" color="grey" />
                            ) : (
                              <LoadableVideoPlayer
                                socket={socket}
                                video={currentVideo}
                                isConverting={isConverting}
                                sendPlayerState={this.sendPlayerState}
                                isSelf={isSelf}
                                jwt={jwt}
                                videoUrl={() => this.videoUrl()}
                                refresh={() => this.runRefresh()}
                              />
                            )
                          }
                        </LoadableAgoraProvider>
                        {
                          gettingMyCurrentVideoStats || gettingVideoStats ? (
                            <Spinner type="relative" color="grey" />
                          ) : (
                            <div className="video-meta">
                              <div className="meta">
                                <div className="title">
                                  <MergeStatus item={currentVideo} />
                                  <span>{currentVideo.get('title')}</span>
                                </div>
                                <ViewCounter 
                                  isSelf={isSelf} 
                                  live={currentVideo.get('live')} 
                                  item={currentVideoStats} 
                                  socket={socket}
                                />
                              </div>
                              <div className="stats">
                                <LikeButton 
                                  videoId={currentVideo.get('id')} 
                                  mergeStatus={currentVideo.get('merge_status')}
                                  stat={myCurrentVideoStats.get('like')} 
                                  likes={currentVideoStats.get('like')}
                                  jwt={jwt}
                                  playerState={playerState}
                                  socket={socket}
                                  hasAccess={hasAccess}
                                  isShow={isShow}
                                />
                                <GenericCounter
                                  classes="pearls"
                                  type="gift"
                                  listenFor="broadcaster_alltime_pearls"
                                  title={TOTAL_PEARLS}
                                  image={pearl}
                                  value={currentVideoStats.get('alltime_pearl_qty')}
                                  socket={socket}
                                />
                                <div className="empty" /> 
                                <FavouriteButton 
                                  videoId={currentVideo.get('id')} 
                                  mergeStatus={currentVideo.get('merge_status')}
                                  stat={myCurrentVideoStats.get('favourite')} 
                                  jwt={jwt}
                                  playerState={playerState}
                                  hasAccess={hasAccess}
                                  isShow={isShow}
                                />
                                <ShareButton
                                  position="bottom right" 
                                  url={fullUrl}
                                  title={currentVideo.get('title')}
                                  videoId={currentVideo.get('id')}
                                  orientation={currentVideo.get('orientation') === 0 ? 'portrait' : 'landscape'}
                                />
                                <FlagButton
                                  position="bottom right" 
                                  videoId={currentVideo.get('id')}
                                  stat={myCurrentVideoStats.get('flag')} 
                                  jwt={jwt}
                                  hasAccess={hasAccess}
                                  isShow={isShow}
                                />
                              </div>
                              {
                                me.size > 0 && currentVideo.get('live') && !gettingCurrentViewers ? (
                                  <ViewerList 
                                    socket={socket}
                                    viewers={currentViewers.get('results')}
                                    viewerCount={currentViewers.get('count')}
                                    viewerSize={viewerSize}
                                  />
                                ) : null
                              }
                              {
                                gettingComments ? (
                                  <Spinner type="relative" color="grey" />
                                ) : (
                                  <Comments
                                    comments={comments}
                                    socket={socket}
                                    orientation="portrait"
                                    video={currentVideo}
                                    playerState={playerState}
                                    jwt={jwt}
                                    hasAccess={hasAccess}
                                    isShow={isShow}
                                  />
                                )
                              }
                            </div>
                          )
                        }
                      </div>
                    ) : (
                      <div className="video-wrapper landscape">
                        <LoadableAgoraProvider
                          video={currentVideo}
                          ref={this.agora}
                        >
                          {
                            gettingAgoraKey || gettingMyCurrentVideoStats || gettingVideoStats ? (
                              <Spinner type="relative" color="grey" />
                            ) : (
                              <LoadableVideoPlayer
                                socket={socket}
                                video={currentVideo}
                                isConverting={isConverting}
                                sendPlayerState={this.sendPlayerState}
                                isSelf={isSelf}
                                jwt={jwt}
                                videoUrl={() => this.videoUrl()}
                                refresh={() => this.runRefresh()}
                              >
                                <div className="video-meta">
                                  <div className="meta">
                                    <div className="title">
                                      <MergeStatus item={currentVideo} />
                                      <span>{currentVideo.get('title')}</span>
                                    </div>
                                    <ViewCounter isSelf={isSelf} live={currentVideo.get('live')} item={currentVideoStats} socket={socket}/>
                                  </div>
                                  {
                                    me.size > 0 && currentVideo.get('live') && !gettingCurrentViewers ? (
                                      <ViewerList 
                                        socket={socket}
                                        viewers={currentViewers.get('results')}
                                        viewerCount={currentViewers.get('count')}
                                        viewerSize={viewerSize}
                                      />
                                    ) : null
                                  }
                                  <div className="stats">
                                    <LikeButton 
                                      videoId={currentVideo.get('id')} 
                                      mergeStatus={currentVideo.get('merge_status')}
                                      stat={myCurrentVideoStats.get('like')} 
                                      likes={currentVideoStats.get('like')}
                                      jwt={jwt}
                                      playerState={playerState}
                                      socket={socket}
                                      hasAccess={hasAccess}
                                      isShow={isShow}    
                                    />
                                    <GenericCounter
                                      classes="pearls"
                                      type="gift"
                                      listenFor="broadcaster_alltime_pearls"
                                      title={TOTAL_PEARLS}
                                      image={pearl}
                                      value={currentVideoStats.get('alltime_pearl_qty')}
                                      socket={socket}
                                    />
                                    <div className="empty" /> 
                                    <FavouriteButton 
                                      videoId={currentVideo.get('id')} 
                                      mergeStatus={currentVideo.get('merge_status')}
                                      stat={myCurrentVideoStats.get('favourite')} 
                                      jwt={jwt}
                                      playerState={playerState}
                                      hasAccess={hasAccess}
                                      isShow={isShow}    
                                    />
                                    <ShareButton
                                      position="bottom right" 
                                      url={fullUrl}
                                      title={currentVideo.get('title')}
                                      videoId={currentVideo.get('id')}
                                      orientation={currentVideo.get('orientation') === 0 ? 'portrait' : 'landscape'}
                                    />
                                    <FlagButton
                                      position="bottom right" 
                                      videoId={currentVideo.get('id')}
                                      stat={myCurrentVideoStats.get('flag')} 
                                      jwt={jwt}
                                      hasAccess={hasAccess}
                                      isShow={isShow}    
                                    />
                                  </div>
                                </div>
                              </LoadableVideoPlayer>
                            )
                          }
                        </LoadableAgoraProvider>
                        {
                          gettingComments ? (
                            <Spinner type="relative" color="grey" />
                          ) : (
                            <Comments
                              comments={comments}
                              socket={socket}
                              orientation="landscape"
                              video={currentVideo}
                              playerState={playerState}
                              jwt={jwt}
                            />
                          )
                        }
                      </div>
                    )
                  ) : error ? (
                    null
                  ) : (
                    <Spinner type="relativefull" color="grey" />
                  )
                }
            </div>
            <div className="empty">
              {
                error ? (
                  <div className="video-error">
                    <img src={errorImage} />
                    <div>{ERROR_LOADING}<br />{ERROR_BLOCKED}</div>
                  </div>
                ) : null
              }
            </div>
            <Footer />
          </section>
        </main>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  loadingVideoPage: state.getIn(['page', 'loadingVideoPage']),
  error: state.getIn(['video', 'error']) || state.getIn(['page', 'error']),
  gettingRelatedVideos: state.getIn(['video', 'gettingRelatedVideos']),
  relatedVideos: state.getIn(['video', 'relatedVideos']),
  gettingVideo: state.getIn(['video', 'gettingVideo']),
  currentVideo: state.getIn(['video', 'currentVideo']),
  isShow: state.getIn(['video', 'currentVideo', 'show_id']),
  hasAccess: state.getIn(['video', 'currentVideo', 'show_description', 'purchase_status']),
  currentVideoStats: state.getIn(['video', 'currentVideoStats']),
  me: state.getIn(['user', 'me']),
  gettingMyCurrentVideoStats: state.getIn(['user', 'gettingMyCurrentVideoStats']),
  myCurrentVideoStats: state.getIn(['user', 'myCurrentVideoStats']),
  gettingComments: state.getIn(['video', 'gettingComments']),
  comments: state.getIn(['video', 'comments']),
  gettingAgoraKey: state.getIn(['video', 'gettingAgoraKey']),
  agoraKey: state.getIn(['video', 'agoraKey']),
  currentViewers: state.getIn(['video', 'currentViewers']),
  gettingCurrentViewers: state.getIn(['video', 'gettingCurrentViewers']),
  gettingPaidContent: state.getIn(['video', 'gettingPaidContent']),
  paidContent: state.getIn(['video', 'paidContent']),
  loadingUser: state.getIn(['user', 'loadingUser']),
  currentUser: state.getIn(['user', 'user', 'results']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadUpVideoPageRequest,
      endViewRequest,
      getSingleVideoStatsRequest,
      getPaidContentRequest,
      showModal
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Video))