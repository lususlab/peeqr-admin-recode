import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Link } from 'routes';
import { withRouter } from 'next/router';

import {
  loadUpProfilePageRequest,
} from "store/pages/actions";

import { 
  loadMoreVideosByUserRequest
} from "store/video/actions";

import Layout from "components/Layout";
import Footer from "components/Footer";
import Avatar from "components/Avatar";
import ShareButton from "components/ShareButton";
import Spinner from "components/Spinner";
import VideoItem from "components/VideoItem";
import LoadMore from 'components/LoadMore';
import ProfileStats from "components/ProfileStats";
import SubscribeButton from "components/SubscribeButton";
import BlockButton from "components/BlockButton";
import ImageEditButton from "components/ImageEditButton";

import profilePearls from 'static/img/profile-pearls.png';
import profileLikes from 'static/img/profile-likes.png';
import profileVideo from 'static/img/profile-videos.png';
import profileSubscribers from 'static/img/profile-subscribers.png';
import profileCoins from 'static/img/profile-coins.png';

import {
  DOT,
  AT,
  LIKE,
  CURRENT,
  TOTAL,
  VIDEO,
  SUBSCRIBER,
  PEARL,
  COIN,
  LOAD_MORE,
  NO_RESULTS_FOUND
} from "helpers/text";

import { getJwt } from 'helpers/session'

import { createNotification } from 'helpers/createnotification';

import pearl from 'static/img/icon-pearl-black-small.png';

class Profile extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isSelf: props.me.get('id') === props.currentUser.getIn(['user', 'id'])
    }
  }

  static async getInitialProps(ctx) {
    const { store, isServer, query } = ctx;

    const {slug} = query;
    const jwt = await getJwt(ctx);
    const page = 1;
    const size = 10;

    await store.dispatch(loadUpProfilePageRequest(slug, page, size, jwt))

    return {jwt, slug}
  }

  componentDidUpdate(prevProps) {
    const { me, currentUser } = this.props;

    if (currentUser && currentUser !== prevProps.currentUser) {
      this.setState({ isSelf: me.get('id') === currentUser.getIn(['user', 'id']) })
    }
  }

  handleLoadMore = (e) => {
    e.preventDefault();
    const { userVideos, loadMoreVideosByUserRequest } = this.props;

    loadMoreVideosByUserRequest(userVideos.get('next'))
  }

  render() {
    const {
      loadingUser,
      loadingProfilePage,
      loggingIn,
      currentUser,
      me,
      gettingUserVideos,
      userVideos,
      loadingMore,
      jwt,
      router,
    } = this.props;
    const {
      isSelf,
    } = this.state;

    const fullUrl = `${process.env.HOSTNAME}${router.asPath}`;

    return (
      <Layout
        metaTitle={`${currentUser.getIn(['user', 'name_display'])} ${DOT} ${currentUser.getIn(['user', 'username'])}`}
        metaImage={currentUser.getIn(['user', 'photo'])}
      >
        <main className="profile">
          {
            loadingUser || loadingProfilePage || loggingIn || gettingUserVideos ? (
              <Spinner type="relative" color="grey" />
            ) : (
              <section id={`profile-${currentUser.getIn(['user', 'id'])}`} className="modal-type">
                <div className="box mid">
                  <div className="profile-area">
                    <div className="cover-photo">
                      <img src={currentUser.getIn(['user', 'cover_photo'])} />
                      {
                        isSelf ? (
                          <ImageEditButton 
                            type="cover"
                            jwt={jwt}
                            username={currentUser.getIn(['user', 'username'])}
                          />
                        ) : null
                      }
                    </div>
                    <div className="profile-avatar">
                      <Avatar size="xlarge" username={currentUser.getIn(['user', 'name_display'])} photo={currentUser.getIn(['user', 'photo'])} />
                      {
                        isSelf ? (
                          <ImageEditButton 
                            type="avatar"
                            jwt={jwt}
                            username={currentUser.getIn(['user', 'username'])}
                          />
                        ) : null
                      }
                    </div>
                    <div className="profile-name">
                      <h1>{currentUser.getIn(['user', 'name_display'])}</h1>
                      <h4>{AT}{currentUser.getIn(['user', 'username'])}</h4>
                    </div>
                    <div className="profile-actions">
                      {
                        !isSelf ? (
                          <SubscribeButton 
                            userId={currentUser.getIn(['user', 'id'])} 
                            username={currentUser.getIn(['user', 'username'])}
                            stat={currentUser.get('subscribe')} 
                            jwt={jwt}
                          />
                        ) : null
                      }
                      {
                        !isSelf ? (
                          <BlockButton 
                            position="top left" 
                            userId={currentUser.getIn(['user', 'id'])} 
                            username={currentUser.getIn(['user', 'username'])}
                            name={currentUser.getIn(['user', 'name_display'])}
                            stat={currentUser.get('block')} 
                            jwt={jwt}
                          />
                        ) : null
                      }
                      <ShareButton 
                        profile
                        position="top right" 
                        url={fullUrl}
                        title={currentUser.get('name_display')}
                      />
                    </div>
                    <div className={`profile-stats ${isSelf ? 'self' : ''}`}>
                      <ProfileStats 
                        classes="like-count"
                        icon={profileLikes}
                        counter={currentUser.getIn(['user', 'like_count'])}
                        singular={LIKE}
                      />
                      {
                        isSelf ? (
                          <ProfileStats 
                            classes="pearl-current-count"
                            icon={profilePearls}
                            counter={currentUser.getIn(['user', 'pearl_qty'])}
                            singular={PEARL}
                            prefix={CURRENT}
                          />
                        ) : null
                      }
                      <ProfileStats 
                        classes="pearl-total-count"
                        icon={profilePearls}
                        counter={currentUser.getIn(['user', 'alltime_pearl_qty'])}
                        singular={PEARL}
                        prefix={TOTAL}
                      />
                      {
                        isSelf ? (
                          <ProfileStats 
                            classes="coin-count"
                            icon={profileCoins}
                            counter={me.get('coin_qty')}
                            singular={COIN}
                          />
                        ) : null
                      }
                      <ProfileStats 
                        classes="subscriber-count"
                        icon={profileSubscribers}
                        counter={currentUser.getIn(['user', 'subscriber_count'])}
                        singular={SUBSCRIBER}
                      />
                      <ProfileStats 
                        classes="video-count"
                        icon={profileVideo}
                        counter={userVideos.get('count')}
                        singular={VIDEO}
                      />
                    </div>
                  </div>
                  <div className="box-content">
                  {
                    gettingUserVideos ? (
                      <Spinner type="relative" color="grey" />
                    ) : (
                      <div className="user-videos">
                        {
                          userVideos.get('count') > 0 ? (
                            userVideos.get('results').map(item => 
                              <VideoItem 
                                key={item.get('id')}
                                className="video-item"
                                item={item}
                                isSelf={isSelf}
                                jwt={jwt}
                              />
                            )
                          ) : NO_RESULTS_FOUND
                        }
                      </div>
                    )
                  }
                  {
                    (userVideos.get('next') && !gettingUserVideos) ? (
                      <div className="centered">
                        <LoadMore size="small" color="white" background="bggrey" onClick={this.handleLoadMore} loading={loadingMore} />
                      </div>
                    ) : null
                  }
                  </div>
                </div>
              </section>
            )
          }
          <Footer />
        </main>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  loadingProfilePage: state.getIn(['page', 'loadingProfilePage']),
  loadingUser: state.getIn(['user', 'loadingUser']),
  loggingIn: state.getIn(['user', 'loggingIn']),
  error: state.getIn(['page', 'error']),
  currentUser: state.getIn(['user', 'user', 'results']),
  me: state.getIn(['user', 'me']),
  gettingUserVideos: state.getIn(['video', 'gettingUserVideos']),
  userVideos: state.getIn(['video', 'userVideos']),
  loadingMore: state.getIn(['video', 'loadingMore']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadUpProfilePageRequest,
      loadMoreVideosByUserRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Profile))