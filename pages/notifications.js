import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { 
  redirectIfNotAuthenticated 
} from "helpers/session";

import Layout from "components/Layout";
import NotificationsList from "components/NotificationsList";

import { MY_NOTIFICATIONS } from 'helpers/text';
import redirect from "helpers/redirect";
import { getJwt, getMe } from 'helpers/session';

import {
  getNotificationsRequest
} from 'store/user/actions'

import { Router } from "routes";

class Notifications extends Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }
  static async getInitialProps(ctx) {
    const jwt = await getJwt(ctx)
    const username = await getMe(ctx)
    
    const { req, store } = ctx;

    if (!jwt) {
      redirect("/", ctx);
    }

    await store.dispatch(getNotificationsRequest(username, jwt))

    return {jwt, username};
  }
  render() {
    const { gettingNotifications, notifications, jwt } = this.props;
    return (
      <Layout 
        jwt={jwt}
        metaTitle={MY_NOTIFICATIONS}
      >
        <section id="notifications" className="modal-type">
          <div className="box small">
            <div className="box-content">
              <NotificationsList 
                isNotificationsPage
                jwt={jwt} 
                gettingNotifications={gettingNotifications}
                notifications={notifications}
              />
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}
const mapStateToProps = state => ({
  me: state.getIn(['user', 'me']),
  gettingNotifications: state.getIn(['user', 'gettingNotifications']),
  notifications: state.getIn(['user', 'notifications'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    getNotificationsRequest
  },
  dispatch
);

export default connect( mapStateToProps, mapDispatchToProps)(Notifications)