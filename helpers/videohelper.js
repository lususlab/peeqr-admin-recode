import moment from 'moment';

export const videoCurrentTime = (playerState, mergeStatus) => {
  if (playerState) {
    let timestamp;
    const {currentTime} = playerState;
    if (mergeStatus === 2) {
      return timestamp = moment().startOf('day').seconds(currentTime).format('mm:ss');
    }
    return timestamp = "00:00";
  }
  return timestamp = "00:00";
}

export const formatVideoTime = time => {
  return moment(time, 'mm:ss').diff(moment().startOf('day'), 'seconds')
}