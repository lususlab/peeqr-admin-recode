import cookie from "cookie";
import jsCookie from "js-cookie";
import redirect from "./redirect";

export const getJwt = ctx => getCookie("jwt", ctx.req);

export const getMe = ctx => getCookie("username", ctx.req);

export const isAuthenticated = ctx => !!getJwt(ctx);

export const redirectIfAuthenticated = ctx => {
  if (isAuthenticated(ctx)) {
    redirect("/", ctx);
  }
};

export const redirectIfNotAuthenticated = ctx => {
  if (!isAuthenticated(ctx)) {
    redirect("/", ctx);
  }
};

export const setCookie = (key, value) => {
  if (process.browser) {
    jsCookie.set(key, value, {
      expires: 7,
      path: "/"
    });
  }
};

export const removeCookie = key => {
  if (process.browser) {
    jsCookie.remove(key, {
      expires: 7,
      path: "/"
    });
  }
};

const getCookieFromBrowser = key => jsCookie.get(key);

const getCookieFromServer = (key, req) => {
  if (!req.headers.cookie) {
    return undefined;
  }
  const rawCookie = cookie.parse(req.headers.cookie)

  if (!rawCookie[key]) {
    return undefined;
  }
  return rawCookie[key];
};

export const getCookie = (key, req) => process.browser
    ? getCookieFromBrowser(key) 
    : getCookieFromServer(key, req);
