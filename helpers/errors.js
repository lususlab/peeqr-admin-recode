export const errors = [
  { 
    type: 'email', 
    errorMessage: 'the email address appears to be invalid.' 
  },
  { 
    type: 'password', 
    errorMessage: 'the password must have at least 8 characters, 1 lowercase, 1 uppercase, 1 special character and 1 number.' 
  },
  {
    type: 'name',
    errorMessage: 'the name must be between 2 to 30 characters.'
  },
  {
    type: 'username',
    errorMessage: 'the username must be between 6 to 20 alphabetical characters only.'
  },
  {
    type: 'confirmpassword',
    errorMessage: 'the passwords do not match.'
  },
];

export const notdefined = '????';

export const ERROR_RELOADING = 'There seems to be an error loading the video. Reloading...';

export const ERROR_LOADING = 'There seems to be a problem loading the video. Please try again later.';

export const ERROR_BLOCKED = 'Alternatively, you may have been blocked by the broadcaster.';

export const JOIN_VIDEO_FAILED = 'Failed to join the channel';

export const FAILED_TO_INIT_PLAYER = 'Failed to initialize video';

export const BROWSER_NO_SUPPORT = "Your browser does not support playback of the video";

export const NO_ACCESS = "You don't have access to this stream.";

export const EMAIL_ERROR = "Verification Error.";

export const EMAIL_ALT = "Try copying the verification link directly into a browser."

