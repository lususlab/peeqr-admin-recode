export const resizeMainHeight = () => {
  let headerHeight, searchHeight, windowHeight
  headerHeight = document.querySelector('header').offsetHeight;
  searchHeight = document.querySelector('#search-bar').offsetHeight;
  windowHeight = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;

  return `${windowHeight - headerHeight - searchHeight}px`;
}

export const resizeComponentHeight = (a, b) => `${document.querySelector(a).offsetHeight - document.querySelector(b).offsetHeight}px`