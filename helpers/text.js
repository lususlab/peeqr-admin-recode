const YES = "Yes";
const NO = "No";
const DELETE = "Delete";
const COMPANY = "Lusus Lab Pte. Ltd.";
const BRAND = "Peeqr";
const MAIN_META_TITLE = "Community-based live streaming with Peeqr!";
const COPYRIGHT = "©";
const AT = "@";
const DOT = "•";
const CURRENCY = "$";
const SEPARATOR = " | "
const SIGN_IN_TO_PEEQR = "Sign in to Peeqr";
const SIGN_UP_WITH_PEEQR = "Sign up with Peeqr";
const BE_A_PARTNER = "Be a Partner";
const GET_THE_APP = "Get the App";
const SIGN_IN_SIGN_UP = "Sign In / Sign Up";
const SIGN_OUT = "Sign Out";
const VIDEOS = "Videos";
const USERS = "Users";
const HASHTAGS = "Hashtags";
const GO_LIVE = "Go Live";
const BLOG = "Blog";
const MY_PROFILE = "My Profile";
const SETTINGS = "Settings";
const SIGN_UP = "Sign Up";
const LOGIN = "Sign In";
const LOGIN_TWITTER = "Sign in with Twitter";
const LOGIN_FACEBOOK = "Sign in with Facebook";
const OR = "Or";
const NOT_A_USER = "Not a user?";
const SIGN_UP_NOW = "Sign up now!";
const FORGOT_PASSWORD = "Forgot your Password?";
const LABEL_EMAIL = "Email";
const LABEL_PASSWORD = "Password";
const LABEL_USERNAME = "Username";
const LABEL_CONFIRM_PASSWORD = "Confirm Password";
const LOGGING_YOU_OUT = "Logging you out...";
const WE_HATE_TO_SEE_YOU_GO = "We hate to see you go";
const COME_BACK_SOON = "Come back soon!";
const SEARCH = "Search...";
const LIVE = "Live";
const CONVERTING = "Converting";
const NO_RESULTS_FOUND = "No results found."
const CATEGORIES = "Categories";
const NOTHING_TO_SEE_HERE = "Nothing to see here!";
const JUST_WENT_LIVE = "just went live!"
const COMMENTED = "commented";
const LIKED = "liked";
const VIEWED = "viewed";
const SHARED = "shared";
const SUBSCRIBED_TO_YOU = "subscribed to you!"
const ON = "on";
const VIEW_ALL_NOTIFICATIONS = "View all notifications";
const VIEW_ALL = "View All";
const MY_NOTIFICATIONS = "My Notifications";
const LOAD_MORE = "Load More";
const DISCOVER = "Discover";
const FRESH = "Fresh";
const RELATED = "Related";
const SUBSCRIPTIONS = "Subscriptions";
const RESTRICTED = "Access to ticketed streams are currently not available for purchase on the web.";
const USE_MOBILE = "Use our mobile app to get access!";
const SEND_FEEDBACK = "Send Feedback";
const PRIVACY = "Privacy";
const TERMS = "Terms of Use";
const ABOUT = "About";
const CONCURRENT = "concurrent";
const MAX = "max";
const VIEWS = "view";
const BLOCK = "Block";
const UNBLOCK = "Unblock";
const MUTE = "Mute";
const UNMUTE = "Unmute";
const SUBSCRIBE = "subscribe";
const UNSUBSCRIBE = "unsubscribe";
const TOTAL_LIKES = "Total Likes";
const TOTAL_PEARLS = "Total Pearls";
const CURRENT = "Current";
const TOTAL = "Total";
const PEARL = "Pearl";
const COIN = "Coin";
const SUBSCRIBER = "Subscriber";
const LIKE = "Like";
const VIDEO = "Video";
const FAVOURITE = "Favourite";
const SHARE = "Share";
const SIMPLE_EMBED = "Simple Embed";
const THUMBNAIL_EMBED = "Thumbnail Embed";
const FLAG_AS_INAPPROPRIATE = "Flag as Inappropriate";
const UNFLAG = "Unflag";
const MORE = "More";
const COMMENT = "Comment";
const SIGN_IN_TO_BLOCK = "Sign in to block.";
const SIGN_IN_TO_FLAG = "Sign in to flag.";
const SIGN_IN_TO_FAVOURITE = "Sign in to favourite.";
const SIGN_IN_TO_SUBSCRIBE = "Sign in to subscribe.";
const SIGN_IN_TO_LIKE = "Sign in to like.";
const SIGN_IN_TO_COMMENT = "Sign in to comment.";
const WARNING = "Be respectful to the broadcaster. Any inappropriate content or conduct that goes against the guidelines will result in the account getting suspended, or banned.";
const BE_THE_FIRST = "Be the first to comment!";
const LIVE_COMMENT = "Live Comment";
const PAUSED = "Paused";
const HAS_ENDED = "has ended the stream";
const NOT_FOUND = 'Page not found!';
const IS_CONVERTING = "This video will be available for viewing once it's converted!";
const LIVE_VIEWS = "Live views";
const TOTAL_VIEWS = "Total views";
const CONCURRENT_VIEWS = "Concurrent views";
const MAX_CONCURRENT_VIEWS = "Max concurrent views";
const VIEWING_NOW = " is viewing now!";
const CONFIRM = "Confirm";
const CANCEL = "Cancel";
const GUIDELINES = "guidelines";
const NUDITY = "Nudity or blatantly sexual content";
const ABUSIVE = "Hateful, abusive, or contemptuous content";
const VIOLENT = "Violent or graphic content";
const SPAM = "Threats, spam, and scams";
const IMAGE_GUIDE = "Drop an image or click to select a file to upload.";
const IMAGE_RULES = "(jpg or png only)";
const UPLOAD = "Upload";
const YOU_HAVE_BLOCKED = "You have blocked";
const BLOCK_GUIDELINE = "You'll need to unblock this user in order to view their videos."
const REDIRECTING = "Redirecting...";
const EMAIL_VERIFIED = "Email Verified!";
const BACK_TO_HOME = "Back to Home";
const EXPORT_VIDEO = "Export Video";
const DELETE_VIDEO = "Delete Video";
const CONFIRM_DELETE = "Are you sure you want to delete ";
const I_HAVE_AN_ACCOUNT = "I have an account"
const ACCEPT = "I accept the ";

export {
  YES,
  NO,
  DELETE,
  COMPANY,
  BRAND,
  MAIN_META_TITLE,
  COPYRIGHT,
  AT,
  DOT,
  CURRENCY,
  SEPARATOR,
  SIGN_IN_TO_PEEQR,
  SIGN_UP_WITH_PEEQR,
  BE_A_PARTNER,
  GET_THE_APP,
  SIGN_IN_SIGN_UP,
  VIDEOS,
  USERS,
  HASHTAGS,
  GO_LIVE,
  BLOG,
  MY_PROFILE,
  SIGN_OUT,
  SETTINGS,
  SIGN_UP,
  LOGIN,
  LOGIN_TWITTER, 
  LOGIN_FACEBOOK,
  OR,
  NOT_A_USER,
  SIGN_UP_NOW,
  FORGOT_PASSWORD,
  LABEL_EMAIL,
  LABEL_PASSWORD,
  LABEL_USERNAME,
  LABEL_CONFIRM_PASSWORD,
  LOGGING_YOU_OUT,
  WE_HATE_TO_SEE_YOU_GO,
  COME_BACK_SOON,
  SEARCH,
  LIVE,
  CONVERTING,
  NO_RESULTS_FOUND,
  CATEGORIES,
  NOTHING_TO_SEE_HERE,
  JUST_WENT_LIVE,
  COMMENTED,
  ON,
  LIKED,
  VIEWED,
  SHARED,
  SUBSCRIBED_TO_YOU,
  VIEW_ALL_NOTIFICATIONS,
  VIEW_ALL,
  MY_NOTIFICATIONS,
  LOAD_MORE,
  DISCOVER,
  FRESH,
  RELATED,
  SUBSCRIPTIONS,
  RESTRICTED,
  USE_MOBILE,
  SEND_FEEDBACK,
  PRIVACY,
  TERMS,
  ABOUT,
  CONCURRENT,
  MAX,
  VIEWS,
  BLOCK,
  UNBLOCK,
  MUTE,
  UNMUTE,
  SUBSCRIBE,
  UNSUBSCRIBE,
  TOTAL_LIKES,
  TOTAL_PEARLS,
  CURRENT,
  TOTAL,
  PEARL,
  COIN,
  SUBSCRIBER,
  LIKE,
  VIDEO,
  FAVOURITE,
  SHARE,
  SIMPLE_EMBED,
  THUMBNAIL_EMBED,
  FLAG_AS_INAPPROPRIATE, 
  UNFLAG, 
  MORE,
  COMMENT,
  SIGN_IN_TO_BLOCK,
  SIGN_IN_TO_FLAG,
  SIGN_IN_TO_FAVOURITE,
  SIGN_IN_TO_SUBSCRIBE,
  SIGN_IN_TO_LIKE,
  SIGN_IN_TO_COMMENT,
  WARNING,
  BE_THE_FIRST,
  LIVE_COMMENT,
  PAUSED,
  HAS_ENDED,
  NOT_FOUND,
  IS_CONVERTING,
  LIVE_VIEWS,
  TOTAL_VIEWS,
  CONCURRENT_VIEWS,
  MAX_CONCURRENT_VIEWS,
  VIEWING_NOW,
  CONFIRM,
  CANCEL,
  GUIDELINES,
  NUDITY,
  ABUSIVE,
  VIOLENT,
  SPAM,
  IMAGE_GUIDE,
  IMAGE_RULES,
  UPLOAD,
  YOU_HAVE_BLOCKED,
  BLOCK_GUIDELINE,
  REDIRECTING,
  EMAIL_VERIFIED,
  BACK_TO_HOME,
  EXPORT_VIDEO,
  DELETE_VIDEO,
  CONFIRM_DELETE,
  I_HAVE_AN_ACCOUNT,
  ACCEPT,
}