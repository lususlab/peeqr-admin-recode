export const closeonclick = (run) => {
  document.body.onclick = (e) => {
    if (!e.target.closest('.bubble')) {
      run()
    }
  };
}