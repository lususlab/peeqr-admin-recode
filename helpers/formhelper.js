export function validCheck(isValid, fieldType) {
  const validFields = this.state.validFields;
  const newValidFields = Object.assign({}, { fieldType, isValid });
  const newKey = newValidFields.fieldType;
  const newValue = newValidFields.isValid;
  
  const notExisting = validFields.every(validField => validField.fieldType !== newKey);
  if (notExisting) {
    validFields.push(newValidFields);
  } else if (!notExisting) { 
    const existingKey = validFields.find(validField => validField.fieldType === newKey);
    if (existingKey !== newValue) {
      existingKey.isValid = newValue;
    }
  }
  const allValidCheck = validFields.every(validField => validField.isValid === true);
  this.setState({
    allValidCheck,
  });
}
