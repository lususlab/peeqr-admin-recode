export const scrollBottom = (element) => {
  const el = document.querySelector(element);
  el.scrollTop = el.offsetHeight * 2;
}