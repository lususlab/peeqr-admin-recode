import React from 'react';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

const WithCustomCookies = Component => {
  class WithCustomCookiesComponent extends React.Component {
    static propTypes = {
      cookies: instanceOf(Cookies).isRequired
    };
    static async getInitialProps(ctx) {
      const props = await Component.getInitialProps(ctx);
      return { ...props };
    }
    render() {
    return <Component {...this.props} cookies={this.props.cookies} />;
    }
  }

  return withCookies(WithCustomCookiesComponent);
};

export default WithCustomCookies;