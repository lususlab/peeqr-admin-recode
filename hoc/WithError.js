import React from 'react';
import ErrorPage from 'pages/error';

const withError = Component => {
  class WithErrorComponent extends React.Component {
    static async getInitialProps(ctx) {
      const props = await Component.getInitialProps(ctx);
      const { statusCode } = ctx.res || {};

      return { statusCode, ...props };
    }

    render() {
      const { statusCode } = this.props;
      if (statusCode && statusCode !== 200) {
        return <ErrorPage statusCode={statusCode} />;
      }
      return <Component {...this.props} />;
    }
  }

  return WithErrorComponent;
};

export default withError;