import { fromJS } from 'immutable';
import {
  BATCH_GET_VIDEOS_SUCCESS,
} from "store/video/actions";

export const INITIAL_STATE = fromJS({
  videos: {},
})

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case BATCH_GET_VIDEOS_SUCCESS:
      return state.mergeDeep(action.payload.entities);
    default: 
      return state;
  }
}