import { normalize, schema } from 'normalizr'

export const video = new schema.Entity('videos', {}, {
  idAttribute: 'id',
  processStrategy: entity => {
    return entity
  }
})

export const videosList = new schema.Array(video)

export const videosListNormalizer = result => normalize(result, videosList)