import { fromJS } from "immutable";
import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import { composeWithDevTools } from "remote-redux-devtools";
import createSagaMiddleware, { END } from "redux-saga";
import rootSaga from "store/sagas";
import reducer from "store/reducer";

// Setup
const middleWare = [];


// Initial Action
const beforeEmitter = () => next => action => {
  const beforeAction = {
    ...action,
    type: action.type
  };
  next(beforeAction); // 'beforeEmitter' emit a 'before-action' for every action
  return next(action);
};
// middleWare.push(beforeEmitter)

// Saga Middleware
const sagaMiddleware = createSagaMiddleware();
middleWare.push(sagaMiddleware);

// Logger Middleware. This always has to be last
const loggerMiddleware = createLogger({
  predicate: () => process.env.NODE_ENV === "development"
});
middleWare.push(loggerMiddleware);

const composeEnhancers = composeWithDevTools({ realtime: true, port: 8000 });

const createStoreWithMiddleware = composeEnhancers(
  applyMiddleware(...middleWare)
)(createStore);

const makeStore = (initialState) => {
  const store = createStoreWithMiddleware(reducer, fromJS(initialState));

  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga);
  };

  store.runSagaTask();

  return store;
};

export default makeStore;
