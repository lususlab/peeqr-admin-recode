import { all, fork, take } from "redux-saga/effects";

import { beforeLoad } from "store/before/sagas";
import { CONTINUE } from "store/before/actions";

import beforeSagas from "store/before/sagas";
import pageSagas from "store/pages/sagas";
import userSagas from "store/user/sagas";
import videoSagas from "store/video/sagas";
import searchSagas from "store/search/sagas";

export default function* rootSaga() {
  yield all([
    beforeSagas(),
    pageSagas(),
    userSagas(),
    videoSagas(),
    searchSagas(),
  ]);
}
