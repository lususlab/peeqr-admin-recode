import { combineReducers } from 'redux-immutable';

import entitiesReducer from "store/entities/reducer";
// import beforeReducer from "store/before/reducer";
import pageReducer from "store/pages/reducer";
import userReducer from "store/user/reducer";
import videoReducer from "store/video/reducer";
import searchReducer from "store/search/reducer";

const reducers = {
  // before: beforeReducer,
  entities: entitiesReducer,
  page: pageReducer,
  user: userReducer,
  video: videoReducer,
  search: searchReducer,
};

export default combineReducers(reducers);
