import {fromJS} from 'immutable';

import {
  BEFORE_LOAD_REQUEST,
  BEFORE_LOAD_SUCCESS,
  BEFORE_LOAD_FAILED,

  CONTINUE
} from 'store/before/actions'

const INITIAL_STATE = fromJS({  
  initialLoading: false,
})

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case BEFORE_LOAD_REQUEST:
      return state.merge ({ 
        ...state, 
        initialLoading: true 
      });
    case BEFORE_LOAD_SUCCESS:
      return state.merge ({ 
        ...state, 
        initialLoading: false,
        me: action.response.data
      });
    case BEFORE_LOAD_FAILED:
      return state.merge ({ 
        ...state, 
        initialLoading: false
      });
    default:
      return state;
  }
}
