import { take, fork, call, put } from "redux-saga/effects";
import userApi from "services/api/user";
import { setCookie, removeCookie } from "helpers/session";

import {
  BEFORE_LOAD_REQUEST,
  BEFORE_LOAD_SUCCESS,
  BEFORE_LOAD_FAILED,

  CONTINUE
} from 'store/before/actions'

export function* beforeLoad(jwt) {
  try {
    const response = yield call(userApi.getSelf, jwt)
    yield put({type: BEFORE_LOAD_SUCCESS, response})
    yield call(setCookie, "username", response.data.username)
    yield put({type: CONTINUE})
    return response;
  } catch(e) {
    yield call(removeCookie, 'jwt')
    yield call(removeCookie, 'username')
    yield put({type: BEFORE_LOAD_FAILED, e})
    yield put({type: CONTINUE})
  } finally {
    yield put({type: CONTINUE})
  }
}

function* watchBeforeLoad() {
  while (true) {
    const {jwt} = yield take(BEFORE_LOAD_REQUEST);
    yield fork(beforeLoad, jwt);
  }
}
export default function* beforeSagas() {
  yield fork(watchBeforeLoad);
}
