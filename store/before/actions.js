export const BEFORE_LOAD_REQUEST = "BEFORE_LOAD_REQUEST";
export const BEFORE_LOAD_SUCCESS = "BEFORE_LOAD_SUCCESS";
export const BEFORE_LOAD_FAILED = "BEFORE_LOAD_FAILED";

export const CONTINUE = "CONTINUE"

export const beforeLoadRequest = jwt => ({ type: BEFORE_LOAD_REQUEST, jwt });

