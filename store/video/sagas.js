import { take, takeEvery, fork, call, put } from "redux-saga/effects";
import videoApi from "services/api/video";

import {
  LOAD_UP_VIDEO_PAGE_SUCCESS
} from "store/pages/actions"

import {  
  GET_BANNERS_REQUEST,
  GET_BANNERS_SUCCESS,
  GET_BANNERS_FAILED,

  GET_COLLECTIONS_REQUEST,
  GET_COLLECTIONS_SUCCESS,
  GET_COLLECTIONS_FAILED,

  GET_ALL_VIDEOS_REQUEST,
  GET_ALL_VIDEOS_SUCCESS,
  GET_ALL_VIDEOS_FAILED,

  GET_SINGLE_VIDEO_REQUEST,
  GET_SINGLE_VIDEO_SUCCESS,
  GET_SINGLE_VIDEO_FAILED,

  DELETE_SINGLE_VIDEO_REQUEST,
  DELETE_SINGLE_VIDEO_SUCCESS,
  DELETE_SINGLE_VIDEO_FAILED,

  GET_SINGLE_VIDEO_STATS_REQUEST,
  GET_SINGLE_VIDEO_STATS_SUCCESS,
  GET_SINGLE_VIDEO_STATS_FAILED,

  GET_COMMENTS_REQUEST,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAILED,

  POST_COMMENT_REQUEST,
  POST_COMMENT_SUCCESS,
  POST_COMMENT_FAILED,

  LOAD_MORE_COMMENTS_REQUEST,
  LOAD_MORE_COMMENTS_SUCCESS,
  LOAD_MORE_COMMENTS_FAILED,

  GET_RELATED_VIDEOS_REQUEST,
  GET_RELATED_VIDEOS_SUCCESS,
  GET_RELATED_VIDEOS_FAILED,

  GET_CATEGORY_VIDEOS_REQUEST,
  GET_CATEGORY_VIDEOS_SUCCESS,
  GET_CATEGORY_VIDEOS_FAILED,

  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILED,

  GET_STAFF_PICKS_REQUEST, 
  GET_STAFF_PICKS_SUCCESS,
  GET_STAFF_PICKS_FAILED,

  GET_DISCOVER_REQUEST,
  GET_DISCOVER_SUCCESS,
  GET_DISCOVER_FAILED,

  LOAD_MORE_FRESH_VIDEOS_REQUEST,
  LOAD_MORE_FRESH_VIDEOS_SUCCESS,
  LOAD_MORE_FRESH_VIDEOS_FAILED,

  START_VIEW_REQUEST,
  START_VIEW_SUCCESS,
  START_VIEW_FAILED,

  END_VIEW_REQUEST,
  END_VIEW_SUCCESS,
  END_VIEW_FAILED,

  GET_AGORA_KEY_REQUEST,
  GET_AGORA_KEY_SUCCESS,
  GET_AGORA_KEY_FAILED,

  GET_CURRENT_VIEWERS_REQUEST,
  GET_CURRENT_VIEWERS_SUCCESS,
  GET_CURRENT_VIEWERS_FAILED,

  GET_ANNOTATIONS_REQUEST,
  GET_ANNOTATIONS_SUCCESS,
  GET_ANNOTATIONS_FAILED,

  GET_PAID_CONTENT_REQUEST,
  GET_PAID_CONTENT_SUCCESS,
  GET_PAID_CONTENT_FAILED,

  GET_VIDEOS_BY_USER_REQUEST,
  GET_VIDEOS_BY_USER_SUCCESS,
  GET_VIDEOS_BY_USER_FAILED,

  LOAD_MORE_VIDEOS_BY_USER_REQUEST,
  LOAD_MORE_VIDEOS_BY_USER_SUCCESS,
  LOAD_MORE_VIDEOS_BY_USER_FAILED,
  
  batchGetVideosSuccess
  
} from "store/video/actions";

import {
  getPublicProfile
} from "store/user/sagas"

function* getBanners() {
  try {
    const response = yield call(videoApi.getBanners)
    yield put({type: GET_BANNERS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_BANNERS_FAILED, e})
  }
}

function* watchGetBanners() {
  while (true) {
    yield take(GET_BANNERS_REQUEST);
    yield fork(getBanners);
  }
}

function* getCollections() {
  try {
    const response = yield call(videoApi.getCollections)
    yield put({type: GET_COLLECTIONS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_COLLECTIONS_FAILED, e})
  }
}

function* watchGetCollections() {
  while (true) {
    yield take(GET_COLLECTIONS_REQUEST);
    yield fork(getCollections);
  }
}

function* getAllVideos(page, size, jwt) {
  try {
    const response = yield call(videoApi.getAllVideos, page, size, jwt)
    yield put({type: GET_ALL_VIDEOS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_ALL_VIDEOS_FAILED, e})
  }
}

function* watchGetAllVideos() {
  while (true) {
    const {page, size, jwt} = yield take(GET_ALL_VIDEOS_REQUEST);
    yield fork(getAllVideos, page, size, jwt);
  }
}

function* getSingleVideo(videoId, jwt) {
  try {
    const response = yield call(videoApi.getSingleVideo, videoId, jwt)
    const username = response.data.user.username
    
    yield put({type: GET_SINGLE_VIDEO_SUCCESS, response})
    yield* getPublicProfile(username, jwt)
    
    yield fork(getAnnotations, videoId)
    
    if (response.data.live && response.data.stream_platform === 1) {
      yield fork(getAgoraKey, videoId)
    } else {
      yield put({type: LOAD_UP_VIDEO_PAGE_SUCCESS})
    }
    
  } catch(e) {
    yield put({type: GET_SINGLE_VIDEO_FAILED, e})
    yield put({type: LOAD_UP_VIDEO_PAGE_SUCCESS})
  }
}

function* watchGetSingleVideo() {
  while (true) {
    const {videoId, jwt} = yield take(GET_SINGLE_VIDEO_REQUEST);
    yield fork(getSingleVideo, videoId, jwt);
  }
}

function* deleteSingleVideo(videoId, username, jwt, page, size) {
  try {
    const response = yield call(videoApi.deleteSingleVideo, videoId, jwt)
    if (response.data.msg === 'Deleted') {
      yield put({type: DELETE_SINGLE_VIDEO_SUCCESS})
      yield put({type: GET_VIDEOS_BY_USER_REQUEST, username, page, size})
    }
    return response
  } catch(e) {
    yield put({type: DELETE_SINGLE_VIDEO_FAILED, e})
  }
}

function* watchDeleteSingleVideo() {
  while (true) {
    const {videoId, username, jwt, page, size} = yield take(DELETE_SINGLE_VIDEO_REQUEST);
    yield fork(deleteSingleVideo, videoId, username, jwt, page, size);
  }
}

function* getSingleVideoStats(videoId) {
  try {
    const response = yield call(videoApi.getSingleVideo, videoId)
    yield put({type: GET_SINGLE_VIDEO_STATS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_SINGLE_VIDEO_STATS_FAILED, e})
  }
}

function* watchGetSingleVideoStats() {
  while (true) {
    const {videoId} = yield take(GET_SINGLE_VIDEO_STATS_REQUEST);
    yield fork(getSingleVideoStats, videoId);
  }
}

function* getComments(videoId, page, size) {
  try {
    const response = yield call(videoApi.getComments, videoId, page, size)
    yield put({type: GET_COMMENTS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_COMMENTS_FAILED, e})
  }
}

function* watchGetComments() {
  while (true) {
    const {videoId, page, size} = yield take(GET_COMMENTS_REQUEST);
    yield fork(getComments, videoId, page, size);
  }
}

function* postComment(videoId, userComment, timestamp, jwt) {
  try {
    const response = yield call(videoApi.postComment, videoId, userComment, timestamp, jwt)
    yield put({type: POST_COMMENT_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: POST_COMMENT_FAILED, e})
  }
}

function* watchPostComment() {
  while (true) {
    const {videoId, userComment, timestamp, jwt} = yield take(POST_COMMENT_REQUEST);
    yield fork(postComment, videoId, userComment, timestamp, jwt);
  }
}

function* loadMoreComments(url) {
  try {
    const response = yield call(videoApi.loadMoreComments, url)
    yield put({type: LOAD_MORE_COMMENTS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: LOAD_MORE_COMMENTS_FAILED, e})
  }
}

function* watchLoadMoreComments() {
  while (true) {
    const {url} = yield take(LOAD_MORE_COMMENTS_REQUEST);
    yield fork(loadMoreComments, url);
  }
}

function* getRelatedVideos(videoId, page, size, lat, long, jwt) {
  try {
    const response = yield call(videoApi.getRelatedVideos, videoId, page, size, lat, long, jwt)
    yield put({type: GET_RELATED_VIDEOS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_RELATED_VIDEOS_FAILED, e})
  }
}

function* watchGetRelatedVideos() {
  while (true) {
    const {videoId, page, size, lat, long, jwt} = yield take(GET_RELATED_VIDEOS_REQUEST);
    yield fork(getRelatedVideos, videoId, page, size, lat, long, jwt);
  }
}

function* getCategoryVideos(action) {
  try {
    const {categoryId, page, size, jwt} = action;
    const response = yield call(videoApi.getCategoryVideos, categoryId, page, size, jwt)
    yield put({type: GET_CATEGORY_VIDEOS_SUCCESS, response})
    yield put(batchGetVideosSuccess(response))
    return response
  } catch(e) {
    yield put({type: GET_CATEGORY_VIDEOS_FAILED, e})
  }
}

function* watchGetCategoryVideos() {
  yield takeEvery(GET_CATEGORY_VIDEOS_REQUEST, getCategoryVideos);
}

function* getCategories() {
  try {
    const response = yield call(videoApi.getCategories)
    yield put({type: GET_CATEGORIES_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_CATEGORIES_FAILED, e})
  }
}

function* watchGetCategories() {
  while (true) {
    yield take(GET_CATEGORIES_REQUEST);
    yield fork(getCategories);
  }
}

function* getStaffPicks(jwt) {
  try {
    const response = yield call(videoApi.getStaffPicks, jwt)
    yield put({type: GET_STAFF_PICKS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_STAFF_PICKS_FAILED, e})
  }
}

function* watchGetStaffPicks() {
  while (true) {
    const {jwt} = yield take(GET_STAFF_PICKS_REQUEST);
    yield fork(getStaffPicks, jwt);
  }
}

function* getDiscover(page, size) {
  try {
    const response = yield call(videoApi.getDiscover, page, size)
    yield put({type: GET_DISCOVER_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_DISCOVER_FAILED, e})
  }
}

function* watchGetDiscover() {
  while (true) {
    const {page, size} = yield take(GET_DISCOVER_REQUEST);
    yield fork(getDiscover, page, size);
  }
}

function* loadMoreFreshVideos(url, jwt) {
  try {
    const response = yield call(videoApi.loadMoreVideos, url, jwt)
    yield put({type: LOAD_MORE_FRESH_VIDEOS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: LOAD_MORE_FRESH_VIDEOS_FAILED, e})
  }
}

function* watchLoadMoreFreshVideos() {
  while (true) {
    const {url, jwt} = yield take(LOAD_MORE_FRESH_VIDEOS_REQUEST);
    yield fork(loadMoreFreshVideos, url, jwt);
  }
}

function* startView(videoId, timestamp, live, jwt) {
  try {
    const response = yield call(videoApi.startView, videoId, timestamp, live, jwt)
    yield put({type: START_VIEW_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: START_VIEW_FAILED, e})
  }
}

function* watchStartView() {
  while (true) {
    const {videoId, timestamp, live, jwt} = yield take(START_VIEW_REQUEST);
    yield fork(startView, videoId, timestamp, live, jwt);
  }
}

function* endView(videoId, jwt) {
  try {
    const response = yield call(videoApi.endView, videoId, jwt)
    yield put({type: END_VIEW_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: END_VIEW_FAILED, e})
  }
}

function* watchEndView() {
  while (true) {
    const {videoId, jwt} = yield take(END_VIEW_REQUEST);
    yield fork(endView, videoId, jwt);
  }
}

function* getAgoraKey(videoId) {
  try {
    const response = yield call(videoApi.getAgoraKey, videoId)
    yield put({type: GET_AGORA_KEY_SUCCESS, response})

    yield put({type: LOAD_UP_VIDEO_PAGE_SUCCESS})
    return response
  } catch(e) {
    yield put({type: GET_AGORA_KEY_FAILED, e})
  }
}

function* watchGetAgoraKey() {
  while (true) {
    const {videoId} = yield take(GET_AGORA_KEY_REQUEST);

    console.log("videoId==", videoId)
    yield fork(getAgoraKey, videoId);
  }
}

function* getCurrentViewers(videoId, page, size, jwt) {
  try {
    const response = yield call(videoApi.getCurrentViewers, videoId, page, size, jwt)
    yield put({type: GET_CURRENT_VIEWERS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_CURRENT_VIEWERS_FAILED, e})
  }
}

function* watchGetCurrentViewers() {
  while (true) {
    const {videoId, page, size, jwt} = yield take(GET_CURRENT_VIEWERS_REQUEST);
    yield fork(getCurrentViewers, videoId, page, size, jwt);
  }
}

function* getAnnotations(videoId) {
  try {
    const response = yield call(videoApi.getAnnotations, videoId)
    yield put({type: GET_ANNOTATIONS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_ANNOTATIONS_FAILED, e})
  }
}

function* watchGetAnnotations() {
  while (true) {
    const {videoId} = yield take(GET_ANNOTATIONS_REQUEST);
    yield fork(getAnnotations, videoId);
  }
}

function* getPaidContent(videoId, jwt) {
  try {
    const response = yield call(videoApi.getPaidContent, videoId, jwt)
    yield put({type: GET_PAID_CONTENT_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_PAID_CONTENT_FAILED, e})
  }
}

function* watchGetPaidContent() {
  while (true) {
    const {videoId, jwt} = yield take(GET_PAID_CONTENT_REQUEST);
    yield fork(getPaidContent, videoId, jwt);
  }
}

function* getVideosByUser(username, page, size) {
  try {
    const response = yield call(videoApi.getVideosByUser, encodeURIComponent(username), page, size)
    yield put({type: GET_VIDEOS_BY_USER_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_VIDEOS_BY_USER_FAILED, e})
  }
}

function* watchGetVideosByUser() {
  while (true) {
    const {username, page, size} = yield take(GET_VIDEOS_BY_USER_REQUEST);
    yield fork(getVideosByUser, username, page, size);
  }
}

function* loadMoreVideosByUser(next) {
  try {
    const response = yield call(videoApi.loadMoreVideos, next)
    yield put({type: LOAD_MORE_VIDEOS_BY_USER_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: LOAD_MORE_VIDEOS_BY_USER_FAILED, e})
  }
}

function* watchLoadMoreVideosByUser() {
  while (true) {
    const {next} = yield take(LOAD_MORE_VIDEOS_BY_USER_REQUEST);
    yield fork(loadMoreVideosByUser, next);
  }
}

export default function* videoSagas() {
  yield fork(watchGetBanners);
  yield fork(watchGetCollections);
  yield fork(watchGetAllVideos);
  yield fork(watchGetSingleVideo);
  yield fork(watchDeleteSingleVideo);
  yield fork(watchGetSingleVideoStats);
  yield fork(watchGetComments);
  yield fork(watchPostComment);
  yield fork(watchLoadMoreComments);
  yield fork(watchGetRelatedVideos);
  yield fork(watchGetCategoryVideos);
  yield fork(watchGetCategories);
  yield fork(watchGetStaffPicks);
  yield fork(watchGetDiscover);
  yield fork(watchLoadMoreFreshVideos);
  yield fork(watchStartView);
  yield fork(watchEndView);
  yield fork(watchGetAgoraKey);
  yield fork(watchGetCurrentViewers);
  yield fork(watchGetAnnotations);
  yield fork(watchGetPaidContent);
  yield fork(watchGetVideosByUser);
  yield fork(watchLoadMoreVideosByUser);
}