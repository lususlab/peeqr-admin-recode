import {fromJS, Map} from 'immutable';
import {
  GET_BANNERS_REQUEST,
  GET_BANNERS_SUCCESS,
  GET_BANNERS_FAILED,

  GET_COLLECTIONS_REQUEST,
  GET_COLLECTIONS_SUCCESS,
  GET_COLLECTIONS_FAILED,

  GET_ALL_VIDEOS_REQUEST,
  GET_ALL_VIDEOS_SUCCESS,
  GET_ALL_VIDEOS_FAILED,

  GET_SINGLE_VIDEO_REQUEST,
  GET_SINGLE_VIDEO_SUCCESS,
  GET_SINGLE_VIDEO_FAILED,

  DELETE_SINGLE_VIDEO_REQUEST,
  DELETE_SINGLE_VIDEO_SUCCESS,
  DELETE_SINGLE_VIDEO_FAILED,

  GET_SINGLE_VIDEO_STATS_REQUEST,
  GET_SINGLE_VIDEO_STATS_SUCCESS,
  GET_SINGLE_VIDEO_STATS_FAILED,

  GET_COMMENTS_REQUEST,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAILED,

  POST_COMMENT_REQUEST,
  POST_COMMENT_SUCCESS,
  POST_COMMENT_FAILED,

  LOAD_MORE_COMMENTS_REQUEST,
  LOAD_MORE_COMMENTS_SUCCESS,
  LOAD_MORE_COMMENTS_FAILED,

  GET_RELATED_VIDEOS_REQUEST,
  GET_RELATED_VIDEOS_SUCCESS,
  GET_RELATED_VIDEOS_FAILED,

  GET_CATEGORY_VIDEOS_REQUEST,
  GET_CATEGORY_VIDEOS_SUCCESS,
  GET_CATEGORY_VIDEOS_FAILED,

  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILED,

  GET_STAFF_PICKS_REQUEST, 
  GET_STAFF_PICKS_SUCCESS,
  GET_STAFF_PICKS_FAILED,

  GET_DISCOVER_REQUEST,
  GET_DISCOVER_SUCCESS,
  GET_DISCOVER_FAILED,

  SHOW_MODAL,
  HIDE_MODAL,

  LOAD_MORE_FRESH_VIDEOS_REQUEST,
  LOAD_MORE_FRESH_VIDEOS_SUCCESS,
  LOAD_MORE_FRESH_VIDEOS_FAILED,

  START_VIEW_REQUEST,
  START_VIEW_SUCCESS,
  START_VIEW_FAILED,

  END_VIEW_REQUEST,
  END_VIEW_SUCCESS,
  END_VIEW_FAILED,

  GET_AGORA_KEY_REQUEST,
  GET_AGORA_KEY_SUCCESS,
  GET_AGORA_KEY_FAILED,

  GET_CURRENT_VIEWERS_REQUEST,
  GET_CURRENT_VIEWERS_SUCCESS,
  GET_CURRENT_VIEWERS_FAILED,
  
  GET_ANNOTATIONS_REQUEST,
  GET_ANNOTATIONS_SUCCESS,
  GET_ANNOTATIONS_FAILED,

  GET_PAID_CONTENT_REQUEST,
  GET_PAID_CONTENT_SUCCESS,
  GET_PAID_CONTENT_FAILED,

  GET_VIDEOS_BY_USER_REQUEST,
  GET_VIDEOS_BY_USER_SUCCESS,
  GET_VIDEOS_BY_USER_FAILED,

  LOAD_MORE_VIDEOS_BY_USER_REQUEST,
  LOAD_MORE_VIDEOS_BY_USER_SUCCESS,
  LOAD_MORE_VIDEOS_BY_USER_FAILED,
  
  SAVE_AGORA_CHANNEL_KEY

} from "store/video/actions";

const INITIAL_STATE = fromJS({ 
  gettingBanners: false,
  banners: {},
  gettingCollections: false,
  collections: {},
  gettingFreshVideos: false,
  freshVideos: {},
  gettingVideo: false,
  currentVideo: {},
  gettingVideoStats: false,
  currentVideoStats: {},
  gettingRelatedVideos: false,
  relatedVideos: {},
  gettingCategoryVideos: false,
  gettingCategories: false,
  categories: {},
  gettingStaffPicks: false,
  staffPicks: {},
  gettingDiscover: false,
  discover: {},
  showModal: false,
  modalDetails: {},
  modalType: '',
  modalJwt: '',
  loadingMore: false,
  subscribing: false,
  gettingComments: false,
  commenting: false,
  comments: {},
  loadingMoreComments: false,
  startingView: false,
  endingView: false,
  gettingAgoraKey: false,
  agoraKey: {},
  gettingCurrentViewers: false,
  currentViewers: {},
  channelKey: '',
  AgoraRTC: null,
  error: '',
  gettingAnnotations: false,
  annotations: {},
  gettingPaidContent: false,
  paidContent: {},
  gettingUserVideos: false,
  userVideos: {},
  deleting: false,
});

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_BANNERS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingBanners: true 
      });
    case GET_BANNERS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingBanners: false,
        banners: action.response.data,
      });
    case GET_BANNERS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingBanners: false
      });
    case GET_COLLECTIONS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingCollections: true 
      });
    case GET_COLLECTIONS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingCollections: false,
        collections: action.response.data,
      });
    case GET_COLLECTIONS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingCollections: false
      });
    case GET_ALL_VIDEOS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingFreshVideos: true 
      });
    case GET_ALL_VIDEOS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingFreshVideos: false,
        freshVideos: action.response.data,
      });
    case GET_ALL_VIDEOS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingFreshVideos: false
      });
    case GET_SINGLE_VIDEO_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingVideo: true,
        error: ''
      });
    case GET_SINGLE_VIDEO_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingVideo: false,
        currentVideo: action.response.data,
      });
    case GET_SINGLE_VIDEO_FAILED:
      return state.merge ({ 
        ...state, 
        gettingVideo: false,
        error: action.e.response.data.detail.toString()
      });
    case DELETE_SINGLE_VIDEO_REQUEST:
      return state.merge ({ 
        ...state, 
        deleting: true,
        error: ''
      });
    case DELETE_SINGLE_VIDEO_SUCCESS:
      return state.merge ({ 
        ...state, 
        deleting: false,
      });
    case DELETE_SINGLE_VIDEO_FAILED:
      return state.merge ({ 
        ...state, 
        deleting: false,
        error: action.e
      });
    case GET_SINGLE_VIDEO_STATS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingVideoStats: true 
      });
    case GET_SINGLE_VIDEO_STATS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingVideoStats: false,
        currentVideoStats: {
          like: action.response.data.like,
          live_concurrent_count: action.response.data.live_concurrent_count,
          live_view_count: action.response.data.live_view_count,
          view_count: action.response.data.view_count,
          max_concurrent_count: action.response.data.max_concurrent_count,
          alltime_pearl_qty: action.response.data.user.alltime_pearl_qty,
          upload_date: action.response.data.upload_date,
        }
      });
    case GET_SINGLE_VIDEO_STATS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingVideoStats: false
      });
    case GET_COMMENTS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingComments: true 
      });
    case GET_COMMENTS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingComments: false,
        comments: action.response.data,
      });
    case GET_COMMENTS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingComments: false
      });
    case POST_COMMENT_REQUEST:
      return state.merge ({ 
        ...state, 
        commenting: true 
      });
    case POST_COMMENT_SUCCESS:
      return state.merge ({ 
        ...state, 
        commenting: false,
      });
    case POST_COMMENT_FAILED:
      return state.merge ({ 
        ...state, 
        commenting: false
      });
    case LOAD_MORE_COMMENTS_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingMoreComments: true 
      });
    case LOAD_MORE_COMMENTS_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingMoreComments: false,
        comments: { ...action.response.data, results: state.getIn(['comments', 'results']).concat(fromJS(action.response.data.results)) },
      });
    case LOAD_MORE_COMMENTS_FAILED:
      return state.merge ({ 
        ...state, 
        loadingMoreComments: false
      });
    case GET_RELATED_VIDEOS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingRelatedVideos: true 
      });
    case GET_RELATED_VIDEOS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingRelatedVideos: false,
        relatedVideos: action.response.data.data
      });
    case GET_RELATED_VIDEOS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingRelatedVideos: false
      });
    case GET_CATEGORY_VIDEOS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingCategoryVideos: true 
      });
    case GET_CATEGORY_VIDEOS_SUCCESS:
      return state.merge({ 
        ...state, 
        gettingCategoryVideos: false,
      });
    case GET_CATEGORY_VIDEOS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingCategoryVideos: false
      });
    case GET_CATEGORIES_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingCategories: true 
      });
    case GET_CATEGORIES_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingCategories: false,
        categories: action.response.data,
      });
    case GET_CATEGORIES_FAILED:
      return state.merge ({ 
        ...state, 
        gettingCategories: false
      });
    case GET_STAFF_PICKS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingStaffPicks: true 
      });
    case GET_STAFF_PICKS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingStaffPicks: false,
        staffPicks: action.response.data,
      });
    case GET_STAFF_PICKS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingStaffPicks: false
      });
    case GET_DISCOVER_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingDiscover: true 
      });
    case GET_DISCOVER_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingDiscover: false,
        discover: action.response.results,
      });
    case GET_DISCOVER_FAILED:
      return state.merge ({ 
        ...state, 
        gettingDiscover: false
      });
    case SHOW_MODAL:
      return state.merge ({ 
        showModal: true,
        modalDetails: action.item,
        modalType: action.modalType,
        modalJwt: action.jwt,
      });
    case HIDE_MODAL:
      return state.merge ({ 
        showModal: false,
        modalDetails: {},
        modalType: '',
        modalJwt: '',
      });
    case LOAD_MORE_FRESH_VIDEOS_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingMore: true 
      });
    case LOAD_MORE_FRESH_VIDEOS_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingMore: false,
        freshVideos: { ...action.response.data, results: state.getIn(['freshVideos', 'results']).concat(fromJS(action.response.data.results)) },
      });
    case LOAD_MORE_FRESH_VIDEOS_FAILED:
      return state.merge ({ 
        ...state, 
        loadingMore: false
      });
    case START_VIEW_REQUEST:
      return state.merge ({ 
        ...state, 
        startingView: true 
      });
    case START_VIEW_SUCCESS:
      return state.merge ({ 
        ...state, 
        startingView: false,
      });
    case START_VIEW_FAILED:
      return state.merge ({ 
        ...state, 
        startingView: false
      });
    case END_VIEW_REQUEST:
      return state.merge ({ 
        ...state, 
        endingView: true 
      });
    case END_VIEW_SUCCESS:
      return state.merge ({ 
        ...state, 
        endingView: false,
      });
    case END_VIEW_FAILED:
      return state.merge ({ 
        ...state, 
        endingView: false
      });
    case GET_AGORA_KEY_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingAgoraKey: true 
      });
    case GET_AGORA_KEY_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingAgoraKey: false,
        agoraKey: action.response.data
      });
    case GET_AGORA_KEY_FAILED:
      return state.merge ({ 
        ...state, 
        gettingAgoraKey: false
      });
    case GET_CURRENT_VIEWERS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingCurrentViewers: true 
      });
    case GET_CURRENT_VIEWERS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingCurrentViewers: false,
        currentViewers: action.response.data
      });
    case GET_CURRENT_VIEWERS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingCurrentViewers: false
      });
    case SAVE_AGORA_CHANNEL_KEY:
      return state.merge ({ 
        ...state, 
        channelKey: action.channelKey,
        AgoraRTC: action.AgoraRTC
      });
    case GET_ANNOTATIONS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingAnnotations: true 
      });
    case GET_ANNOTATIONS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingAnnotations: false,
        annotations: action.response.data
      });
    case GET_ANNOTATIONS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingAnnotations: false
      });
    case GET_PAID_CONTENT_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingPaidContent: true 
      });
    case GET_PAID_CONTENT_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingPaidContent: false,
        paidContent: action.response.data
      });
    case GET_PAID_CONTENT_FAILED:
      return state.merge ({ 
        ...state, 
        gettingPaidContent: false
      });
    case GET_VIDEOS_BY_USER_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingUserVideos: true 
      });
    case GET_VIDEOS_BY_USER_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingUserVideos: false,
        userVideos: action.response.data
      });
    case GET_VIDEOS_BY_USER_FAILED:
      return state.merge ({ 
        ...state, 
        gettingUserVideos: false
      });
    case LOAD_MORE_VIDEOS_BY_USER_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingMore: true 
      });
    case LOAD_MORE_VIDEOS_BY_USER_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingMore: false,
        userVideos: { ...action.response.data, results: state.getIn(['userVideos', 'results']).concat(fromJS(action.response.data.results)) },
      });
    case LOAD_MORE_VIDEOS_BY_USER_FAILED:
      return state.merge ({ 
        ...state, 
        loadingMore: false
      });
    default:
      return state;
  }
}