export const getVideoEntities = state => state.getIn(['entities', 'videos']);

export const getCategoryVideos = (videos, categoryId) => {
  const videosObj = videos.filter(video => {
    if (video.getIn(['category', 0, 'id']) === categoryId) {
      return video.getIn(['category', 0, 'id']) === categoryId
    }
  })
  if (videosObj) {
    return videosObj.toList()
  }
  return List([])
}

export const getVideos = (videos, videoIds) => {
  if (videoIds.size > 0) {
    const videosList = videos.filter((v, k) => videoIds.includes(k))
    return videosList.toArray()
  }
  return List([])
}