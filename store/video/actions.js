import { videosListNormalizer } from 'store/schema';

export const GET_BANNERS_REQUEST = "GET_BANNERS_REQUEST";
export const GET_BANNERS_SUCCESS = "GET_BANNERS_SUCCESS";
export const GET_BANNERS_FAILED = "GET_BANNERS_FAILED";

export const GET_COLLECTIONS_REQUEST = "GET_COLLECTIONS_REQUEST";
export const GET_COLLECTIONS_SUCCESS = "GET_COLLECTIONS_SUCCESS";
export const GET_COLLECTIONS_FAILED = "GET_COLLECTIONS_FAILED"

export const GET_ALL_VIDEOS_REQUEST = "GET_ALL_VIDEOS_REQUEST";
export const GET_ALL_VIDEOS_SUCCESS = "GET_ALL_VIDEOS_SUCCESS";
export const GET_ALL_VIDEOS_FAILED = "GET_ALL_VIDEOS_FAILED";

export const GET_SINGLE_VIDEO_REQUEST = "GET_SINGLE_VIDEO_REQUEST";
export const GET_SINGLE_VIDEO_SUCCESS = "GET_SINGLE_VIDEO_SUCCESS";
export const GET_SINGLE_VIDEO_FAILED = "GET_SINGLE_VIDEO_FAILED";

export const DELETE_SINGLE_VIDEO_REQUEST = "DELETE_SINGLE_VIDEO_REQUEST";
export const DELETE_SINGLE_VIDEO_SUCCESS = "DELETE_SINGLE_VIDEO_SUCCESS";
export const DELETE_SINGLE_VIDEO_FAILED = "DELETE_SINGLE_VIDEO_FAILED";

export const GET_SINGLE_VIDEO_STATS_REQUEST = "GET_SINGLE_VIDEO_STATS_REQUEST";
export const GET_SINGLE_VIDEO_STATS_SUCCESS = "GET_SINGLE_VIDEO_STATS_SUCCESS";
export const GET_SINGLE_VIDEO_STATS_FAILED = "GET_SINGLE_VIDEO_STATS_FAILED";

export const GET_COMMENTS_REQUEST = "GET_COMMENTS_REQUEST";
export const GET_COMMENTS_SUCCESS = "GET_COMMENTS_SUCCESS";
export const GET_COMMENTS_FAILED = "GET_COMMENTS_FAILED";

export const POST_COMMENT_REQUEST = "POST_COMMENT_REQUEST";
export const POST_COMMENT_SUCCESS = "POST_COMMENT_SUCCESS";
export const POST_COMMENT_FAILED = "POST_COMMENT_FAILED";

export const LOAD_MORE_COMMENTS_REQUEST = "LOAD_MORE_COMMENTS_REQUEST";
export const LOAD_MORE_COMMENTS_SUCCESS = "LOAD_MORE_COMMENTS_SUCCESS";
export const LOAD_MORE_COMMENTS_FAILED = "LOAD_MORE_COMMENTS_FAILED";

export const GET_RELATED_VIDEOS_REQUEST = "GET_RELATED_VIDEOS_REQUEST";
export const GET_RELATED_VIDEOS_SUCCESS = "GET_RELATED_VIDEOS_SUCCESS";
export const GET_RELATED_VIDEOS_FAILED = "GET_RELATED_VIDEOS_FAILED";

export const GET_CATEGORY_VIDEOS_REQUEST = "GET_CATEGORY_VIDEOS_REQUEST";
export const GET_CATEGORY_VIDEOS_SUCCESS = "GET_CATEGORY_VIDEOS_SUCCESS";
export const GET_CATEGORY_VIDEOS_FAILED = "GET_CATEGORY_VIDEOS_FAILED";

export const GET_CATEGORIES_REQUEST = "GET_CATEGORIES_REQUEST";
export const GET_CATEGORIES_SUCCESS = "GET_CATEGORIES_SUCCESS";
export const GET_CATEGORIES_FAILED = "GET_CATEGORIES_FAILED";

export const GET_STAFF_PICKS_REQUEST = "GET_STAFF_PICKS_REQUEST";
export const GET_STAFF_PICKS_SUCCESS = "GET_STAFF_PICKS_SUCCESS";
export const GET_STAFF_PICKS_FAILED = "GET_STAFF_PICKS_FAILED";

export const GET_DISCOVER_REQUEST = "GET_DISCOVER_REQUEST";
export const GET_DISCOVER_SUCCESS = "GET_DISCOVER_SUCCESS";
export const GET_DISCOVER_FAILED = "GET_DISCOVER_FAILED";

export const SHOW_MODAL = "SHOW_MODAL";
export const HIDE_MODAL = "HIDE_MODAL";

export const LOAD_MORE_FRESH_VIDEOS_REQUEST = "LOAD_MORE_FRESH_VIDEOS_REQUEST";
export const LOAD_MORE_FRESH_VIDEOS_SUCCESS = "LOAD_MORE_FRESH_VIDEOS_SUCCESS";
export const LOAD_MORE_FRESH_VIDEOS_FAILED = "LOAD_MORE_FRESH_VIDEOS_FAILED";

export const BATCH_GET_VIDEOS_REQUEST = "BATCH_GET_VIDEOS_REQUEST";
export const BATCH_GET_VIDEOS_SUCCESS = "BATCH_GET_VIDEOS_SUCCESS";
export const BATCH_GET_VIDEOS_FAILED = "BATCH_GET_VIDEOS_FAILED";

export const START_VIEW_REQUEST = "START_VIEW_REQUEST";
export const START_VIEW_SUCCESS = "START_VIEW_SUCCESS";
export const START_VIEW_FAILED = "START_VIEW_FAILED";

export const END_VIEW_REQUEST = "END_VIEW_REQUEST";
export const END_VIEW_SUCCESS = "END_VIEW_SUCCESS";
export const END_VIEW_FAILED = "END_VIEW_FAILED";

export const GET_AGORA_KEY_REQUEST = 'GET_AGORA_KEY_REQUEST';
export const GET_AGORA_KEY_SUCCESS = 'GET_AGORA_KEY_SUCCESS';
export const GET_AGORA_KEY_FAILED = 'GET_AGORA_KEY_FAILED';

export const GET_CURRENT_VIEWERS_REQUEST = 'GET_CURRENT_VIEWERS_REQUEST';
export const GET_CURRENT_VIEWERS_SUCCESS = 'GET_CURRENT_VIEWERS_SUCCESS';
export const GET_CURRENT_VIEWERS_FAILED = 'GET_CURRENT_VIEWERS_FAILED';

export const GET_ANNOTATIONS_REQUEST = "GET_ANNOTATIONS_REQUEST";
export const GET_ANNOTATIONS_SUCCESS = "GET_ANNOTATIONS_SUCCESS";
export const GET_ANNOTATIONS_FAILED = "GET_ANNOTATIONS_FAILED";

export const GET_PAID_CONTENT_REQUEST = "GET_PAID_CONTENT_REQUEST";
export const GET_PAID_CONTENT_SUCCESS = "GET_PAID_CONTENT_SUCCESS";
export const GET_PAID_CONTENT_FAILED = "GET_PAID_CONTENT_FAILED";

export const GET_VIDEOS_BY_USER_REQUEST = "GET_VIDEOS_BY_USER_REQUEST";
export const GET_VIDEOS_BY_USER_SUCCESS = "GET_VIDEOS_BY_USER_SUCCESS";
export const GET_VIDEOS_BY_USER_FAILED = "GET_VIDEOS_BY_USER_FAILED";

export const LOAD_MORE_VIDEOS_BY_USER_REQUEST = "LOAD_MORE_VIDEOS_BY_USER_REQUEST";
export const LOAD_MORE_VIDEOS_BY_USER_SUCCESS = "LOAD_MORE_VIDEOS_BY_USER_SUCCESS";
export const LOAD_MORE_VIDEOS_BY_USER_FAILED = "LOAD_MORE_VIDEOS_BY_USER_FAILED";

export const SAVE_AGORA_CHANNEL_KEY = "SAVE_AGORA_CHANNEL_KEY";

export const getBannersRequest = () => ({ type: GET_BANNERS_REQUEST });

export const getCollectionsRequest = () => ({ type: GET_COLLECTIONS_REQUEST })

export const getAllVideosRequest = (page, size, jwt) => ({type: GET_ALL_VIDEOS_REQUEST, page, size, jwt})

export const getSingleVideoRequest = (videoId, jwt) => ({type: GET_SINGLE_VIDEO_REQUEST, videoId, jwt})

export const deleteSingleVideoRequest = (videoId, username, jwt, page, size) => ({type: DELETE_SINGLE_VIDEO_REQUEST, videoId, username, jwt, page, size})

export const getSingleVideoStatsRequest = (videoId) => ({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})

export const getCommentsRequest = (videoId, page, size) => ({type: GET_COMMENTS_REQUEST, videoId, page, size})

export const postCommentRequest = (videoId, userComment, timestamp, jwt) => ({type: POST_COMMENT_REQUEST, videoId, userComment, timestamp, jwt})

export const loadMoreCommentsRequest = (url) => ({type: LOAD_MORE_COMMENTS_REQUEST, url})

export const getRelatedVideosRequest = (videoId, page, size, lat, long, jwt) => ({ type: GET_RELATED_VIDEOS_REQUEST, videoId, page, size, lat, long, jwt });

export const getCategoriesRequest = () => ({ type: GET_CATEGORIES_REQUEST });

export const getStaffPicksRequest = (jwt) => ({ type: GET_STAFF_PICKS_REQUEST, jwt });

export const getDiscoverRequest = (page, size) => ({ type: GET_DISCOVER_REQUEST, page, size });

export const showModal = (item, modalType, jwt) => ({ type: SHOW_MODAL, item, modalType, jwt });

export const hideModal = () => ({ type: HIDE_MODAL });

export const loadMoreFreshVideosRequest = (url, jwt) => ({ type: LOAD_MORE_FRESH_VIDEOS_REQUEST, url, jwt })

export const getCategoryVideosRequest = (categoryId, page, size, jwt) => ({ type: GET_CATEGORY_VIDEOS_REQUEST, categoryId, page, size, jwt })

export const batchGetVideosSuccess = response => ({ 
  type: BATCH_GET_VIDEOS_SUCCESS, 
  payload: videosListNormalizer({...response.data.results})
})

export const startViewRequest = (videoId, timestamp, live, jwt) => ({ type: START_VIEW_REQUEST, videoId, timestamp, live, jwt })

export const endViewRequest = (videoId, jwt) => ({ type: END_VIEW_REQUEST, videoId, jwt })

export const getAgoraKeyRequest = videoId => ({ type: GET_AGORA_KEY_REQUEST, videoId })

export const getCurrentViewersRequest = (videoId, page, size, jwt) => ({ type: GET_CURRENT_VIEWERS_REQUEST, videoId, page, size, jwt })

export const saveAgoraChannelKey = (channelKey, AgoraRTC) => ({ type: SAVE_AGORA_CHANNEL_KEY, channelKey, AgoraRTC })

export const getAnnotationsRequest = videoId => ({ type: GET_ANNOTATIONS_REQUEST, videoId })

export const getPaidContentRequest = (videoId, jwt) => ({ type: GET_PAID_CONTENT_REQUEST, videoId, jwt })

export const getVideosByUserRequest = (username, page, size) => ({ type: GET_VIDEOS_BY_USER_REQUEST, username, page, size })

export const loadMoreVideosByUserRequest = next => ({ type: LOAD_MORE_VIDEOS_BY_USER_REQUEST, next })