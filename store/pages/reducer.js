import {fromJS} from 'immutable';
import {
  LOAD_UP_HOME_PAGE_REQUEST,
  LOAD_UP_HOME_PAGE_SUCCESS,
  LOAD_UP_HOME_PAGE_FAILED,

  LOAD_UP_VIDEO_PAGE_REQUEST,
  LOAD_UP_VIDEO_PAGE_SUCCESS,
  LOAD_UP_VIDEO_PAGE_FAILED,

  LOAD_UP_PROFILE_PAGE_REQUEST,
  LOAD_UP_PROFILE_PAGE_SUCCESS,
  LOAD_UP_PROFILE_PAGE_FAILED,

} from "store/pages/actions";

const INITIAL_STATE = fromJS({ 
  loadingHomePage: false,
  loadingVideoPage: false,
  loadingProfilePage: false,
  error: ''
})

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_UP_HOME_PAGE_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingHomePage: true 
      });
    case LOAD_UP_HOME_PAGE_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingHomePage: false,
      });
    case LOAD_UP_HOME_PAGE_FAILED:
      return state.merge ({ 
        ...state, 
        loadingHomePage: false,
        error: action.e
      });
    case LOAD_UP_VIDEO_PAGE_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingVideoPage: true 
      });
    case LOAD_UP_VIDEO_PAGE_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingVideoPage: false,
      });
    case LOAD_UP_VIDEO_PAGE_FAILED:
      return state.merge ({ 
        ...state, 
        loadingVideoPage: false,
        error: action.e
      });
    case LOAD_UP_PROFILE_PAGE_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingProfilePage: true 
      });
    case LOAD_UP_PROFILE_PAGE_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingProfilePage: false,
      });
    case LOAD_UP_PROFILE_PAGE_FAILED:
      return state.merge ({ 
        ...state, 
        loadingProfilePage: false,
        error: action.e
      });
    default:
      return state;
  }
}
