export const LOAD_UP_HOME_PAGE_REQUEST = "LOAD_UP_HOME_PAGE_REQUEST";
export const LOAD_UP_HOME_PAGE_SUCCESS = "LOAD_UP_HOME_PAGE_SUCCESS";
export const LOAD_UP_HOME_PAGE_FAILED = "LOAD_UP_HOME_PAGE_FAILED";

export const LOAD_UP_VIDEO_PAGE_REQUEST = "LOAD_UP_VIDEO_PAGE_REQUEST";
export const LOAD_UP_VIDEO_PAGE_SUCCESS = "LOAD_UP_VIDEO_PAGE_SUCCESS";
export const LOAD_UP_VIDEO_PAGE_FAILED = "LOAD_UP_VIDEO_PAGE_FAILED";

export const LOAD_UP_PROFILE_PAGE_REQUEST = "LOAD_UP_PROFILE_PAGE_REQUEST";
export const LOAD_UP_PROFILE_PAGE_SUCCESS = "LOAD_UP_PROFILE_PAGE_SUCCESS";
export const LOAD_UP_PROFILE_PAGE_FAILED = "LOAD_UP_PROFILE_PAGE_FAILED";

export const loadUpHomePageRequest = (page, size, jwt) => ({ type: LOAD_UP_HOME_PAGE_REQUEST, page, size, jwt })

export const loadUpVideoPageRequest = (videoId, page, size, viewerSize, lat, long, jwt) => ({ type: LOAD_UP_VIDEO_PAGE_REQUEST, videoId, page, size, viewerSize, lat, long, jwt })

export const loadUpProfilePageRequest = (username, page, size, jwt) => ({ type: LOAD_UP_PROFILE_PAGE_REQUEST, username, page, size, jwt })
