import { take, fork, call, put, all } from "redux-saga/effects";

import {
  LOAD_UP_HOME_PAGE_REQUEST,
  LOAD_UP_HOME_PAGE_SUCCESS,
  LOAD_UP_HOME_PAGE_FAILED,

  LOAD_UP_VIDEO_PAGE_REQUEST,
  LOAD_UP_VIDEO_PAGE_SUCCESS,
  LOAD_UP_VIDEO_PAGE_FAILED,

  LOAD_UP_PROFILE_PAGE_REQUEST,
  LOAD_UP_PROFILE_PAGE_SUCCESS,
  LOAD_UP_PROFILE_PAGE_FAILED,
  
} from "store/pages/actions";

import {
  GET_BANNERS_REQUEST,
  GET_COLLECTIONS_REQUEST,
  GET_ALL_VIDEOS_REQUEST,
  GET_CATEGORIES_REQUEST,
  GET_STAFF_PICKS_REQUEST,
  
  GET_RELATED_VIDEOS_REQUEST,
  GET_SINGLE_VIDEO_STATS_REQUEST,
  GET_COMMENTS_REQUEST,
  GET_CURRENT_VIEWERS_REQUEST,
  GET_SINGLE_VIDEO_REQUEST,

  GET_VIDEOS_BY_USER_REQUEST
} from "store/video/actions";

import {
  GET_CURRENT_VIDEO_SELF_STATS_REQUEST,
  GET_SELF_REQUEST,
  GET_PUBLIC_PROFILE_REQUEST,
  GET_MUTED_LIST_REQUEST
} from "store/user/actions"

function* loadUpHomePage(page, size, jwt) {
  try {
    yield put({type: GET_BANNERS_REQUEST})
    yield put({type: GET_COLLECTIONS_REQUEST})
    yield put({type: GET_ALL_VIDEOS_REQUEST, page, size, jwt})
    yield put({type: GET_CATEGORIES_REQUEST, jwt})
    yield put({type: GET_STAFF_PICKS_REQUEST, jwt})

    yield put({type: LOAD_UP_HOME_PAGE_SUCCESS})
  } catch(e) {
    yield put({type: LOAD_UP_HOME_PAGE_FAILED, e})
  }
}

function* watchLoadUpHomePage() {
  while (true) {
    const {page, size, jwt} = yield take(LOAD_UP_HOME_PAGE_REQUEST);
    yield fork(loadUpHomePage, page, size, jwt);
  }
}

function* loadUpVideoPage(videoId, page, size, viewerSize, lat, long, jwt) {
  try {
    yield put({type: GET_RELATED_VIDEOS_REQUEST, videoId, page, size, lat, long, jwt})
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    yield put({type: GET_COMMENTS_REQUEST, videoId, page, size})

    if (jwt) {
      yield put({type: GET_CURRENT_VIDEO_SELF_STATS_REQUEST, videoId, jwt })
      yield put({type: GET_CURRENT_VIEWERS_REQUEST, videoId, page, viewerSize, jwt })
      yield put({type: GET_MUTED_LIST_REQUEST, videoId, jwt})
    }

    yield put.resolve({type: GET_SINGLE_VIDEO_REQUEST, videoId, jwt})
    
  } catch(e) {
    yield put({type: LOAD_UP_VIDEO_PAGE_FAILED, e})
  }
}

function* watchLoadUpVideoPage() {
  while (true) {
    const {videoId, page, size, viewerSize, lat, long, jwt} = yield take(LOAD_UP_VIDEO_PAGE_REQUEST);
    yield fork(loadUpVideoPage, videoId, page, size, viewerSize, lat, long, jwt);
  }
}

function* loadUpProfilePage(username, page, size, jwt) {
  try {
    yield put.resolve({type: GET_PUBLIC_PROFILE_REQUEST, username, jwt })
    yield put.resolve({type: GET_VIDEOS_BY_USER_REQUEST, username, page, size})
    if (jwt) {
      yield put({type: GET_SELF_REQUEST, jwt })
    }
  } catch(e) {
    yield put({type: LOAD_UP_PROFILE_PAGE_FAILED, e})
  }
}

function* watchLoadUpProfilePage() {
  while (true) {
    const {username, page, size, jwt} = yield take(LOAD_UP_PROFILE_PAGE_REQUEST);
    yield fork(loadUpProfilePage, username, page, size, jwt);
  }
}

export default function* pageSagas() {
  yield fork(watchLoadUpHomePage);
  yield fork(watchLoadUpVideoPage);
  yield fork(watchLoadUpProfilePage);
}