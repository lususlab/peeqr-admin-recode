import {fromJS} from 'immutable';
import {
  GET_SEARCH_DROPDOWN_REQUEST,
  GET_SEARCH_DROPDOWN_SUCCESS,
  GET_SEARCH_DROPDOWN_FAILED,

  GET_SEARCH_REQUEST,
  GET_SEARCH_SUCCESS,
  GET_SEARCH_FAILED,

  SEARCH_INPUT_HIDE,
  SEARCH_INPUT_UNHIDE,

  CLEAR_RESULTS,
} from "store/search/actions";

const INITIAL_STATE = fromJS({ 
  loadingSearch: false,
  results: {},
  searchInputHide: false
});

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_SEARCH_DROPDOWN_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingSearch: true 
      });
    case GET_SEARCH_DROPDOWN_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingSearch: false,
        results: action.response.data,
      });
    case GET_SEARCH_DROPDOWN_FAILED:
      return state.merge ({ 
        ...state, 
        loadingSearch: false
      });
    case GET_SEARCH_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingSearch: true 
      });
    case GET_SEARCH_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingSearch: false,
        results: action.response.data,
      });
    case GET_SEARCH_FAILED:
      return state.merge ({ 
        ...state, 
        loadingSearch: false
      });
    case SEARCH_INPUT_HIDE:
      return state.merge ({ 
        ...state, 
        searchInputHide: true,
      });
    case SEARCH_INPUT_UNHIDE:
      return state.merge ({ 
        ...state, 
        searchInputHide: false
      });
    case CLEAR_RESULTS:
      return state.merge ({
        ...state,
        results: {}
      });
    default:
      return state;
  }
}