export const GET_SEARCH_DROPDOWN_REQUEST = "GET_SEARCH_DROPDOWN_REQUEST";
export const GET_SEARCH_DROPDOWN_SUCCESS = "GET_SEARCH_DROPDOWN_SUCCESS";
export const GET_SEARCH_DROPDOWN_FAILED = "GET_SEARCH_DROPDOWN_FAILED";

export const GET_SEARCH_REQUEST = "GET_SEARCH_REQUEST";
export const GET_SEARCH_SUCCESS = "GET_SEARCH_SUCCESS";
export const GET_SEARCH_FAILED = "GET_SEARCH_FAILED";

export const SEARCH_INPUT_HIDE = "SEARCH_INPUT_HIDE";
export const SEARCH_INPUT_UNHIDE = "SEARCH_INPUT_UNHIDE";

export const CLEAR_RESULTS = "CLEAR_RESULTS";

export const getSearchDropdownRequest = (search, size) => ({ type: GET_SEARCH_DROPDOWN_REQUEST, search, size });

export const getSearchRequest = (search, size) => ({ type: GET_SEARCH_REQUEST, search, size });

export const searchInputHide = () => ({ type: SEARCH_INPUT_HIDE })

export const searchInputUnhide = () => ({ type: SEARCH_INPUT_UNHIDE })

export const clearResults = () => ({ type: CLEAR_RESULTS })
