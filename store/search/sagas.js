import { take, fork, call, put } from "redux-saga/effects";
import searchApi from "services/api/search";

import {
  GET_SEARCH_DROPDOWN_REQUEST,
  GET_SEARCH_DROPDOWN_SUCCESS,
  GET_SEARCH_DROPDOWN_FAILED,

  GET_SEARCH_REQUEST,
  GET_SEARCH_SUCCESS,
  GET_SEARCH_FAILED,
} from "store/search/actions";

function* getSearchDropdown(search, size) {
  try {
    const response = yield call(searchApi.getSearchDropdown, search, size)
    yield put({type: GET_SEARCH_DROPDOWN_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_SEARCH_DROPDOWN_FAILED, e})
  }
}

function* watchGetSearchDropdown() {
  while (true) {
    const {search, size} = yield take(GET_SEARCH_DROPDOWN_REQUEST);
    yield fork(getSearchDropdown, search, size);
  }
}

function* getSearch(search, size) {
  try {
    const response = yield call(searchApi.getSearch, search, size)
    yield put({type: GET_SEARCH_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_SEARCH_FAILED, e})
  }
}

function* watchGetSearch() {
  while (true) {
    const {search, size} = yield take(GET_SEARCH_REQUEST);
    yield fork(getSearch, search, size);
  }
}

export default function* searchSagas() {
  yield fork(watchGetSearchDropdown);
  yield fork(watchGetSearch);
}