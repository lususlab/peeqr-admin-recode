import {fromJS} from 'immutable';
import {
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,

  EMAIL_LOGIN_REQUEST,
  EMAIL_LOGIN_SUCCESS,
  EMAIL_LOGIN_FAILED,

  FACEBOOK_LOGIN_REQUEST,
  FACEBOOK_LOGIN_FAILED,
  FACEBOOK_LOGIN_SUCCESS,

  TWITTER_LOGIN_REQUEST,
  TWITTER_LOGIN_SUCCESS,
  TWITTER_LOGIN_FAILED,

  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILED,

  GET_SELF_REQUEST,
  GET_SELF_SUCCESS,
  GET_SELF_FAILED,

  EDIT_PROFILE_REQUEST,
  EDIT_PROFILE_SUCCESS,
  EDIT_PROFILE_FAILED,

  GET_PUBLIC_PROFILE_REQUEST,
  GET_PUBLIC_PROFILE_FAILED,
  GET_PUBLIC_PROFILE_SUCCESS,

  VERIFY_EMAIL_REQUEST,
  VERIFY_EMAIL_SUCCESS,
  VERIFY_EMAIL_FAILED,

  GET_NOTIFICATIONS_REQUEST,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILED,

  CLEAR_NOTIFICATIONS_SUCCESS,

  LOAD_MORE_NOTIFICATIONS_REQUEST,
  LOAD_MORE_NOTIFICATIONS_SUCCESS,
  LOAD_MORE_NOTIFICATIONS_FAILED,

  GET_CURRENT_VIDEO_SELF_STATS_REQUEST,
  GET_CURRENT_VIDEO_SELF_STATS_SUCCESS,
  GET_CURRENT_VIDEO_SELF_STATS_FAILED,

  BLOCK_REQUEST,
  BLOCK_SUCCESS,
  BLOCK_FAILED,

  UNBLOCK_REQUEST,
  UNBLOCK_SUCCESS,
  UNBLOCK_FAILED,

  GET_BLOCKED_LIST_REQUEST,
  GET_BLOCKED_LIST_SUCCESS,
  GET_BLOCKED_LIST_FAILED,

  MUTE_REQUEST,
  MUTE_SUCCESS,
  MUTE_FAILED,

  UNMUTE_REQUEST,
  UNMUTE_SUCCESS,
  UNMUTE_FAILED,

  GET_MUTED_LIST_REQUEST,
  GET_MUTED_LIST_SUCCESS,
  GET_MUTED_LIST_FAILED,

  SUBSCRIBE_REQUEST,
  SUBSCRIBE_SUCCESS,
  SUBSCRIBE_FAILED,
  
  UNSUBSCRIBE_REQUEST,
  UNSUBSCRIBE_SUCCESS,
  UNSUBSCRIBE_FAILED, 

  LIKE_REQUEST,
  LIKE_SUCCESS,
  LIKE_FAILED,
  
  UNLIKE_REQUEST,
  UNLIKE_SUCCESS,
  UNLIKE_FAILED, 

  FAVOURITE_REQUEST,
  FAVOURITE_SUCCESS,
  FAVOURITE_FAILED,
  
  UNFAVOURITE_REQUEST,
  UNFAVOURITE_SUCCESS,
  UNFAVOURITE_FAILED,

  FLAG_REQUEST,
  FLAG_SUCCESS,
  FLAG_FAILED,
  
  UNFLAG_REQUEST,
  UNFLAG_SUCCESS,
  UNFLAG_FAILED,

  SAVE_LOCATION_REQUEST,
  SAVE_LOCATION_SUCCESS,
  SAVE_LOCATION_FAILED,

  GET_AWS_SIGNATURE_REQUEST,
  GET_AWS_SIGNATURE_SUCCESS,
  GET_AWS_SIGNATURE_FAILED,
  
  CLEAR_AWS_SIGNATURE_REQUEST,
  CLEAR_AWS_SIGNATURE_SUCCESS,
  CLEAR_AWS_SIGNATURE_FAILED,

  UPLOAD_PROFILE_IMAGE_REQUEST,
  UPLOAD_PROFILE_IMAGE_SUCCESS,
  UPLOAD_PROFILE_IMAGE_FAILED,

  UPLOAD_COVER_IMAGE_REQUEST,
  UPLOAD_COVER_IMAGE_SUCCESS,
  UPLOAD_COVER_IMAGE_FAILED

} from "store/user/actions";

const INITIAL_STATE = fromJS({  
  loggingIn: false,
  loggingOut: false,
  loadingUser: false,
  me: {},
  user: {},
  gettingNotifications: false,
  notifications: {},
  notificationsPaged: {},
  loadingMoreNotifications: false,
  gettingMyCurrentVideoStats: false,
  myCurrentVideoStats: {}, 
  blocking: false,
  blockedList: {},
  gettingBlockedList: false,
  muting: false,
  mutedList: {},
  gettingMutedList: false,
  subscribing: false,
  liking: false,
  favouriting: false,
  flagging: false,
  lat: 0,
  long: 0,
  uploading: false,
  error: '',
  gettingSignature: false,
  awsSignature: {},
  updatingProfile: false,
  verificationStatus: {},
  verifyingEmail: true,
  signingUp: false,
});

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case EMAIL_LOGIN_REQUEST:
      return state.merge ({ 
        ...state, 
        loggingIn: true,
        error: '',
      });
    case EMAIL_LOGIN_SUCCESS:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
      });
    case EMAIL_LOGIN_FAILED:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
        error: action.e.response.data.non_field_errors.toString() || action.e.response.data.email.toString()
      });
    case FACEBOOK_LOGIN_REQUEST:
      return state.merge ({ 
        ...state, 
        loggingIn: true,
        error: '',
      });
    case FACEBOOK_LOGIN_SUCCESS:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
      });
    case FACEBOOK_LOGIN_FAILED:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
        error: action.e.message
      });
    case TWITTER_LOGIN_REQUEST:
      return state.merge ({ 
        ...state, 
        loggingIn: true,
        error: '',
      });
    case TWITTER_LOGIN_SUCCESS:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
      });
    case TWITTER_LOGIN_FAILED:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
        error: action.e.message
      });
    case SIGN_UP_REQUEST:
      return state.merge ({ 
        ...state, 
        signingUp: true,
        error: '',
      });
    case SIGN_UP_SUCCESS:
      return state.merge ({ 
        ...state, 
        signingUp: false,
      });
    case SIGN_UP_FAILED:
      return state.merge ({ 
        ...state, 
        signingUp: false,
        error: action.e.response.data
      });
    case LOGOUT_REQUEST:
      return state.merge ({ 
        ...state, 
        loggingOut: true 
      });
    case LOGOUT_SUCCESS:
      return state.merge ({ 
        ...state,
        loggingOut: false,
        me: {},
        twitterLoggedIn: false
      });
    case LOGOUT_FAILED:
      return state.merge ({ 
        ...state,
        loggingOut: false 
      });
    case GET_SELF_REQUEST:
      return state.merge ({ 
        ...state, 
        loggingIn: true 
      });
    case GET_SELF_SUCCESS:
      return state.merge ({ 
        ...state, 
        loggingIn: false,
        me: action.response.data,
      });
    case GET_SELF_FAILED:
      return state.merge ({ 
        ...state, 
        loggingIn: false
      });
    case EDIT_PROFILE_REQUEST:
      return state.merge ({ 
        ...state, 
        updatingProfile: true 
      });
    case EDIT_PROFILE_SUCCESS:
      return state.merge ({ 
        ...state, 
        updatingProfile: false,
      });
    case EDIT_PROFILE_FAILED:
      return state.merge ({ 
        ...state, 
        updatingProfile: false
      });
    case GET_PUBLIC_PROFILE_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingUser: true 
      });
    case GET_PUBLIC_PROFILE_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingUser: false,
        user: action.response.data,
      });
    case GET_PUBLIC_PROFILE_FAILED:
      return state.merge ({ 
        ...state, 
        loadingUser: false
      });
    case VERIFY_EMAIL_REQUEST:
      return state.merge ({ 
        ...state, 
        verifyingEmail: true 
      });
    case VERIFY_EMAIL_SUCCESS:
      return state.merge ({ 
        ...state, 
        verifyingEmail: false,
        verificationStatus: action.response.data.message,
      });
    case VERIFY_EMAIL_FAILED:
      return state.merge ({ 
        ...state, 
        verifyingEmail: false,
        verificationStatus: action.e.response.data.message,
      });
    case GET_NOTIFICATIONS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingNotifications: true 
      });
    case GET_NOTIFICATIONS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingNotifications: false,
        notifications: action.response.data,
      });
    case GET_NOTIFICATIONS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingNotifications: false
      });
    case CLEAR_NOTIFICATIONS_SUCCESS:
      return state.merge ({ 
        ...state, 
      });
    case LOAD_MORE_NOTIFICATIONS_REQUEST:
      return state.merge ({ 
        ...state, 
        loadingMoreNotifications: true 
      });
    case LOAD_MORE_NOTIFICATIONS_SUCCESS:
      return state.merge ({ 
        ...state, 
        loadingMoreNotifications: false,
        notifications: { ...action.response.data, results: state.getIn(['notifications', 'results']).concat(fromJS(action.response.data.results)) },
      });
    case LOAD_MORE_NOTIFICATIONS_FAILED:
      return state.merge ({ 
        ...state, 
        loadingMoreNotifications: false
      });  
    case GET_CURRENT_VIDEO_SELF_STATS_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingMyCurrentVideoStats: true 
      });
    case GET_CURRENT_VIDEO_SELF_STATS_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingMyCurrentVideoStats: false,
        myCurrentVideoStats: action.response.data,
      });
    case GET_CURRENT_VIDEO_SELF_STATS_FAILED:
      return state.merge ({ 
        ...state, 
        gettingMyCurrentVideoStats: false
      });
    case BLOCK_REQUEST:
      return state.merge ({ 
        ...state, 
        blocking: true 
      });
    case BLOCK_SUCCESS:
      return state.merge ({ 
        ...state, 
        blocking: false,
      });
    case BLOCK_FAILED:
      return state.merge ({ 
        ...state, 
        blocking: false
      });
    case UNBLOCK_REQUEST:
      return state.merge ({ 
        ...state, 
        blocking: true 
      });
    case UNBLOCK_SUCCESS:
      return state.merge ({ 
        ...state, 
        blocking: false,
      });
    case UNBLOCK_FAILED:
      return state.merge ({ 
        ...state, 
        blocking: false
      });
    case GET_BLOCKED_LIST_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingBlockedList: true 
      });
    case GET_BLOCKED_LIST_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingBlockedList: false,
        blockedList: action.response.data.block_user
      });
    case GET_BLOCKED_LIST_FAILED:
      return state.merge ({ 
        ...state, 
        gettingBlockedList: false
      });
    case MUTE_REQUEST:
      return state.merge ({ 
        ...state, 
        muting: true 
      });
    case MUTE_SUCCESS:
      return state.merge ({ 
        ...state, 
        muting: false,
      });
    case MUTE_FAILED:
      return state.merge ({ 
        ...state, 
        muting: false
      });
    case UNMUTE_REQUEST:
      return state.merge ({ 
        ...state, 
        muting: true 
      });
    case UNMUTE_SUCCESS:
      return state.merge ({ 
        ...state, 
        muting: false,
      });
    case UNMUTE_FAILED:
      return state.merge ({ 
        ...state, 
        muting: false
      });
    case GET_MUTED_LIST_REQUEST:
      return state.merge ({ 
        ...state, 
        gettingMutedList: true 
      });
    case GET_MUTED_LIST_SUCCESS:
      return state.merge ({ 
        ...state, 
        gettingMutedList: false,
        mutedList: action.response.data.muted_user
      });
    case GET_MUTED_LIST_FAILED:
      return state.merge ({ 
        ...state, 
        gettingMutedList: false
      });
    case SUBSCRIBE_REQUEST:
      return state.merge ({ 
        ...state, 
        subscribing: true 
      });
    case SUBSCRIBE_SUCCESS:
      return state.merge ({ 
        ...state, 
        subscribing: false,
      });
    case SUBSCRIBE_FAILED:
      return state.merge ({ 
        ...state, 
        subscribing: false
      });
    case UNSUBSCRIBE_REQUEST:
      return state.merge ({ 
        ...state, 
        subscribing: true 
      });
    case UNSUBSCRIBE_SUCCESS:
      return state.merge ({ 
        ...state, 
        subscribing: false,
      });
    case UNSUBSCRIBE_FAILED:
      return state.merge ({ 
        ...state, 
        subscribing: false
      });
    case LIKE_REQUEST:
      return state.merge ({ 
        ...state, 
        liking: true 
      });
    case LIKE_SUCCESS:
      return state.merge ({ 
        ...state, 
        liking: false,
      });
    case LIKE_FAILED:
      return state.merge ({ 
        ...state, 
        liking: false
      });
    case UNLIKE_REQUEST:
      return state.merge ({ 
        ...state, 
        liking: true 
      });
    case UNLIKE_SUCCESS:
      return state.merge ({ 
        ...state, 
        liking: false,
      });
    case UNLIKE_FAILED:
      return state.merge ({ 
        ...state, 
        liking: false
      });  
    case FAVOURITE_REQUEST:
      return state.merge ({ 
        ...state, 
        favouriting: true 
      });
    case FAVOURITE_SUCCESS:
      return state.merge ({ 
        ...state, 
        favouriting: false,
      });
    case FAVOURITE_FAILED:
      return state.merge ({ 
        ...state, 
        favouriting: false
      });
    case UNFAVOURITE_REQUEST:
      return state.merge ({ 
        ...state, 
        favouriting: true 
      });
    case UNFAVOURITE_SUCCESS:
      return state.merge ({ 
        ...state, 
        favouriting: false,
      });
    case UNFAVOURITE_FAILED:
      return state.merge ({ 
        ...state, 
        favouriting: false
      });
    case FLAG_REQUEST:
      return state.merge ({ 
        ...state, 
        flagging: true 
      });
    case FLAG_SUCCESS:
      return state.merge ({ 
        ...state, 
        flagging: false,
      });
    case FLAG_FAILED:
      return state.merge ({ 
        ...state, 
        flagging: false
      });
    case UNFLAG_REQUEST:
      return state.merge ({ 
        ...state, 
        flagging: true 
      });
    case UNFLAG_SUCCESS:
      return state.merge ({ 
        ...state, 
        flagging: false,
      });
    case UNFLAG_FAILED:
      return state.merge ({ 
        ...state, 
        flagging: false
      });  
    case SAVE_LOCATION_REQUEST:
      return state.merge ({ 
        ...state, 
      });
    case SAVE_LOCATION_SUCCESS:
      return state.merge ({ 
        ...state, 
        lat: action.coords.latitude,
        long: action.coords.longitude,
      });
    case SAVE_LOCATION_FAILED:
      return state.merge ({ 
        ...state, 
      });  
    case GET_AWS_SIGNATURE_REQUEST:
      return state.merge ({ 
        ...state,
        gettingSignature: true
      });
    case GET_AWS_SIGNATURE_SUCCESS:
      return state.merge ({ 
        ...state,
        gettingSignature: false,
        awsSignature: action.response.data
      });
    case GET_AWS_SIGNATURE_FAILED:
      return state.merge ({ 
        ...state, 
        gettingSignature: false,
        error: true
      });
    case CLEAR_AWS_SIGNATURE_REQUEST:
      return state.merge ({ 
        ...state,
      });
    case CLEAR_AWS_SIGNATURE_SUCCESS:
      return state.merge ({ 
        ...state,
        awsSignature: {}
      });
    case CLEAR_AWS_SIGNATURE_FAILED:
      return state.merge ({ 
        ...state, 
      });
    case UPLOAD_PROFILE_IMAGE_REQUEST:
      return state.merge ({ 
        ...state,
        uploading: true
      });
    case UPLOAD_PROFILE_IMAGE_SUCCESS:
      return state.merge ({ 
        ...state,
        uploading: false,
      });
    case UPLOAD_PROFILE_IMAGE_FAILED:
      return state.merge ({ 
        ...state, 
        uploading: false,
      });
    case UPLOAD_COVER_IMAGE_REQUEST:
      return state.merge ({ 
        ...state,
        uploading: true
      });
    case UPLOAD_COVER_IMAGE_SUCCESS:
      return state.merge ({ 
        ...state,
        uploading: false,
      });
    case UPLOAD_COVER_IMAGE_FAILED:
      return state.merge ({ 
        ...state, 
        uploading: false,
      });
    default:
      return state;
  }
}
