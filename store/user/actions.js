export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILED = "LOGOUT_FAILED";

export const EMAIL_LOGIN_REQUEST = 'EMAIL_LOGIN_REQUEST';
export const EMAIL_LOGIN_FAILED = 'EMAIL_LOGIN_FAILED';
export const EMAIL_LOGIN_SUCCESS = 'EMAIL_LOGIN_SUCCESS';

export const FACEBOOK_LOGIN_REQUEST = 'FACEBOOK_LOGIN_REQUEST';
export const FACEBOOK_LOGIN_FAILED = 'FACEBOOK_LOGIN_FAILED';
export const FACEBOOK_LOGIN_SUCCESS = 'FACEBOOK_LOGIN_SUCCESS';

export const TWITTER_LOGIN_REQUEST = 'TWITTER_LOGIN_REQUEST';
export const TWITTER_LOGIN_FAILED = 'TWITTER_LOGIN_FAILED';
export const TWITTER_LOGIN_SUCCESS = 'TWITTER_LOGIN_SUCCESS';

export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const SIGN_UP_FAILED = 'SIGN_UP_FAILED';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';

export const GET_SELF_REQUEST = 'GET_SELF_REQUEST';
export const GET_SELF_FAILED = 'GET_SELF_FAILED';
export const GET_SELF_SUCCESS = 'GET_SELF_SUCCESS';

export const EDIT_PROFILE_REQUEST = 'EDIT_PROFILE_REQUEST';
export const EDIT_PROFILE_SUCCESS = 'EDIT_PROFILE_SUCCESS';
export const EDIT_PROFILE_FAILED = 'EDIT_PROFILE_FAILED';

export const GET_PUBLIC_PROFILE_REQUEST = 'GET_PUBLIC_PROFILE_REQUEST';
export const GET_PUBLIC_PROFILE_FAILED = 'GET_PUBLIC_PROFILE_FAILED';
export const GET_PUBLIC_PROFILE_SUCCESS = 'GET_PUBLIC_PROFILE_SUCCESS';

export const REQUEST_PASSWORD_RESET_REQUEST = 'REQUEST_PASSWORD_RESET_REQUEST';
export const REQUEST_PASSWORD_RESET_FAILED = 'REQUEST_PASSWORD_RESET_FAILED';
export const REQUEST_PASSWORD_RESET_SUCCESS = 'REQUEST_PASSWORD_RESET_SUCCESS';

export const RESET_PASSWORD_REQUEST = 'RESET_PASSWORD_REQUEST';
export const RESET_PASSWORD_FAILED = 'RESET_PASSWORD_FAILED';
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';

export const VERIFY_EMAIL_REQUEST = 'VERIFY_EMAIL_REQUEST';
export const VERIFY_EMAIL_FAILED = 'VERIFY_EMAIL_FAILED';
export const VERIFY_EMAIL_SUCCESS = 'VERIFY_EMAIL_SUCCESS';

export const SEND_FEEDBACK_REQUEST = 'SEND_FEEDBACK_REQUEST';
export const SEND_FEEDBACK_FAILED = 'SEND_FEEDBACK_FAILED';
export const SEND_FEEDBACK_SUCCESS = 'SEND_FEEDBACK_SUCCESS';
export const CLEAR_FEEDBACK_STATUS = 'CLEAR_FEEDBACK_STATUS';

export const BLOCK_REQUEST = 'BLOCK_REQUEST';
export const BLOCK_FAILED = 'BLOCK_FAILED';
export const BLOCK_SUCCESS = 'BLOCK_SUCCESS';

export const GET_BLOCKED_LIST_REQUEST = 'GET_BLOCKED_LIST_REQUEST';
export const GET_BLOCKED_LIST_FAILED = 'GET_BLOCKED_LIST_FAILED';
export const GET_BLOCKED_LIST_SUCCESS = 'GET_BLOCKED_LIST_SUCCESS';

export const UNBLOCK_REQUEST = 'UNBLOCK_REQUEST';
export const UNBLOCK_FAILED = 'UNBLOCK_FAILED';
export const UNBLOCK_SUCCESS = 'UNBLOCK_SUCCESS';

export const MUTE_REQUEST = 'MUTE_REQUEST';
export const MUTE_FAILED = 'MUTE_FAILED';
export const MUTE_SUCCESS = 'MUTE_SUCCESS';

export const GET_MUTED_LIST_REQUEST = 'GET_MUTED_LIST_REQUEST';
export const GET_MUTED_LIST_FAILED = 'GET_MUTED_LIST_FAILED';
export const GET_MUTED_LIST_SUCCESS = 'GET_MUTED_LIST_SUCCESS';

export const UNMUTE_REQUEST = 'UNMUTE_REQUEST';
export const UNMUTE_FAILED = 'UNMUTE_FAILED';
export const UNMUTE_SUCCESS = 'UNMUTE_SUCCESS';

export const VERIFY_TOKEN_REQUEST = 'VERIFY_TOKEN_REQUEST';
export const VERIFY_TOKEN_FAILED = 'VERIFY_TOKEN_FAILED';
export const VERIFY_TOKEN_SUCCESS = 'VERIFY_TOKEN_SUCCESS';

export const GET_NOTIFICATIONS_REQUEST = 'GET_NOTIFICATIONS_REQUEST';
export const GET_NOTIFICATIONS_FAILED = 'GET_NOTIFICATIONS_FAILED';
export const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS';

export const CLEAR_NOTIFICATIONS_REQUEST = 'CLEAR_NOTIFICATIONS_REQUEST';
export const CLEAR_NOTIFICATIONS_SUCCESS = 'CLEAR_NOTIFICATIONS_SUCCESS';
export const CLEAR_NOTIFICATIONS_FAILED = 'CLEAR_NOTIFICATIONS_FAILED';

export const LOAD_MORE_NOTIFICATIONS_REQUEST = 'LOAD_MORE_NOTIFICATIONS_REQUEST';
export const LOAD_MORE_NOTIFICATIONS_SUCCESS = 'LOAD_MORE_NOTIFICATIONS_SUCCESS';
export const LOAD_MORE_NOTIFICATIONS_FAILED = 'LOAD_MORE_NOTIFICATIONS_FAILED';

export const CANCEL_CLEAR_NOTIFICATIONS_REQUEST = "CANCEL_CLEAR_NOTIFICATIONS_REQUEST";

export const GET_CONNECTION_REQUEST = 'GET_CONNECTION_REQUEST';
export const GET_CONNECTION_FAILED = 'GET_CONNECTION_FAILED';
export const GET_CONNECTION_SUCCESS = 'GET_CONNECTION_SUCCESS';

export const CONNECT_TO_FACEBOOK_REQUEST = 'CONNECT_TO_FACEBOOK_REQUEST';
export const CONNECT_TO_FACEBOOK_FAILED = 'CONNECT_TO_FACEBOOK_FAILED';
export const CONNECT_TO_FACEBOOK_SUCCESS = 'CONNECT_TO_FACEBOOK_SUCCESS';

export const DISCONNECT_FROM_FACEBOOK_REQUEST = 'DISCONNECT_FROM_FACEBOOK_REQUEST';
export const DISCONNECT_FROM_FACEBOOK_FAILED = 'DISCONNECT_FROM_FACEBOOK_FAILED';
export const DISCONNECT_FROM_FACEBOOK_SUCCESS = 'DISCONNECT_FROM_FACEBOOK_SUCCESS';

export const CHANGE_PASSWORD_REQUEST = 'CHANGE_PASSWORD_REQUEST';
export const CHANGE_PASSWORD_FAILED = 'CHANGE_PASSWORD_FAILED';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';

export const RESET_PASSWORD_CHANGE_STATUS = 'RESET_PASSWORD_CHANGE_STATUS';

export const GET_COIN_HISTORY_REQUEST = 'GET_COIN_HISTORY_REQUEST';
export const GET_COIN_HISTORY_FAILED = 'GET_COIN_HISTORY_FAILED';
export const GET_COIN_HISTORY_SUCCESS = 'GET_COIN_HISTORY_SUCCESS';

export const GET_PEARL_HISTORY_REQUEST = 'GET_PEARL_HISTORY_REQUEST';
export const GET_PEARL_HISTORY_FAILED = 'GET_PEARL_HISTORY_FAILED';
export const GET_PEARL_HISTORY_SUCCESS = 'GET_PEARL_HISTORY_SUCCESS';

export const GET_APPROXIMATE_LOCATION_REQUEST = 'GET_APPROXIMATE_LOCATION_REQUEST';
export const GET_APPROXIMATE_LOCATION_FAILED = 'GET_APPROXIMATE_LOCATION_FAILED';
export const GET_APPROXIMATE_LOCATION_SUCCESS = 'GET_APPROXIMATE_LOCATION_SUCCESS';

export const GET_CURRENT_VIDEO_SELF_STATS_REQUEST = 'GET_CURRENT_VIDEO_SELF_STATS_REQUEST';
export const GET_CURRENT_VIDEO_SELF_STATS_SUCCESS = 'GET_CURRENT_VIDEO_SELF_STATS_SUCCESS';
export const GET_CURRENT_VIDEO_SELF_STATS_FAILED = 'GET_CURRENT_VIDEO_SELF_STATS_FAILED';

export const SUBSCRIBE_REQUEST = "SUBSCRIBE_REQUEST";
export const SUBSCRIBE_SUCCESS = "SUBSCRIBE_SUCCESS";
export const SUBSCRIBE_FAILED = "SUBSCRIBE_FAILED";

export const UNSUBSCRIBE_REQUEST = "UNSUBSCRIBE_REQUEST";
export const UNSUBSCRIBE_SUCCESS = "UNSUBSCRIBE_SUCCESS";
export const UNSUBSCRIBE_FAILED = "UNSUBSCRIBE_FAILED";

export const LIKE_REQUEST = "LIKE_REQUEST";
export const LIKE_SUCCESS = "LIKE_SUCCESS";
export const LIKE_FAILED = "LIKE_FAILED";

export const UNLIKE_REQUEST = "UNLIKE_REQUEST";
export const UNLIKE_SUCCESS = "UNLIKE_SUCCESS";
export const UNLIKE_FAILED = "UNLIKE_FAILED";

export const FAVOURITE_REQUEST = "FAVOURITE_REQUEST";
export const FAVOURITE_SUCCESS = "FAVOURITE_SUCCESS";
export const FAVOURITE_FAILED = "FAVOURITE_FAILED";

export const UNFAVOURITE_REQUEST = "UNFAVOURITE_REQUEST";
export const UNFAVOURITE_SUCCESS = "UNFAVOURITE_SUCCESS";
export const UNFAVOURITE_FAILED = "UNFAVOURITE_FAILED";

export const FLAG_REQUEST = "FLAG_REQUEST";
export const FLAG_SUCCESS = "FLAG_SUCCESS";
export const FLAG_FAILED = "FLAG_FAILED";

export const UNFLAG_REQUEST = "UNFLAG_REQUEST";
export const UNFLAG_SUCCESS = "UNFLAG_SUCCESS";
export const UNFLAG_FAILED = "UNFLAG_FAILED";

export const SAVE_LOCATION_REQUEST = "SAVE_LOCATION_REQUEST";
export const SAVE_LOCATION_SUCCESS = "SAVE_LOCATION_SUCCESS";
export const SAVE_LOCATION_FAILED = "SAVE_LOCATION_FAILED";

export const GET_AWS_SIGNATURE_REQUEST = "GET_AWS_SIGNATURE_REQUEST";
export const GET_AWS_SIGNATURE_SUCCESS = "GET_AWS_SIGNATURE_SUCCESS";
export const GET_AWS_SIGNATURE_FAILED = "GET_AWS_SIGNATURE_FAILED";

export const CLEAR_AWS_SIGNATURE_REQUEST = "CLEAR_AWS_SIGNATURE_REQUEST";
export const CLEAR_AWS_SIGNATURE_SUCCESS = "CLEAR_AWS_SIGNATURE_SUCCESS";
export const CLEAR_AWS_SIGNATURE_FAILED = "CLEAR_AWS_SIGNATURE_FAILED"

export const UPLOAD_PROFILE_IMAGE_REQUEST = "UPLOAD_PROFILE_IMAGE_REQUEST";
export const UPLOAD_PROFILE_IMAGE_SUCCESS = "UPLOAD_PROFILE_IMAGE_SUCCESS";
export const UPLOAD_PROFILE_IMAGE_FAILED = "UPLOAD_PROFILE_IMAGE_FAILED";

export const UPLOAD_COVER_IMAGE_REQUEST = "UPLOAD_COVER_IMAGE_REQUEST";
export const UPLOAD_COVER_IMAGE_SUCCESS = "UPLOAD_COVER_IMAGE_SUCCESS";
export const UPLOAD_COVER_IMAGE_FAILED = "UPLOAD_COVER_IMAGE_FAILED";

export const emailLoginRequest = (email, password) => ({ type: EMAIL_LOGIN_REQUEST, email, password });

export const facebookLoginRequest = token => ({ type: FACEBOOK_LOGIN_REQUEST, token });

export const twitterLoginRequest = body => ({ type: TWITTER_LOGIN_REQUEST, body });

export const signUpRequest = (username, email, password1, password2) => ({ type: SIGN_UP_REQUEST, username, email, password1, password2 })

export const getSelfRequest = jwt => ({ type: GET_SELF_REQUEST, jwt });

export const editProfileRequest = (data, jwt) => ({ type: EDIT_PROFILE_REQUEST, data, jwt });

export const getPublicProfileRequest = (username, jwt) => ({ type: GET_PUBLIC_PROFILE_REQUEST, username, jwt });

export const logOutRequest = () => ({ type: LOGOUT_REQUEST });

export const verifyEmailRequest = key => ({ type: VERIFY_EMAIL_REQUEST, key });

export const getNotificationsRequest = (username, jwt) => ({ type: GET_NOTIFICATIONS_REQUEST, username, jwt })

export const clearNotificationsRequest = (jwt) => ({ type: CLEAR_NOTIFICATIONS_REQUEST, jwt })

export const cancelClearNotificationsRequest = () => ({ type: CANCEL_CLEAR_NOTIFICATIONS_REQUEST })

export const loadMoreNotificationsRequest = (next, jwt) => ({ type: LOAD_MORE_NOTIFICATIONS_REQUEST, next, jwt })

export const getCurrentVideoSelfStatsRequest = (videoId, jwt) => ({ type: GET_CURRENT_VIDEO_SELF_STATS_REQUEST, videoId, jwt })

export const getBlockedListRequest = jwt => ({ type: GET_BLOCKED_LIST_REQUEST, jwt })

export const blockRequest = (userId, username, jwt) => ({ type: BLOCK_REQUEST, userId, username, jwt })

export const unblockRequest = (userId, username, jwt) => ({ type: UNBLOCK_REQUEST, userId, username, jwt })

export const getMutedListRequest = (videoId, jwt) => ({ type: GET_MUTED_LIST_REQUEST, videoId, jwt })

export const muteRequest = (videoId, userId, username, jwt) => ({ type: MUTE_REQUEST, videoId, userId, username, jwt })

export const unmuteRequest = (videoId, userId, username, jwt) => ({ type: UNMUTE_REQUEST, videoId, userId, username, jwt })

export const subscribeRequest = (userId, username, jwt) => ({ type: SUBSCRIBE_REQUEST, userId, username, jwt })

export const unsubscribeRequest = (userId, username, jwt) => ({ type: UNSUBSCRIBE_REQUEST, userId, username, jwt })

export const likeRequest = (videoId, timestamp, jwt) => ({ type: LIKE_REQUEST, videoId, timestamp, jwt })

export const unlikeRequest = (videoId, timestamp, jwt) => ({ type: UNLIKE_REQUEST, videoId, timestamp, jwt })

export const favouriteRequest = (videoId, timestamp, jwt) => ({ type: FAVOURITE_REQUEST, videoId, timestamp, jwt })

export const unfavouriteRequest = (videoId, timestamp, jwt) => ({ type: UNFAVOURITE_REQUEST, videoId, timestamp, jwt })

export const flagRequest = (videoId, jwt) => ({ type: FLAG_REQUEST, videoId, jwt })

export const unflagRequest = (videoId, jwt) => ({ type: UNFLAG_REQUEST, videoId, jwt })

export const saveLocationRequest = (coords) => ({ type: SAVE_LOCATION_REQUEST, coords })

export const uploadProfileImageRequest = (image, filename, username, awsSignatureData, jwt) => ({ type: UPLOAD_PROFILE_IMAGE_REQUEST, image, filename, username, awsSignatureData, jwt })

export const uploadCoverImageRequest = (image, filename, username, awsSignatureData, jwt) => ({ type: UPLOAD_COVER_IMAGE_REQUEST, image, filename, username, awsSignatureData, jwt })

export const getAWSSignatureRequest = jwt => ({ type: GET_AWS_SIGNATURE_REQUEST, jwt })

export const clearAWSSignatureRequest = () => ({ type: CLEAR_AWS_SIGNATURE_REQUEST })