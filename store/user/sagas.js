import { eventChannel, END, delay } from "redux-saga";
import { take, fork, call, put, cancel } from "redux-saga/effects";
import userApi from "services/api/user";
import { setCookie, removeCookie } from "helpers/session";

import {
  EMAIL_LOGIN_REQUEST,
  EMAIL_LOGIN_SUCCESS,
  EMAIL_LOGIN_FAILED,

  FACEBOOK_LOGIN_REQUEST,
  FACEBOOK_LOGIN_FAILED,
  FACEBOOK_LOGIN_SUCCESS,

  TWITTER_LOGIN_REQUEST,
  TWITTER_LOGIN_SUCCESS,
  TWITTER_LOGIN_FAILED,

  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,

  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILED,

  GET_SELF_REQUEST,
  GET_SELF_SUCCESS,
  GET_SELF_FAILED,

  EDIT_PROFILE_REQUEST,
  EDIT_PROFILE_SUCCESS,
  EDIT_PROFILE_FAILED,

  GET_PUBLIC_PROFILE_REQUEST,
  GET_PUBLIC_PROFILE_FAILED,
  GET_PUBLIC_PROFILE_SUCCESS,

  VERIFY_EMAIL_REQUEST,
  VERIFY_EMAIL_SUCCESS,
  VERIFY_EMAIL_FAILED,

  GET_NOTIFICATIONS_REQUEST,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILED,

  CLEAR_NOTIFICATIONS_REQUEST,
  CLEAR_NOTIFICATIONS_SUCCESS,
  CLEAR_NOTIFICATIONS_FAILED,
  CANCEL_CLEAR_NOTIFICATIONS_REQUEST,

  LOAD_MORE_NOTIFICATIONS_REQUEST,
  LOAD_MORE_NOTIFICATIONS_SUCCESS,
  LOAD_MORE_NOTIFICATIONS_FAILED,

  GET_CURRENT_VIDEO_SELF_STATS_REQUEST,
  GET_CURRENT_VIDEO_SELF_STATS_SUCCESS,
  GET_CURRENT_VIDEO_SELF_STATS_FAILED,

  GET_BLOCKED_LIST_REQUEST,
  GET_BLOCKED_LIST_SUCCESS,
  GET_BLOCKED_LIST_FAILED,

  BLOCK_REQUEST,
  BLOCK_SUCCESS,
  BLOCK_FAILED,

  UNBLOCK_REQUEST,
  UNBLOCK_SUCCESS,
  UNBLOCK_FAILED,

  GET_MUTED_LIST_REQUEST,
  GET_MUTED_LIST_SUCCESS,
  GET_MUTED_LIST_FAILED,

  MUTE_REQUEST,
  MUTE_SUCCESS,
  MUTE_FAILED,

  UNMUTE_REQUEST,
  UNMUTE_SUCCESS,
  UNMUTE_FAILED,

  SUBSCRIBE_REQUEST,
  SUBSCRIBE_SUCCESS,
  SUBSCRIBE_FAILED,

  UNSUBSCRIBE_REQUEST,
  UNSUBSCRIBE_SUCCESS,
  UNSUBSCRIBE_FAILED,

  LIKE_REQUEST,
  LIKE_SUCCESS,
  LIKE_FAILED,
  
  UNLIKE_REQUEST,
  UNLIKE_SUCCESS,
  UNLIKE_FAILED, 

  FAVOURITE_REQUEST,
  FAVOURITE_SUCCESS,
  FAVOURITE_FAILED,
  
  UNFAVOURITE_REQUEST,
  UNFAVOURITE_SUCCESS,
  UNFAVOURITE_FAILED,

  FLAG_REQUEST,
  FLAG_SUCCESS,
  FLAG_FAILED,
  
  UNFLAG_REQUEST,
  UNFLAG_SUCCESS,
  UNFLAG_FAILED,

  SAVE_LOCATION_REQUEST,
  SAVE_LOCATION_SUCCESS,
  SAVE_LOCATION_FAILED,

  GET_AWS_SIGNATURE_REQUEST,
  GET_AWS_SIGNATURE_SUCCESS,
  GET_AWS_SIGNATURE_FAILED,
  
  UPLOAD_PROFILE_IMAGE_REQUEST,
  UPLOAD_PROFILE_IMAGE_SUCCESS,
  UPLOAD_PROFILE_IMAGE_FAILED,

  UPLOAD_COVER_IMAGE_REQUEST,
  UPLOAD_COVER_IMAGE_SUCCESS,
  UPLOAD_COVER_IMAGE_FAILED

} from "store/user/actions";

import {
  GET_SINGLE_VIDEO_STATS_REQUEST
} from "store/video/actions"

import {
  LOAD_UP_PROFILE_PAGE_SUCCESS
} from "store/pages/actions"

const AWS_UPLOAD_ROOT = process.env.AWS_UPLOAD_ROOT;
const AWS_FILE_ROOT = process.env.AWS_FILE_ROOT;

function* logOut() {
  try {
    yield call(removeCookie, 'jwt')
    yield call(removeCookie, 'username')

    yield put({type: LOGOUT_SUCCESS})
  } catch(e) {
    yield put({type: LOGOUT_FAILED, e})
  }
}

function* watchLogOut() {
  while (true) {
    yield take(LOGOUT_REQUEST);
    yield fork(logOut);
  }
}

function* emailLogin(email, password) {
  try {
    const response = yield call(userApi.emailLogin, email, password)
    yield put({type: EMAIL_LOGIN_SUCCESS, response})

    const jwt = response.data.token
    yield call(setCookie, "jwt", jwt)
    yield put({type: GET_SELF_REQUEST, jwt})
    return response
  } catch(e) {
    yield put({type: EMAIL_LOGIN_FAILED, e})
  }
}

function* watchEmailLogin() {
  while (true) {
    const {email, password} = yield take(EMAIL_LOGIN_REQUEST);
    yield fork(emailLogin, email, password)
  }
}

function* facebookLogin(token) {
  try {
    const response = yield call(userApi.facebookLogin, token)
    yield put({type: FACEBOOK_LOGIN_SUCCESS, response})

    const jwt = response.data.token
    yield call(setCookie, "jwt", jwt)
    yield put({type: GET_SELF_REQUEST, jwt})
    return response
  } catch(e) {
    yield put({type: FACEBOOK_LOGIN_FAILED, e})
  }
}

function* watchFacebookLogin() {
  while (true) {
    const {token} = yield take(FACEBOOK_LOGIN_REQUEST);
    yield fork(facebookLogin, token)
  }
}

function* twitterLogin(body) {
  try {
    const response = yield call(userApi.twitterLogin, body)
    const jwt = response.data.token
    yield call(setCookie, "jwt", jwt)

    yield put({type: TWITTER_LOGIN_SUCCESS})
    yield put({type: GET_SELF_REQUEST, jwt})
    return jwt
  } catch(e) {
    yield put({type: TWITTER_LOGIN_FAILED, e})
  }
}

function* watchTwitterLogin() {
  while (true) {
    const {body} = yield take(TWITTER_LOGIN_REQUEST);
    yield fork(twitterLogin, body)
  }
}

function* signUp(username, email, password1, password2) {
  try {
    const response = yield call(userApi.signUp, username, email, password1, password2)
    yield put({type: SIGN_UP_SUCCESS})
    return response
  } catch(e) {
    yield put({type: SIGN_UP_FAILED, e})
  }
}

function* watchSignUp() {
  while (true) {
    const {username, email, password1, password2} = yield take(SIGN_UP_REQUEST);
    yield fork(signUp, username, email, password1, password2)
  }
}

function* getSelf(jwt) {
  try {
    const response = yield call(userApi.getSelf, jwt);
    const username = encodeURIComponent(response.data.username)
    yield put({type: GET_SELF_SUCCESS, response})
    yield call(getNotifications, username, jwt)
    yield call(setCookie, "username", username)
    return response
  } catch(e) {
    yield put({type: LOGOUT_REQUEST})
    yield put({type: GET_SELF_FAILED, e})
  }
}

function* watchGetSelf() {
  while (true) {
    const {jwt} = yield take(GET_SELF_REQUEST);
    yield fork(getSelf, jwt);
  }
}

function* editProfile(data, jwt) {
  try {
    const response = yield call(userApi.editProfile, data, jwt)
    yield put({type: EDIT_PROFILE_SUCCESS, response})
    if (jwt) {
      yield put({type: GET_SELF_REQUEST, jwt })
    }
    return response
  } catch(e) {
    yield put({type: EDIT_PROFILE_FAILED, e})
  }
}

function* watchEditProfile() {
  while (true) {
    const {data, jwt} = yield take(EDIT_PROFILE_REQUEST);
    yield fork(editProfile, data, jwt);
  }
}

export function* getPublicProfile(username, jwt) {
  try {
    const response = yield call(userApi.getPublicProfile, username, jwt)
    yield put({type: GET_PUBLIC_PROFILE_SUCCESS, response})
    yield put({type: LOAD_UP_PROFILE_PAGE_SUCCESS})
    return response
  } catch(e) {
    yield put({type: GET_PUBLIC_PROFILE_FAILED, e})
  }
}

function* watchPublicProfile() {
  while (true) {
    const {username, jwt} = yield take(GET_PUBLIC_PROFILE_REQUEST);
    yield fork(getPublicProfile, username, jwt);
  }
}

function* verifyEmail(key) {
  try {
    const response = yield call(userApi.verifyEmail, key)
    yield put({type: VERIFY_EMAIL_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: VERIFY_EMAIL_FAILED, e})
  }
}

function* watchVerifyEmail() {
  while (true) {
    const {key} = yield take(VERIFY_EMAIL_REQUEST);
    yield fork(verifyEmail, key);
  }
}

function* getNotifications(username, jwt) {
  try {
    const response = yield call(userApi.getNotifications, username, jwt)
    yield put({type: GET_NOTIFICATIONS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_NOTIFICATIONS_FAILED, e})
  }
}

function* watchGetNotifications() {
  while (true) {
    const {username, jwt} = yield take(GET_NOTIFICATIONS_REQUEST);
    yield fork(getNotifications, username, jwt);
  }
}

function* clearNotifications(jwt) {
  try {
    yield delay(2000)
    yield call(userApi.clearNotifications, jwt)
    yield put({type: CLEAR_NOTIFICATIONS_SUCCESS})
  } catch(e) {
    yield put({type: CLEAR_NOTIFICATIONS_FAILED, e})
  }
}

function* watchClearNotifications() {
  while (true) {
    const {jwt} = yield take(CLEAR_NOTIFICATIONS_REQUEST);
    const task = yield fork(clearNotifications, jwt);
    const action = yield take([CANCEL_CLEAR_NOTIFICATIONS_REQUEST, CLEAR_NOTIFICATIONS_FAILED])
    if (action.type === CANCEL_CLEAR_NOTIFICATIONS_REQUEST) {
      yield cancel(task)
    }
  }
}

function* loadMoreNotifications(next, jwt) {
  try {
    const response = yield call(userApi.loadMoreNotifications, next, jwt)
    yield put({type: LOAD_MORE_NOTIFICATIONS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: LOAD_MORE_NOTIFICATIONS_FAILED, e})
  }
}

function* watchLoadMoreNotifications() {
  while (true) {
    const {next, jwt} = yield take(LOAD_MORE_NOTIFICATIONS_REQUEST);
    yield fork(loadMoreNotifications, next, jwt);
  }
}

function* getCurrentVideoSelfStats(videoId, jwt) {
  try {
    const response = yield call(userApi.getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_CURRENT_VIDEO_SELF_STATS_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_CURRENT_VIDEO_SELF_STATS_FAILED, e})
  }
}

function* watchGetCurrentVideoSelfStats() {
  while (true) {
    const {videoId, jwt} = yield take(GET_CURRENT_VIDEO_SELF_STATS_REQUEST);
    yield fork(getCurrentVideoSelfStats, videoId, jwt);
  }
}

function* block(userId, username, jwt) {
  try {
    const response = yield call(userApi.block, userId, jwt)
    yield put({type: BLOCK_SUCCESS, response})
    yield put({type: GET_BLOCKED_LIST_REQUEST, jwt})
    yield fork(getPublicProfile, username, jwt)
    return response
  } catch(e) {
    yield put({type: BLOCK_FAILED, e})
  }
}

function* watchBlock() {
  while (true) {
    const {userId, username, jwt} = yield take(BLOCK_REQUEST);
    yield fork(block, userId, username, jwt);
  }
}

function* unblock(userId, username, jwt) {
  try {
    const response = yield call(userApi.unblock, userId, jwt)
    yield put({type: UNBLOCK_SUCCESS, response})
    yield put({type: GET_BLOCKED_LIST_REQUEST, jwt})
    yield fork(getPublicProfile, username, jwt)
    return response
  } catch(e) {
    yield put({type: UNBLOCK_FAILED, e})
  }
}

function* watchUnblock() {
  while (true) {
    const {userId, username, jwt} = yield take(UNBLOCK_REQUEST);
    yield fork(unblock, userId, username, jwt);
  }
}

function* getBlockedList(jwt) {
  try {
    const response = yield call(userApi.getBlockedList, jwt)
    yield put({type: GET_BLOCKED_LIST_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_BLOCKED_LIST_FAILED, e})
  }
}

function* watchGetBlockedList() {
  while (true) {
    const {jwt} = yield take(GET_BLOCKED_LIST_REQUEST);
    yield fork(getBlockedList, jwt);
  }
}

function* mute(videoId, userId, username, jwt) {
  try {
    const response = yield call(userApi.mute, videoId, userId, jwt)
    yield put({type: MUTE_SUCCESS, response})
    yield put({type: GET_MUTED_LIST_REQUEST, videoId, jwt})
    return response
  } catch(e) {
    yield put({type: MUTE_FAILED, e})
  }
}

function* watchMute() {
  while (true) {
    const {videoId, userId, username, jwt} = yield take(MUTE_REQUEST);
    yield fork(mute, videoId, userId, username, jwt);
  }
}

function* unmute(videoId, userId, username, jwt) {
  try {
    const response = yield call(userApi.unmute, videoId, userId, jwt)
    yield put({type: UNMUTE_SUCCESS, response})
    yield put({type: GET_MUTED_LIST_REQUEST, videoId, jwt})
    return response
  } catch(e) {
    yield put({type: UNMUTE_FAILED, e})
  }
}

function* watchUnmute() {
  while (true) {
    const {videoId, userId, username, jwt} = yield take(UNMUTE_REQUEST);
    yield fork(unmute, videoId, userId, username, jwt);
  }
}

function* getMutedList(videoId, jwt) {
  try {
    const response = yield call(userApi.getMutedList, videoId, jwt)
    yield put({type: GET_MUTED_LIST_SUCCESS, response})
    return response
  } catch(e) {
    yield put({type: GET_MUTED_LIST_FAILED, e})
  }
}

function* watchGetMutedList() {
  while (true) {
    const {videoId, jwt} = yield take(GET_MUTED_LIST_REQUEST);
    yield fork(getMutedList, videoId, jwt);
  }
}

function* subscribe(userId, username, jwt) {
  try {
    const response = yield call(userApi.subscribe, userId, jwt)
    yield put({type: SUBSCRIBE_SUCCESS, response})
    
    yield fork(getPublicProfile, username, jwt)
    return response
  } catch(e) {
    yield put({type: SUBSCRIBE_FAILED, e})
  }
}

function* watchSubscribe() {
  while (true) {
    const {userId, username, jwt} = yield take(SUBSCRIBE_REQUEST);
    yield fork(subscribe, userId, username, jwt);
  }
}

function* unsubscribe(userId, username, jwt) {
  try {
    const response = yield call(userApi.unsubscribe, userId, jwt)
    yield put({type: UNSUBSCRIBE_SUCCESS, response})

    yield fork(getPublicProfile, username, jwt)
    return response
  } catch(e) {
    yield put({type: UNSUBSCRIBE_FAILED, e})
  }
}

function* watchUnsubscribe() {
  while (true) {
    const {userId, username, jwt} = yield take(UNSUBSCRIBE_REQUEST);
    yield fork(unsubscribe, userId, username, jwt);
  }
}

function* like(videoId, timestamp, jwt) {
  try {
    const response = yield call(userApi.like, videoId, timestamp, jwt)
    yield put({type: LIKE_SUCCESS, response})
    
    yield call(getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    return response
  } catch(e) {
    yield put({type: LIKE_FAILED, e})
  }
}

function* watchLike() {
  while (true) {
    const {videoId, timestamp, jwt} = yield take(LIKE_REQUEST);
    yield fork(like, videoId, timestamp, jwt);
  }
}

function* unlike(videoId, timestamp, jwt) {
  try {
    const response = yield call(userApi.unlike, videoId, timestamp, jwt)
    yield put({type: UNLIKE_SUCCESS, response})

    yield call(getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    return response
  } catch(e) {
    yield put({type: UNLIKE_FAILED, e})
  }
}

function* watchUnlike() {
  while (true) {
    const {videoId, timestamp, jwt} = yield take(UNLIKE_REQUEST);
    yield fork(unlike, videoId, timestamp, jwt);
  }
}

function* favourite(videoId, timestamp, jwt) {
  try {
    const response = yield call(userApi.favourite, videoId, timestamp, jwt)
    yield put({type: FAVOURITE_SUCCESS, response})
    
    yield call(getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    return response
  } catch(e) {
    yield put({type: FAVOURITE_FAILED, e})
  }
}

function* watchFavourite() {
  while (true) {
    const {videoId, timestamp, jwt} = yield take(FAVOURITE_REQUEST);
    yield fork(favourite, videoId, timestamp, jwt);
  }
}

function* unfavourite(videoId, timestamp, jwt) {
  try {
    const response = yield call(userApi.unfavourite, videoId, timestamp, jwt)
    yield put({type: UNFAVOURITE_SUCCESS, response})

    yield call(getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    return response
  } catch(e) {
    yield put({type: UNFAVOURITE_FAILED, e})
  }
}

function* watchUnfavourite() {
  while (true) {
    const {videoId, timestamp, jwt} = yield take(UNFAVOURITE_REQUEST);
    yield fork(unfavourite, videoId, timestamp, jwt);
  }
}

function* flag(videoId, jwt) {
  try {
    const response = yield call(userApi.flag, videoId, jwt)
    yield put({type: FLAG_SUCCESS, response})
    
    yield call(getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    return response
  } catch(e) {
    yield put({type: FLAG_FAILED, e})
  }
}

function* watchFlag() {
  while (true) {
    const {videoId, jwt} = yield take(FLAG_REQUEST);
    yield fork(flag, videoId, jwt);
  }
}

function* unflag(videoId, jwt) {
  try {
    const response = yield call(userApi.unflag, videoId, jwt)
    yield put({type: UNFLAG_SUCCESS, response})

    yield call(getCurrentVideoSelfStats, videoId, jwt)
    yield put({type: GET_SINGLE_VIDEO_STATS_REQUEST, videoId})
    return response
  } catch(e) {
    yield put({type: UNFLAG_FAILED, e})
  }
}

function* watchUnflag() {
  while (true) {
    const {videoId, jwt} = yield take(UNFLAG_REQUEST);
    yield fork(unflag, videoId, jwt);
  }
}

function* saveLocation(coords) {
  try {
    yield put({type: SAVE_LOCATION_SUCCESS, coords})
  } catch(e) {
    yield put({type: SAVE_LOCATION_FAILED, e})
  }
}

function* watchSaveLocation() {
  while (true) {
    const {coords} = yield take(SAVE_LOCATION_REQUEST);
    yield fork(saveLocation, coords);
  }
}

function* getAWSSignature(jwt) {
  try {
    const response = yield call(userApi.getAWSSignature, jwt)
    yield put({type: GET_AWS_SIGNATURE_SUCCESS, response})
  } catch(e) {
    yield put({type: GET_AWS_SIGNATURE_FAILED, e})
  }
}

function* watchGetAWSSignature() {
  while (true) {
    const {jwt} = yield take(GET_AWS_SIGNATURE_REQUEST);
    yield fork(getAWSSignature, jwt);
  }
}

function* uploadImage(image, filename, username, awsSignatureData) {
  console.log("==uploadImage=", image, filename, username, awsSignatureData)
  return eventChannel(emitter => {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', AWS_UPLOAD_ROOT, true)

    let data = new FormData();
    data.append('key', `${username}/${filename}`);
    data.append('AWSAccessKeyId', awsSignatureData.get('a'));
    data.append('policy', awsSignatureData.get('p'));
    data.append('signature', awsSignatureData.get('s'));
    data.append('file', image);

    xhr.upload.onprogress = (ev) => {
      if (ev.lengthComputable) {
        let percentageComplete = ev.loaded / ev.total
        emitter({ progress: { name: image.name, percentageComplete } })
      }
    }
    xhr.onload = (ev) => {
      if (xhr.status >= 200 && xhr.status <= 299) {
        emitter({ success: true })
      } else {
        emitter({ err: new Error('upload failed') })
      }
      emitter(END)
    };
    xhr.onerror = (e) => {
      console.log(e)
      emitter({ err: new Error(e) })
      emitter(END)
    }

    xhr.send(data)
    return () => { }
  })
}

function* uploadProfileImage(image, filename, username, awsSignatureData, jwt) {
  if (Object.keys(image).length > 0) {
    const eventChannel = yield call(uploadImage, image, filename, username, awsSignatureData)
    while (true) {
      const { err, success, progress } = yield take(eventChannel)
      if (err) {
        yield put({type: UPLOAD_PROFILE_IMAGE_FAILED, err})
        return
      }
      if (progress) {
        console.log(progress.name, progress.percentageComplete)
      }
      if (success) {
        yield put.resolve({type: UPLOAD_PROFILE_IMAGE_SUCCESS})
        const data = {
          photo: `${AWS_FILE_ROOT}/${username}/${filename}`
        }
        yield put.resolve({type: EDIT_PROFILE_REQUEST, data, jwt });
      }
    }
  }
}

function* watchUploadProfileImage() {
  while (true) {
    const {image, filename, username, awsSignatureData, jwt} = yield take(UPLOAD_PROFILE_IMAGE_REQUEST);
    yield fork(uploadProfileImage, image, filename, username, awsSignatureData, jwt);
  }
}

function* uploadCoverImage(image, filename, username, awsSignatureData, jwt) {
  if (Object.keys(image).length > 0) {
    const eventChannel = yield call(uploadImage, image, filename, username, awsSignatureData)
    while (true) {
      const { err, success, progress } = yield take(eventChannel)
      if (err) {
        yield put({type: UPLOAD_COVER_IMAGE_FAILED, err})
        return
      }
      if (progress) {
        console.log(progress.name, progress.percentageComplete)
      }
      if (success) {
        yield put.resolve({type: UPLOAD_COVER_IMAGE_SUCCESS})
        const data = {
          cover_photo: `${AWS_FILE_ROOT}/${username}/${filename}`
        }
        yield put.resolve({type: EDIT_PROFILE_REQUEST, data, jwt });
      }
    }
  }
}

function* watchUploadCoverImage() {
  while (true) {
    const {image, filename, username, awsSignatureData, jwt} = yield take(UPLOAD_COVER_IMAGE_REQUEST);
    yield fork(uploadCoverImage, image, filename, username, awsSignatureData, jwt);
  }
}

export default function* userSagas() {
  yield fork(watchLogOut);
  yield fork(watchEmailLogin); 
  yield fork(watchFacebookLogin); 
  yield fork(watchTwitterLogin);
  yield fork(watchSignUp);
  yield fork(watchGetNotifications);
  yield fork(watchGetSelf);
  yield fork(watchEditProfile);
  yield fork(watchPublicProfile);
  yield fork(watchVerifyEmail);
  yield fork(watchClearNotifications);
  yield fork(watchLoadMoreNotifications);
  yield fork(watchGetCurrentVideoSelfStats);
  yield fork(watchGetBlockedList);
  yield fork(watchBlock);
  yield fork(watchUnblock);
  yield fork(watchGetMutedList);
  yield fork(watchMute);
  yield fork(watchUnmute)
  yield fork(watchSubscribe);
  yield fork(watchUnsubscribe);
  yield fork(watchLike);
  yield fork(watchUnlike);
  yield fork(watchFavourite);
  yield fork(watchUnfavourite);
  yield fork(watchFlag);
  yield fork(watchUnflag);
  yield fork(watchSaveLocation);
  yield fork(watchGetAWSSignature);
  yield fork(watchUploadProfileImage);
  yield fork(watchUploadCoverImage);
}