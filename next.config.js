const webpack = require("webpack");
const { parsed: localEnv } = require("dotenv").config();
const withSourceMaps = require("@zeit/next-source-maps");
const withImages = require("next-images");
const withPlugins = require("next-compose-plugins");
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");
const path = require("path");
const glob = require("glob");

const paths = {
  source: path.join(__dirname, './'),
  images: path.join(__dirname, './static/img'),
  svg: path.join(__dirname, './static/svg'),
  node_modules: path.join(__dirname, './node_modules'),
};

const plugins = [
  withSourceMaps,
  withImages,
  [
    withBundleAnalyzer,
    {
      analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
      analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
      bundleAnalyzerConfig: {
        server: {
          analyzerMode: "static",
          reportFilename: "../server-analyze.html"
        },
        browser: {
          analyzerMode: "static",
          reportFilename: "client-analyze.html"
        }
      }
    }
  ]
];

module.exports = withPlugins([...plugins], {
  webpack: (config, { dev }) => {
    const conf = config;
    if (dev) {
      conf.devtool = 'cheap-module-eval-source-map'
    }
    // Fixes npm packages that depend on `fs` module
    conf.node = {
      fs: "empty"
    };
    conf.module.rules.push(
      {
        type: 'javascript/auto',
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.(css|scss)/,
        loader: "emit-file-loader",
        options: {
          name: "dist/[path][name].[ext]"
        }
      },
      {
        test: /\.css$/,
        use: ["babel-loader", "raw-loader", "postcss-loader"]
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          "babel-loader",
          "raw-loader",
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              includePaths: ["styles", "node_modules"]
                .map(d => path.join(__dirname, d))
                .map(g => glob.sync(g))
                .reduce((a, c) => a.concat(c), [])
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true,
              svgo: {
                plugins: [
                  {
                    removeTitle: true,
                  },
                ],
                floatPrecision: 2,
              },
            },
          },
        ],
        include: paths.svg,
      },
    );
    conf.plugins.push(new webpack.EnvironmentPlugin(localEnv));
    return conf;
  }
});
