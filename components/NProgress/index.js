import React from "react";
import NProgress from "nprogress";
import { Router } from "routes";

NProgress.configure({ showSpinner: false });

Router.onRouteChangeStart = url => {
  NProgress.start();
};

Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

export default () => <div className="progress-bar" />;
