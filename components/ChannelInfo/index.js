import React from "react";
import { Link } from "routes";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTicketAlt } from '@fortawesome/free-solid-svg-icons';

import {
  ON
} from 'helpers/text'

const ChannelInfo = ({item}) => (
  <Link route="channel">
    <a>
      <div className="channel-info">
        <span>{item.getIn(['ticket_tier', 'tier_name'])}</span> <span>{ON}</span> <span className="purple"><FontAwesomeIcon icon={faTicketAlt} /></span> <span className="purple">{item.getIn(['show_description', 'show_name'])}</span> 
      </div>
    </a>
  </Link>
)

export default ChannelInfo
