
import React from 'react';

import {
  CONVERTING,
  LIVE
} from 'helpers/text'

import {
  notdefined
} from 'helpers/errors'

const MergeStatus = ({item}) => {
  if (
    item.get('live') || 
    item.get('merge_status') === 0) 
  {
    return <span className='live'>{LIVE}</span>
  } else if (
    item.get('merge_status') === 1 || 
    item.get('merge_status') === 4 || 
    item.get('merge_status') === 5 || 
    item.get('merge_status') === 6 || 
    item.get('merge_status') === 7 || 
    item.get('merge_status') === 8) 
  {
    return <span className="converting">{CONVERTING}</span>
  } else if (
    item.get('merge_status') === 2) 
  {
    return <span className="notlive">{item.get('length')}</span>
  }
  return <span className="notlive">{notdefined}</span>
}

export default MergeStatus