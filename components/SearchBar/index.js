import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Link, Router } from "routes";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faChevronDown, faTimes, faHashtag } from '@fortawesome/free-solid-svg-icons';

import { lowerCase } from 'helpers/lowercase';
import { 
  AT,
  DOT,
  SEARCH,
  VIDEOS,
  HASHTAGS,
  USERS,
  LIVE,
  NO_RESULTS_FOUND
 } from "helpers/text";
 import {
  getSearchDropdownRequest,
  getSearchRequest,
  searchInputUnhide,
  searchInputHide,
  clearResults
 } from 'store/search/actions'

import Input from "components/Input";
import Avatar from "components/Avatar";
import CheckLink from "components/CheckLink";
import Spinner from "components/Spinner";

class SearchBar extends Component {
  constructor() {
    super();

    this.state = {
      search: '',
      searchPrevious: '',
      type: lowerCase(VIDEOS),
      showClear: false,
    }

    this.onSearchChange = this.onSearchChange.bind(this);
    this.onSearchKeyUp = this.onSearchKeyUp.bind(this);
    this.onSearchFocus = this.onSearchFocus.bind(this);
    this.onTypeChange = this.onTypeChange.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }
  componentDidMount() {
    document.body.onclick = (e) => {
      if (!e.target.closest('.search-container')) {
        this.props.searchInputHide()
      }
    };
  }
  componentDidUpdate(prevProps, prevState) {
    const {searchPrevious, search} = this.state;
    if (prevState.searchPrevious !== searchPrevious && searchPrevious === search && search.length >= 2) {
      this.props.getSearchDropdownRequest(search, 5)
    }
  }
  onSearchChange(e) {
    const { value } = e.target;
    this.setState({ search: value });
    setTimeout(() => {
      this.setState({ searchPrevious: value });
    }, 1000)
  }
  onSearchKeyUp(e) {
    const { search, type } = this.state;
    if (e.key !== 'Enter') {
      if (search.length > 0) {
        this.setState({ showClear: true })
      } else {
        this.handleClear();
      }
    } else {
      if (search.length > 0) {
        Router.pushRoute('search', {type, search})
      }
    }
  }
  onSearchFocus() {
    this.props.searchInputUnhide();
  }
  onTypeChange(e) {
    const { search } = this.state;
    this.setState({ type: e.target.value })
    if (search.length >= 2) {
      this.props.getSearchDropdownRequest(search, 5)
    }
  }

  handleClear(e) {
    this.setState({ showClear: false, search: '', searchPrevious: '' });
    this.props.clearResults();
  }
  renderSearchResults = (item, index) => (
    index <= 5 ? (
      <div className="entry" key={index}>
        {
          this.state.type === lowerCase(VIDEOS) ? (
            <CheckLink route="video" params={{id: item.get('id')}} item={item}>
              {
                (item.get('live')) ? (
                  <div>
                    <span className='live'>{LIVE}</span><span className="title">{item.get('title')}</span><span> — {item.getIn(['user', 'name_display'])} {DOT} {AT}{item.getIn(['user', 'username'])}</span>
                  </div>
                ) : (
                  <div>
                    <span className='notlive'>{item.get('length_string')}</span><span className="title">{item.get('title')}</span><span> — {item.getIn(['user', 'name_display'])} {DOT} {AT}{item.getIn(['user', 'username'])}</span>
                  </div>
                )
              }
            </CheckLink>
          ) : this.state.type === lowerCase(HASHTAGS) ? (
            <CheckLink route="video" params={{id: item.get('id')}} item={item}>
              <div>
                <span><FontAwesomeIcon icon={faHashtag} size="lg" /></span>
                <span className="title">{item.get('name')}</span>
              </div>
            </CheckLink>
          ) : this.state.type === lowerCase(USERS) ? (
            <Link route="profile" params={{slug: item.get('username')}}>
              <a target="_blank" title={item.get('name_display')}>
                <div>
                  <span><Avatar size="small" username={item.get('name_display')} photo={item.get('photo')} /></span>
                  <span>{item.get('name_display')} {DOT} {AT}{item.get('username')}</span>
                </div>
              </a>
            </Link>
          ) : null
        }
      </div>
    ) : null
  )
  render() {
    const { loadingSearch, results, searchInputDropdown } = this.props;
    const { search, type, showClear } = this.state;
    const typeDict = [
      { value: VIDEOS },
      { value: HASHTAGS },
      { value: USERS },
    ];
    return (
      <section id="search-bar">
        <div className="search-container">
          <div className="search-grid">
            <div className="search-input">
              <span className="search-icon"><FontAwesomeIcon icon={faSearch} size="xs" /></span>
              <Input 
                type="text"
                theme="search"
                placeholder={SEARCH}
                name={SEARCH}
                value={search}
                onChange={this.onSearchChange}
                onKeyUp={this.onSearchKeyUp}
                onFocus={this.onSearchFocus}
                autoComplete={false}
              />
              {
                (showClear) ? (
                  <span className="clear"><FontAwesomeIcon icon={faTimes} onClick={this.handleClear} /></span>
                ) : null
              }
              {
                loadingSearch ? (
                  <div className={`recent-searches ${searchInputDropdown ? 'hide' : '' }`}>
                    <Spinner type="relative" color="white" />
                  </div>
                ) : (results) ? (
                  <div className={`recent-searches ${searchInputDropdown ? 'hide' : '' }`}>
                    <div className="search-results">
                      {
                        type === lowerCase(VIDEOS) && results.get('videos').size > 0 ? (
                          results.get('videos').map((item, index) => this.renderSearchResults(item, index))
                        ) : type === lowerCase(HASHTAGS) && results.get('hashtag').size > 0 ? (
                          results.get('hashtag').map((item, index) => this.renderSearchResults(item, index))
                        ) : type === lowerCase(USERS) && results.get('user').size > 0 ? (
                          results.get('user').map((item, index) => this.renderSearchResults(item, index))
                        ) : (
                          <div className="nothing">{NO_RESULTS_FOUND}</div>
                        )
                      }
                    </div>
                  </div>
                ) : null
              }
            </div>
            <div className="type-dropdown">
              <select onChange={this.onTypeChange} value={type}>
                {typeDict.map(option => <option key={lowerCase(option.value)} value={lowerCase(option.value)}>{ option.value }</option>)}
              </select>
              <span className="dropdown-icon"><FontAwesomeIcon icon={faChevronDown} size="xs" /></span>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
  const mapStateToProps = state => ({
    loadingSearch: state.getIn(['search', 'loadingSearch']),
    results: state.getIn(['search', 'results', 'data']),
    searchInputDropdown: state.getIn(['search', 'searchInputHide'])
  });
  
  const mapDispatchToProps = dispatch =>
    bindActionCreators(
    {
      getSearchDropdownRequest,
      getSearchRequest,
      searchInputUnhide,
      searchInputHide,
      clearResults
    },
    dispatch
  );
  
  export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)
