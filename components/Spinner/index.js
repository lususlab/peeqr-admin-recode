import React from 'react';

const Spinner = ({type, color}) => (
  type ? (
    <div className={`spinner-wrap ${type}`}><div className={`spinner ${color}`} /></div>
  ) : (
    <span className={`spinner ${color}`} />
  )
  
)

export default Spinner