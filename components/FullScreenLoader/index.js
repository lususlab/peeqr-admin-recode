import React from 'react';
import RandomGirl from '../RandomGirl';

const FullScreenLoader = (props) => (
  <div className="girl-spinner-wrapper">
    <div className="girl-spinner">
      <RandomGirl />
    </div>
    {props.children}
  </div>
);

export default FullScreenLoader