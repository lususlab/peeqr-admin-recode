import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Link } from "routes";

import {
  showModal,
} from 'store/video/actions'

class CheckLink extends Component {
  constructor(props) {
    super(props);

    this.state = {
      blockedUsers: props.blockedList.size > 0 ? props.blockedList.map(user => user.get('username')) : [],
    }
    
    this.handleShowModalActiveChannel = this.handleShowModalActiveChannel.bind(this);
    this.handleShowModalActiveBlock = this.handleShowModalActiveBlock.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    const { blockedList } = this.props;
    const { blockedUsers } = this.state;
    if (blockedList !== prevProps.blockedList && blockedList) {
      this.setState({ blockedUsers: blockedList.size > 0 ? blockedList.map(user => user.get('username')) : [] })
    }
  }
  handleShowModalActiveChannel = () => {
    const { showModal, item } = this.props;
    showModal(item, 'access')
  }
  handleShowModalActiveBlock = () => {
    const { showModal, item } = this.props;
    showModal(item, 'block')
  }

  render() {
    const { route, params, children, item } = this.props;
    const { blockedUsers } = this.state;

    return (
      blockedUsers.indexOf(item.getIn(['user', 'username'])) > -1 ? (
        <a onClick={this.handleShowModalActiveBlock}>
          {children}
        </a>
      ) : (
        (!item.get('show_id') || item.get('show_id') && item.getIn(['show_description', 'purchase_status'])) ? (
          <Link route={route} params={params}>
            <a title={item.get('title')}>
              {children}
            </a>
          </Link>
        ) : (
          <a onClick={this.handleShowModalActiveChannel}>
            {children}
          </a>
        )
      )
    )
  }
}
const mapStateToProps = state => ({
  me: state.getIn(['user', 'me']),
  blockedList: state.getIn(['user', 'blockedList']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    showModal,
  },
  dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(CheckLink)