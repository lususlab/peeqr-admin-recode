

import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Transition from 'react-transition-group/Transition';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
  faBell,
} from '@fortawesome/free-solid-svg-icons';

import { closeonclick } from "helpers/closeonclick";

import { 
  clearNotificationsRequest,
  cancelClearNotificationsRequest
} from 'store/user/actions';

import BubbleMenu from '../BubbleMenu';
import NotificationsList from '../NotificationsList';

class Notifications extends Component {
  constructor() {
    super();

    this.state = {
      notificationsActive: false,
    }
    this.handleNotificationsActive = this.handleNotificationsActive.bind(this);
    this.handleNotificationsInactive = this.handleNotificationsInactive.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    const { notificationsActive } = this.state;

    if (notificationsActive !== prevState.notificationsActive && notificationsActive) {
      closeonclick(this.handleNotificationsInactive);
    }
  }
  handleNotificationsActive() {
    const {jwt, clearNotificationsRequest} = this.props;
    this.setState({ notificationsActive: true })
    clearNotificationsRequest(jwt);
  }
  handleNotificationsInactive() {
    const {cancelClearNotificationsRequest} = this.props; 
    this.setState({ notificationsActive: false })
    cancelClearNotificationsRequest();
  }
  renderNotifications = (notificationsActive, notifications, gettingNotifications) => (
    <Transition
      timeout={150}
      in={notificationsActive}
      mountOnEnter
      unmountOnExit
    >
      {status => (
        <BubbleMenu classes={`fade-${status} bottom right large`}>
          <NotificationsList 
            gettingNotifications={gettingNotifications}
            notifications={notifications}
          />
        </BubbleMenu>
      )}
    </Transition>
  )
  render() {
    const { notifications, gettingNotifications, clearedNotifications } = this.props;
    const { notificationsActive } = this.state;
    return (
      <div className="notification-wrapper">
        <a onClick={this.handleNotificationsActive}><FontAwesomeIcon icon={faBell} size="lg" />
          {
            notifications.get('count') > 0 && notifications.get('results').some(item => !item.get('is_read')) ? (
              <span className="unread" />
            ) : null
          }
        </a>
        { this.renderNotifications(notificationsActive, notifications, gettingNotifications, clearedNotifications) }
      </div>
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    clearNotificationsRequest,
    cancelClearNotificationsRequest
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Notifications)
