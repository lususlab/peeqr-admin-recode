import React from 'react';
import {Router} from 'routes';
import {REDIRECTING} from 'helpers/text';

const Error = props => {
  if (props.redirect) {
    setTimeout(() => {
      Router.pushRoute(props.redirect)
    }, 2000)
  }
  return (
    <div className="error">
      {props.error}
      {
        props.redirect ? (
          <div className="redirect">{REDIRECTING}</div>   
        ) : null
      }
    </div>
  )}

export default Error