import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faCheck } from '@fortawesome/free-solid-svg-icons';

import { 
  subscribeRequest,
  unsubscribeRequest,
} from 'store/user/actions';

import {
  SUBSCRIBE,
  UNSUBSCRIBE, 
  SIGN_IN_TO_SUBSCRIBE
} from "helpers/text";

import {createNotification} from 'helpers/createnotification'

import Spinner from "components/Spinner";

class SubscribeButton extends Component {
  constructor() {
    super();

    this.handleSubscribe = this.handleSubscribe.bind(this)
  }

  handleSubscribe() {
    const {stat, unsubscribeRequest, subscribeRequest, userId, username, jwt, me} = this.props;
    if (me.size > 0) {
      if (stat) {
        unsubscribeRequest(userId, username, jwt) 
      } else {
        subscribeRequest(userId, username, jwt)
      }
    } else {
      createNotification('error', SIGN_IN_TO_SUBSCRIBE)
    }
  }

  render() {
    const { stat, subscribing } = this.props;

    return (
      <div className="item subscribe">
        <a className={subscribing ? 'disabled' : ''} onClick={this.handleSubscribe}>
          <div className={`circle ${stat ? 'green' : ''}`}>
            {
              subscribing ? (
                <Spinner color="white" />
              ) : (
                <FontAwesomeIcon icon={stat ? faCheck : faPlus} size="xs" />
              )  
            }
          </div>
          <span>{stat ? UNSUBSCRIBE : SUBSCRIBE}</span>
        </a>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  subscribing: state.getIn(['user', 'subscribing']),
  me: state.getIn(['user', 'me'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      subscribeRequest,
      unsubscribeRequest,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SubscribeButton)
