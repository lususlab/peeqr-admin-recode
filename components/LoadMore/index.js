import React from 'react';

import { LOAD_MORE } from "helpers/text";

import Button from 'components/Button';
import Spinner from 'components/Spinner';

const LoadMore = (props) => (
  <Button size={props.size} color={props.color} background={props.background}>
    {
      props.loading ? (
        <a className="disabled"><Spinner color="purple" /><span>{LOAD_MORE}</span></a>
      ) : (
        <a onClick={props.onClick}><span>{LOAD_MORE}</span></a>
      )
    }
  </Button>
)

export default LoadMore