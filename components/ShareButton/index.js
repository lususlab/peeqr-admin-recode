import React, { Component } from 'react';
import Transition from 'react-transition-group/Transition';
import MediaQuery from 'react-responsive';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faTwitter, faGooglePlusG } from '@fortawesome/free-brands-svg-icons';
import { faLink, faShare } from '@fortawesome/free-solid-svg-icons';
import {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
} from 'react-share'

import { SHARE, SIMPLE_EMBED, THUMBNAIL_EMBED } from "helpers/text";
import { closeonclick } from "helpers/closeonclick";

import Input from 'components/Input';
import Button from 'components/Button';
import BubbleMenu from 'components/BubbleMenu';
import Modal from 'components/Modal';

class ShareButton extends Component {
  constructor() {
    super();
    
    this.state = {
      shareActive: false,
      embedType: 0
    }
    this.handleActive = this.handleActive.bind(this);
    this.handleInactive = this.handleInactive.bind(this);
    this.onShareFocus = this.onShareFocus.bind(this);
    this.onEmbedChange = this.onEmbedChange.bind(this)
  }
  componentDidUpdate(prevProps, prevState) {
    const { shareActive } = this.state;
    if (shareActive !== prevState.shareActive && shareActive) {
      closeonclick(this.handleInactive);
    }
  }
  handleActive() {
    this.setState({ shareActive: true })
  }
  handleInactive() {
    this.setState({ shareActive: false })
  }
  onShareFocus(e) {
    e.target.select();
  }
  onEmbedChange(e) {
    this.setState({ embedType: parseInt(e.target.value) });
  }
  renderContent() {
    const {url, videoId, title, orientation, profile} = this.props;
    const {embedType} = this.state;
    const embedUrl = `<div class="peeqr_embed ${orientation}"><iframe frameBorder='0' src="${window.location.href}/embed/${videoId}?type=${embedType}" allowfullscreen></iframe></div>`

    return (
      <div>
        <div className="share-grid">
          <FacebookShareButton
            url={url}
            quote={title}
          >
            <Button size="round" background="bgfbblue" color="white" onClick={this.handleFacebookShare}>
              <a><FontAwesomeIcon icon={faFacebookF} /></a>
            </Button>
          </FacebookShareButton>
          <TwitterShareButton
            url={url}
            title={title}
          >
            <Button size="round" background="bgtwblue" color="white" onClick={this.handleTwitterShare}>
              <a><FontAwesomeIcon icon={faTwitter} /></a>
            </Button>
          </TwitterShareButton>
          <GooglePlusShareButton
            url={url}
          >
            <Button size="round" background="bggored" color="white" onClick={this.handleGoogleShare}>
              <a><FontAwesomeIcon icon={faGooglePlusG} /></a>
            </Button>
          </GooglePlusShareButton>
        </div>
        {
          !profile ? (
            <div className="tip">
              <form>
                <Input 
                  type="radio"
                  theme="radio"
                  label={SIMPLE_EMBED}
                  name={SIMPLE_EMBED}
                  onChange={this.onEmbedChange}
                  checked={embedType === 0}
                  value={0}
                />
                <Input 
                  type="radio"
                  theme="radio"
                  label={THUMBNAIL_EMBED}
                  name={THUMBNAIL_EMBED}
                  onChange={this.onEmbedChange}
                  checked={embedType === 1}
                  value={1}
                />
              </form>
            </div>
          ) : null
        }
        {
          !profile ? (
            <div className="url">
              <Input 
                type="text"
                theme="boxed"
                name={SHARE}
                value={embedUrl}
                onFocus={this.onShareFocus}
                required
                readOnly
              />
              <div className="url-icon">
                <FontAwesomeIcon icon={faLink} size="xs" />
              </div>
            </div>
          ) : null
        }
      </div>
    )
  }
  render() {
    const { position } = this.props;
    const { shareActive } = this.state;
    return (
      <div className="item share">
        <a onClick={this.handleActive}>
          <FontAwesomeIcon icon={faShare} />
          <span className="value">{ SHARE }</span>
        </a>
        <MediaQuery minWidth={768}>
          <Transition
            timeout={150}
            in={shareActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <BubbleMenu classes={`fade-${status} ${position}`} >
                {this.renderContent()}
              </BubbleMenu>
            )}
          </Transition>
        </MediaQuery>
        <MediaQuery maxWidth={767}>
          <Transition
            timeout={150}
            in={shareActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <Modal classes={`fade-${status}`} close={() => this.handleInactive}>
                {this.renderContent()}
              </Modal>
            )}
          </Transition>
        </MediaQuery>
      </div>
    )
  }
}

export default ShareButton;
