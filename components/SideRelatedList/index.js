import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Truncate from 'react-truncate';
import TimeAgo from 'react-timeago';
import { Link } from "routes";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLemon } from '@fortawesome/free-regular-svg-icons';

import Avatar from 'components/Avatar';
import MergeStatus from 'components/MergeStatus';
import CheckLink from 'components/CheckLink';
import LoadMore from 'components/LoadMore';
import Spinner from 'components/Spinner';

import {
  loadMoreFreshVideosRequest
} from 'store/video/actions'

import { RELATED, LOAD_MORE, CURRENCY } from "helpers/text"

class SideRelatedList extends Component {
  constructor() {
    super()

    this.handleLoadMore = this.handleLoadMore.bind(this)
  }

  handleLoadMore = (e) => {
    const { videoList, loadMoreFreshVideosRequest } = this.props;
    e.preventDefault();

    loadMoreFreshVideosRequest(videoList.get('next'))
  }
  renderVideoList = (item, i) => (
    <div key={`${item.get('id')}-${i}`} className="related">
      <div className="user">
        {
          item.get('show_id') ? (
            <span className={`tier_avatar circle ${item.getIn(['show_description', 'purchase_status']) ? 'green' : 'red' }`}> {CURRENCY} </span>
          ) : null
        }
        <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
          <a target="_blank">
            <Avatar size="medium" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} />
          </a>
        </Link>
      </div>
      <div className="description">
        <MergeStatus item={item} /><span className="ago"><TimeAgo date={item.get('upload_date')} /></span>
        <CheckLink route="video" params={{id: item.get('id')}} item={item}>
          <p>
            <Truncate lines={1} ellipsis={<span>...</span>}>{item.get('title')}</Truncate>
          </p>
        </CheckLink>
      </div>
    </div>
  )
  render() {
    const { videoList, gettingVideos, loadingMore } = this.props;
    return (
      gettingVideos ? (
        <Spinner type="relative" color="grey" />
      ) : (
        <div className="related-list">
          <h4><FontAwesomeIcon icon={faLemon} size="lg" /><span>{RELATED}</span></h4>
          {
            videoList.get('results').map((item, i) => (
              this.renderVideoList(item, i)
            ))
          }
          {
            (videoList.get('next') !== null && !gettingVideos) ? (
              <div className="centered">
                <LoadMore size="small" color="white" background="bggrey" onClick={this.handleLoadMore} loading={loadingMore} />
              </div>
            ) : null
          }
        </div>
      )
    )
  }
}

const mapStateToProps = state => ({
  loadingMore: state.getIn(['video', 'loadingMore'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    loadMoreFreshVideosRequest
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(SideRelatedList)
