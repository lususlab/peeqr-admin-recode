import React, { Component } from 'react';
import CountUp from 'react-countup';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class GenericCounter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultValue_start: 0,
      defaultValue_end: props.value
    }
    this.onCompleteCount = this.onCompleteCount.bind(this)
  }
  componentDidMount() {
    const { socket, type } = this.props;

    socket.on(type, data => {
      this.handleSocketData(data);
    });

  }
  handleSocketData = (data) => {
    const { listenFor, type } = this.props;
    if (data.cmd === type) {
      this.setState({
        defaultValue_end: parseInt(data[listenFor])
      })
    }
  }
  onCompleteCount = () => {
    this.setState({
      defaultValue_start: this.state.defaultValue_end
    })
  }
  render() {
    const { image, icon, classes, title } = this.props;
    const { defaultValue_start, defaultValue_end } = this.state;
    return (
      <div className={`item ${classes}`} title={title}>
        {
          image ? (
            <img src={image} />
          ) : (
            <FontAwesomeIcon icon={icon} size="md" />
          )
        }
        <CountUp start={defaultValue_start} end={defaultValue_end} onEnd={this.onCompleteCount}/>
      </div>
    )
  }
}

export default GenericCounter