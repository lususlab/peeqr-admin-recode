
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const Modal = (props) => (
  <div className={`modal-wrapper fade ${props.classes ? props.classes : ''}`}>
    <div className="overlay">
      <div className="modal">
        <div className="close-wrapper">
          <div className="close" onClick={props.close()}><FontAwesomeIcon icon={faTimes} size="lg" /></div>
        </div>
        <div className="modal-content">
          {props.children}
        </div>
      </div>
    </div>
  </div>
)

export default Modal