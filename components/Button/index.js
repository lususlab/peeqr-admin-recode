import React from 'react';

const Button = (props) => (
  <div className={`button ${props.size ? props.size : ''} ${props.background} ${props.color}`}>
    {props.children}
  </div>
)

export default Button;
