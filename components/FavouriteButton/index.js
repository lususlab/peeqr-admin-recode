import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as faStarFilled } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-regular-svg-icons';

import { 
  favouriteRequest,
  unfavouriteRequest,
} from 'store/user/actions';

import {
  FAVOURITE,
  SIGN_IN_TO_FAVOURITE
} from "helpers/text";

import {
  NO_ACCESS
} from "helpers/errors"

import { createNotification } from 'helpers/createnotification';
import { videoCurrentTime } from 'helpers/videohelper';

import Spinner from "components/Spinner";

class FavouriteButton extends Component {
  constructor() {
    super();

    this.favRef = React.createRef()
    this.handleFavourite = this.handleFavourite.bind(this)
  }

  handleFavourite() {
    const {stat, favouriteRequest, unfavouriteRequest, videoId, mergeStatus, jwt, me, playerState, hasAccess, isShow} = this.props;
    const timestamp = videoCurrentTime(playerState, mergeStatus);

    if (!isShow || hasAccess && isShow) {
      if (me.size > 0) {
        if (stat) {
          unfavouriteRequest(videoId, timestamp, jwt) 
        } else {
          const node = this.favRef.current;
          node.classList.add('jumpUp')
          setTimeout(() => {
            node.classList.remove('jumpUp')
          }, 800)
          favouriteRequest(videoId, timestamp, jwt)
        }
      } else {
        createNotification('error', SIGN_IN_TO_FAVOURITE)
      }
    } else {
      createNotification('error', NO_ACCESS)
    }
  }
  render() {
    const { stat, favouriting } = this.props;
    return (
      <a className={favouriting ? 'disabled' : ''} onClick={this.handleFavourite}>
        <div className={`item favourite ${stat ? 'favourited' : ''}`}>
          {
            favouriting ? (
              <Spinner color="grey" />
            ) : (
              <FontAwesomeIcon icon={stat ? faStarFilled : faStar} />
            )
          }
          <span ref={this.favRef} className="hidden-favourite hidden-icon"><FontAwesomeIcon icon={faStarFilled} /></span>
          <span className="value">{FAVOURITE}</span>
        </div>
      </a>
    )
  }
}
const mapStateToProps = state => ({
  favouriting: state.getIn(['user', 'favouriting']),
  me: state.getIn(['user', 'me'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      favouriteRequest,
      unfavouriteRequest,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(FavouriteButton)
