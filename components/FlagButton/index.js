import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Transition from 'react-transition-group/Transition';
import MediaQuery from 'react-responsive';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFlag as faFlagFilled, faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { faFlag } from '@fortawesome/free-regular-svg-icons';

import { FLAG_AS_INAPPROPRIATE, UNFLAG, MORE, SIGN_IN_TO_FLAG } from "helpers/text";
import { closeonclick } from "helpers/closeonclick";
import {
  NO_ACCESS
} from "helpers/errors"

import { 
  flagRequest,
  unflagRequest,
} from 'store/user/actions';

import Button from 'components/Button';
import BubbleMenu from 'components/BubbleMenu';
import Modal from 'components/Modal';
import Spinner from 'components/Spinner';

import {createNotification} from 'helpers/createnotification'

class FlagButton extends Component{
  constructor() {
    super();
    
    this.state = {
      flagActive: false,
    }

    this.flagRef = React.createRef()
    this.handleActive = this.handleActive.bind(this);
    this.handleInactive = this.handleInactive.bind(this);
    this.handleFlag = this.handleFlag.bind(this)
  }
  componentDidUpdate(prevProps, prevState) {
    const { flagActive } = this.state;
    if (flagActive !== prevState.flagActive && flagActive) {
      closeonclick(this.handleInactive);
    }
  }
  handleActive() {
    this.setState({ flagActive: true })
  }
  handleInactive() {
    this.setState({ flagActive: false })
  }
  handleFlag() {
    const {stat, flagRequest, unflagRequest, videoId, jwt, me, hasAccess, isShow} = this.props;
    
    if (!isShow || hasAccess && isShow) {
      if (me.size > 0) {
        if (stat) {
          unflagRequest(videoId, jwt) 
        } else {
          const node = this.flagRef.current;
          node.classList.add('jumpUp')
          setTimeout(() => {
            node.classList.remove('jumpUp')
          }, 800)
          flagRequest(videoId, jwt)
        }
      } else {
        createNotification('error', SIGN_IN_TO_FLAG)
      }
    } else {
      createNotification('error', NO_ACCESS)
    }
  }
  renderContent() {
    const { stat, flagging } = this.props;
    return (
      <a className={flagging ? 'disabled' : ''} onClick={this.handleFlag}>
        <div className={`item flag ${stat ? 'flagged' : ''}`}>
          {
            flagging ? (
              <Spinner color="grey" />
            ) : (
              <FontAwesomeIcon icon={stat ? faFlagFilled : faFlag} />
            )
          }
          <span ref={this.flagRef} className="hidden-flag hidden-icon"><FontAwesomeIcon icon={faFlagFilled} /></span>
          <span className="value">{stat ? UNFLAG : FLAG_AS_INAPPROPRIATE}</span>
        </div>
      </a>
    )
  }
  render() {
    const { position } = this.props;
    const { flagActive } = this.state;
    return (
      <div className="item more">
        <a onClick={this.handleActive}>
          <FontAwesomeIcon icon={faEllipsisH} />
          <span className="value">{ MORE }</span>
        </a>
        <MediaQuery minWidth={768}>
          <Transition
            timeout={150}
            in={flagActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <BubbleMenu classes={`fade-${status} ${position}`} >
                {this.renderContent()}
              </BubbleMenu>
            )}
          </Transition>
        </MediaQuery>
        <MediaQuery maxWidth={767}>
          <Transition
            timeout={150}
            in={flagActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <Modal classes={`fade-${status}`} close={() => this.handleInactive}>
                {this.renderContent()}
              </Modal>
            )}
          </Transition>
        </MediaQuery>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  flagging: state.getIn(['user', 'flagging']),
  me: state.getIn(['user', 'me'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      flagRequest,
      unflagRequest,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(FlagButton)
