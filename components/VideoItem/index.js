import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import TimeAgo from 'react-timeago';
import Truncate from 'react-truncate';
import { Link } from "routes";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import Avatar from 'components/Avatar';
import VideoThumbnail from 'components/VideoThumbnail';
import CheckLink from 'components/CheckLink';
import MergeStatus from 'components/MergeStatus';
import Button from 'components/Button';

import {
  showModal,
} from 'store/video/actions'

import { 
  CURRENCY,
  DELETE_VIDEO,
  EXPORT_VIDEO,
} from 'helpers/text';

import {lowerCase} from 'helpers/lowercase'

class VideoItem extends Component {
  constructor() {
    super();

    this.handleDeleteVideoActive = this.handleDeleteVideoActive.bind(this);
  }
  handleDeleteVideoActive(e) {
    const { showModal, item, jwt } = this.props;
    showModal(item, 'delete', jwt)
  }
  render() {
    const { item, className, isSelf } = this.props;
    return (
      <div order={item.get('order')} className={`${className} video-item`}>
        <CheckLink route="video" params={{id: item.get('id')}} item={item}>
          <VideoThumbnail alt={item.get('title')} thumbnail={item.get('thumbnail')} cover={item.getIn(['user', 'cover_photo'])} photo={item.getIn(['user', 'photo'])} />
        </CheckLink>
        {
          isSelf ? (
            <div className="self-options">
              <Button size="round" background="bggored" color="white">
                <a onClick={this.handleDeleteVideoActive} title={DELETE_VIDEO}><FontAwesomeIcon icon={faTrashAlt} /></a>
              </Button>
              <Button size="round" background="bglightpurple" color="white">
                <a target="_blank" href={item.get('mp4_url')} title={EXPORT_VIDEO}><FontAwesomeIcon icon={faDownload} /></a>
              </Button>
            </div>
          ) : null
        }
        <div className="meta">
          <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
            <a target="_blank" title={item.getIn(['user', 'name_display'])}>
              <Avatar size="large" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} />
            </a>
          </Link>
          <div className="description">
            {
              item.get('show_id') ? (
                <span className="tier"><span className="tier-wrap">{item.getIn(['show_description', 'purchase_status']) ? <span className="circle green" /> : <span className="circle red" />} {CURRENCY} {item.get('ticket_tier') ? item.getIn(['ticket_tier', 'tier_name']) : ''}</span></span>
              ) : null
            }
            <MergeStatus item={item} />
            <CheckLink route="video" params={{id: item.get('id')}} item={item}>
              <h5>
                <Truncate lines={3} ellipsis={<span>...</span>}>{item.get('title')}</Truncate>
              </h5>
            </CheckLink>
            <div className="ago"><TimeAgo date={item.get('upload_date')} /></div>
          </div>
        </div>
        
      </div>
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    showModal,
  },
  dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(VideoItem)
