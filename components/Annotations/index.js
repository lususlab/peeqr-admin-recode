import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { fromJS } from 'immutable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink } from '@fortawesome/free-solid-svg-icons';

import { Link } from 'routes';

import { formatVideoTime } from 'helpers/videohelper'

let playerW, playerH, vodAnnotations;

class Annotations extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      apiAnnotations: props.annotations.get('results'),
      currentTime: props.playerState ? props.playerState.currentTime : null
    }

    this.handleSocketAnnotation = this.handleSocketAnnotation.bind(this);
    this.handleVodAnnotations = this.handleVodAnnotations.bind(this);
    this.renderAnnotations = this.renderAnnotations.bind(this);
  }
  async componentDidMount() {
    const { socket } = this.props;
    const { currentTime } = this.state;

    socket.on('annotation', data => {
      this.handleSocketAnnotation(data);
    });
    socket.on('deleteAnnotation', data => {
      this.handleSocketAnnotation(data);
    });

    playerW = await document.querySelector('.playback').clientWidth;
    playerH = await document.querySelector('.playback').clientHeight;

    vodAnnotations = await document.querySelectorAll('.annotation')
  }

  componentDidUpdate(prevProps) {
    const { playerState } = this.props;
    if (playerState && prevProps.playerState.currentTime !== playerState.currentTime) {
      this.handleVodAnnotations(playerState.currentTime, playerState.duration)
    }
  }

  handleVodAnnotations(currentTime, duration) {
    for (let i = 0; i < vodAnnotations.length; ++i) {
      const place = formatVideoTime(vodAnnotations[i].getAttribute('data-place'));
      const release = formatVideoTime(vodAnnotations[i].getAttribute('data-release')) || duration;
      
      if (currentTime > place && currentTime < release) {
        vodAnnotations[i].classList.remove('hide')
      } else {
        vodAnnotations[i].classList.add('hide')
      }
    }
  }

  handleSocketAnnotation(data) {
    const {apiAnnotations} = this.state;

    this.setState({ apiAnnotations: fromJS(data.data.data) });
  }

  renderAnnotations(item) {
    const {videoWidth, videoHeight} = this.props;

    const itemW = item.get('bw') === "SCREEN_WIDTH" ? item.get('bw') : parseInt(item.get('bw'));
    const itemH = parseInt(item.get('bh'));
    const itemX = parseInt(item.get('posx'));
    const itemY = parseInt(item.get('posy'));
    const itemFontSize = parseInt(item.getIn(['annotation', 'size']));

    const ratio = item.get('ratio').split(':');
    const deviceRatioW = videoWidth/ratio[0]; 
    const deviceRatioH = videoHeight/ratio[1];
    const screenRatioW = playerW/videoWidth;
    const screenRatioH = playerH/videoHeight;

    let annW, annH, annL, annT, annSize, maxAnnW, annTransform;

    annW = itemW * screenRatioW;
    annH = itemH * screenRatioH;
    annL = Math.max(0, (itemX * deviceRatioW * screenRatioW));
    annT = Math.max(0, (itemY * deviceRatioH * screenRatioH));
    annTransform = `translate(${annL}px, ${annT}px)`;
    annSize = Math.min(24, Math.max(13, itemFontSize * screenRatioW));
    maxAnnW = playerW;

    return (
      <div 
        id={`annotation-${item.get('id')}`}
        className={`annotation ${item.getIn(['annotation', 'background']) ? 'background' : ''}`} 
        key={item.get('id')}
        data-place={item.get('place')}
        data-release={item.get('release')}
        style={{ 
          width: itemW === 'SCREEN_WIDTH' ? playerW : annW, 
          top: annT, 
          left: annL, 
          fontSize: annSize 
        }}
      >
      {
        item.getIn(['annotation', 'link']) ? (
          <Link route={item.getIn(['annotation', 'link'])}>
            <a target="_blank">
              <span><FontAwesomeIcon icon={faLink} size="sm" /></span>
              <span>{item.getIn(['annotation', 'name'])}</span>
            </a>
          </Link>
        ) : (
          <span>{item.getIn(['annotation', 'name'])}</span>
        )
      }
      </div>
    )
  }

  render() {
    const { gettingAnnotations } = this.props;
    const { apiAnnotations } = this.state;
    return (
      <div className="annotation-overlay">
      {
        !gettingAnnotations && apiAnnotations.size > 0 ? (
          apiAnnotations.map(item => this.renderAnnotations(item))
        ) : null
      }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  annotations: state.getIn(['video', 'annotations']),
  gettingAnnotations: state.getIn(['video', 'gettingAnnotations'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Annotations)
