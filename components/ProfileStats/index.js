import React from 'react';
import Pluralize from 'react-pluralize';

const ProfileStats = ({classes, icon, counter, singular, prefix}) => (
  <div className={classes}>
    <div className="profile-icon"><img src={icon} /></div>
    <h3 className="counter">{counter}</h3>
    <div className="tip">{prefix} <Pluralize singular={singular} count={counter} showCount={false} /></div>
  </div>
)

export default ProfileStats