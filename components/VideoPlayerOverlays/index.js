import React from 'react';

import Avatar from 'components/Avatar';

import {
  PAUSED,
  IS_CONVERTING,
  HAS_ENDED
} from 'helpers/text';

const VideoPlayerOverlays = ({isSystemEnded, isSystemPaused, isUserPaused, isConverting, video}) => (
  <div className={`video-overlay ${isSystemEnded || isSystemPaused || isUserPaused || isConverting ? 'active' : ''}`}
    style={ { 
      backgroundImage: 
        isSystemEnded || isSystemPaused || isUserPaused || isConverting ? (
        `url(${video.get('thumbnail')})` 
        ) : (
          ''
        )
    } }
  >
    {
      isSystemEnded ? (
        <div className="end">
          <Avatar size="large" username={video.getIn(['user', 'name_display'])} photo={video.getIn(['user', 'photo'])} />
          <h4>{video.getIn(['user', 'name_display'])}</h4>
          <h6>{HAS_ENDED}</h6>
        </div>
      ) : isSystemPaused ? (
        <h5>{PAUSED}</h5>
      ) : isUserPaused ? (
        <div className="resumeButton" />
      ) : isConverting ? (
        <h5>{IS_CONVERTING}</h5>
      ) : null
    }
  </div>
)

export default VideoPlayerOverlays