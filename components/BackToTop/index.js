import React, { Component } from 'react';
import { throttle } from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';

class BackToTop extends Component {
  constructor() {
    super();

    this.checkScrollTop = this.checkScrollTop.bind(this);
    this.handleToTop = this.handleToTop.bind(this);
    this.throttledFunction = throttle(this.checkScrollTop, 500);
  }
  componentDidMount() {
    window.addEventListener('scroll', this.throttledFunction, false);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.throttledFunction, false);
  }
  checkScrollTop = () => {
    if (window.scrollY > 160) {
      document.querySelector(".back-to-top").classList.add('active')
    } else {
      document.querySelector(".back-to-top").classList.remove('active')
    }
  }
  handleToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }
  render() {
    return (
      <div className="back-to-top" onClick={this.handleToTop}>
        <FontAwesomeIcon icon={faArrowUp} />
      </div >
    )
  }
}

export default BackToTop;