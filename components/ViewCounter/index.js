import React, { Component } from 'react';
import CountUp from 'react-countup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-regular-svg-icons';

import TimeAgo from 'components/TimeAgo'
import Pluralize from 'react-pluralize';

import {
  CONCURRENT,
  MAX,
  VIEWS,
  LIVE_VIEWS,
  TOTAL_VIEWS,
  CONCURRENT_VIEWS,
  MAX_CONCURRENT_VIEWS,
  SEPARATOR
} from "helpers/text";


class ViewCounter extends Component {
  constructor(props) {
    super(props)

    this.state = {
      live_total_start: 0,
      live_concurrent_start: 0,
      max_concurrent_start: 0,
      view_total_start: 0,

      live_total_end: props.item.get('live_view_count'),
      live_concurrent_end: props.item.get('live_concurrent_count'),
      max_concurrent_end: props.item.get('max_concurrent_count'),
      view_total_end: props.item.get('view_count'),
    }

    this.onCompleteViewTotal = this.onCompleteViewTotal.bind(this);
    this.onCompleteLiveTotal = this.onCompleteLiveTotal.bind(this);
    this.onCompleteLiveConcurrent = this.onCompleteLiveConcurrent.bind(this);
    this.onCompleteMaxConcurrent = this.onCompleteMaxConcurrent.bind(this);
  }
  componentDidMount() {
    const { socket } = this.props;

    socket.on('comment', data => {
      this.handleSocketData(data);
    });

  }
  handleSocketData = (data) => {
    if (data.cmd === "view" || data.cmd === "exit_view") {
      this.setState({
        view_total_end: data.view_count,
        live_concurrent_end: data.live_concurrent_count
      })
    }
  }
  onCompleteViewTotal = () => {
    this.setState({
      view_total_start: this.state.view_total_end
    })
  }
  onCompleteLiveTotal = () => {
    this.setState({
      live_total_start: this.state.live_total_end
    })
  }
  onCompleteLiveConcurrent = () => {
    this.setState({
      live_concurrent_start: this.state.live_concurrent_end
    })
  }
  onCompleteMaxConcurrent = () => {
    this.setState({
      max_concurrent_start: this.state.max_concurrent_end
    })
  }

  render() {
    const { item, isSelf, live } = this.props;
    const {
      live_concurrent_start,
      live_total_start,
      max_concurrent_start,
      view_total_start,
      live_concurrent_end,
      live_total_end,
      max_concurrent_end,
      view_total_end,
    } = this.state;

    return (
      <div className="views">
        <FontAwesomeIcon icon={faEye} size="sm" />
        {
          live ? (
            <div>
              <CountUp start={live_concurrent_start} end={live_concurrent_end} onEnd={this.onCompleteLiveConcurrent} /> <span title={CONCURRENT_VIEWS}>{CONCURRENT}</span>
                <span className="divider">{SEPARATOR}</span>
              <CountUp start={live_total_start} end={live_total_end} onEnd={this.onCompleteLiveTotal} /> <span title={LIVE_VIEWS}><Pluralize singular={VIEWS} count={live_total_end} showCount={false} /></span>
            </div>
          ) : (
            isSelf ? (
              <div >
                <CountUp start={max_concurrent_start} end={max_concurrent_end} onEnd={this.onCompleteMaxConcurrent} /> <span title={MAX_CONCURRENT_VIEWS}>{MAX}</span>
                  <span className="divider">{SEPARATOR}</span>
                <CountUp start={view_total_start} end={view_total_end} onEnd={this.onCompleteViewTotal} /> <span title={TOTAL_VIEWS}><Pluralize singular={VIEWS} count={view_total_end} showCount={false} /></span>
              </div>
            ) : (
              <div>
                <CountUp start={view_total_start} end={view_total_end} onEnd={this.onCompleteViewTotal} /> <span title={TOTAL_VIEWS}><Pluralize singular={VIEWS} count={view_total_end} showCount={false} /></span>
              </div>
            )
          )
        }
        {
          item.get('upload_date') ? (
            <TimeAgo date={item.get('upload_date')} />
          ) : null
        }
      </div>
    )
  }
}

export default ViewCounter