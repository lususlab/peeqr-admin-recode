import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Player } from 'video-react';
import { detect } from 'detect-browser';
import { throttle } from 'lodash';

import {
  getAgoraKeyRequest,
  startViewRequest
} from 'store/video/actions'

import VideoPlayerOverlays from 'components/VideoPlayerOverlays';
import Annotations from 'components/Annotations';
import HlsProvider from './hlsProvider';

import { createNotification } from 'helpers/createnotification';
import { videoCurrentTime } from 'helpers/videohelper';

import {
  ERROR_RELOADING,
  ERROR_LOADING,
  FAILED_TO_INIT_PLAYER,
  JOIN_VIDEO_FAILED
} from 'helpers/errors';

const AGORA_ID = process.env.AGORA_ID;
let client, stream, agoraPlayer, agoraVideo, agoraAudio, peeqrPlayers, peeqrVideos, peeqrAudios, peeqrPlayersLength, playerElement, wrapper;

class VideoPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playerIsReady: false,
      currentVolume: '1',
      isSystemPaused: false,
      isUserPaused: false,
      isSystemEnded: false,
      reloadCount: 0,
      playerState: {},
      joining: false,
      videoWidth: 0,
      videoHeight: 0,
    }
    
    this.player = React.createRef();
    this.startAgora = this.startAgora.bind(this);
    this.handleSocketActions = this.handleSocketActions.bind(this)
    this.checkPlayer = this.checkPlayer.bind(this)
    this.handlePlayerStateChange = throttle(this.handlePlayerStateChange.bind(this), 2000)
  }

  async componentDidMount() {
    const { video } = this.props;

    if (video.get('live') && video.get('stream_platform') === 1) {
      await this.startAgora()
    } else {
      await this.checkPlayer()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { isSelf, video, jwt, startViewRequest, channelKey } = this.props;
    const { playerIsReady, playerState } = this.state;

    if (playerIsReady && prevState.playerIsReady !== playerIsReady) {
      if (!isSelf) {
        const timestamp = videoCurrentTime(playerState, video.get('merge_status'));
        startViewRequest(video.get('id'), timestamp, video.get('live'), jwt)
      }
    }
  }

  componentWillUnmount() {
    const { playerIsReady } = this.state;

    if (client && playerIsReady) {
      client.leave();
    }
  }

  checkPlayer() {
    const { reloadCount } = this.state;
    if (this.player.current) {
      this.player.current.subscribeToStateChange(this.handlePlayerStateChange)
      this.player.current.video.video.addEventListener('error', () => {
        if (reloadCount <= 5) {
          this.setState({ reloadCount: reloadCount + 1 });
          createNotification('error', ERROR_RELOADING);
          setTimeout(() => this.player.current.load, 3000);
        } else {
          createNotification('error', ERROR_LOADING);
        }
      });
    } else {
      setTimeout(() => this.checkPlayer, 2000);
    }
  }

  handlePlayerStateChange(state, prevState) {
    const { sendPlayerState } = this.props;
    this.setState({ playerState: state });

    if (state.readyState === 4) {
      this.setState({ playerIsReady: true, videoWidth: state.videoWidth, videoHeight: state.videoHeight })
    }

    sendPlayerState(state);
  }

  runResize(peeqrPlayers, peeqrVideos, wrapper, peeqrPlayersLength) {
    const { video } = this.props;
    if (video.get('orientation') === 0) {
      for (let i = 0; i < peeqrPlayersLength; ++i) {
        peeqrPlayers[i].style.height = 100/peeqrPlayersLength+'%';
        peeqrVideos[i].style.marginTop = (peeqrVideos[i].offsetHeight - peeqrPlayers[i].offsetHeight)/2*-1+'px'
        peeqrVideos[i].style.height = 'auto'
        wrapper.style.flexDirection = "column"
      }
    } else {
      for (let i = 0; i < peeqrPlayersLength; ++i) {
        peeqrPlayers[i].style.width = 100/peeqrPlayersLength+'%';
        peeqrVideos[i].style.width = 'auto'
        wrapper.style.flexDirection = "row"
      }
    }
  }

  startAgora() {
    const { video, me, getAgoraKeyRequest, channelKey, safari, refresh, AgoraRTC } = this.props;
    const self = this;

    let userID;
    if (video) {
      (me.get('id') !== video.getIn(['user', 'id'])) ? (
        userID = me.get('id')
      ) : userID = 0;

      if (safari) {
        client = AgoraRTC.toJS().createClient({mode: 'live', codec: 'h264'});
      } else {
        client = AgoraRTC.toJS().createClient({mode: 'live', codec: 'vp8'});
      }
      client.init(AGORA_ID, () => {
        this.joinChannel(channelKey, video.get('id'), userID);
      }, (err) => {
        createNotification('error', `${FAILED_TO_INIT_PLAYER} (${err})`)
      });

      client.on('error', (err) => {
        createNotification("error", err.reason);
        if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
          client.renewChannelKey(channelKey, () => {
            console.log("Renewed channel key!");
          }, (err) => {
            createNotification("error", `${FAILED_TO_INIT_PLAYER} (${err})`);
          });
        }
        if (err.reason === 'SOCKET_DISCONNECTED') {
          client.leave();
          agoraPlayer.outerHTML = '';
          refresh();
        }
      });
      client.on('stream-added', (evt) => {
        stream = evt.stream;
        client.subscribe(stream, (err) => {
          createNotification('error', `${JOIN_VIDEO_FAILED} (${err})`)
        });
      });
      client.on('stream-subscribed', (evt) => {
        stream = evt.stream;
        wrapper = document.querySelector('.peeqr-wrapper');
        wrapper.setAttribute("id", `peeqr${stream.getId()}`);
        let loaderWrap = document.createElement('div');
        loaderWrap.setAttribute("class", "spinner-wrap relativefull");
        let loader = document.createElement('div');
        loader.setAttribute("class", "spinner white");
        loaderWrap.appendChild(loader);

        if (!document.querySelector(`.playback .peeqr-wrapper`)) {
          document.querySelector('.playback').appendChild(wrapper);
        }
        stream.play('peeqr' + stream.getId());

        stream.player.video.addEventListener( "loadedmetadata", function (e) {
          let width = this.videoWidth,
              height = this.videoHeight;
          
          self.setState({playerIsReady: true, videoWidth: width, videoHeight: height});
        }, false );

        agoraPlayer = document.getElementById(`player_${stream.getId()}`);
        agoraPlayer.classList.add('peeqr-player-prepare');
        agoraPlayer.appendChild(loaderWrap);
        agoraVideo = document.getElementById(`video${stream.getId()}`);
        agoraVideo.classList.add('peeqr-video');
        agoraAudio = document.getElementById(`audio${stream.getId()}`);
        agoraAudio.classList.add('peeqr-audio');

        peeqrPlayers = document.querySelectorAll('.peeqr-player-prepare')
        peeqrVideos = document.querySelectorAll('.peeqr-video')
        peeqrAudios = document.querySelectorAll('.peeqr-audio');

        peeqrPlayersLength = peeqrPlayers.length

        for (let i = 0; i < peeqrPlayersLength; ++i) {
          peeqrVideos[i].muted = true;
        }
        
        this.runResize(peeqrPlayers, peeqrVideos, wrapper, peeqrPlayersLength);

        client.on('stream-removed', (evt) => {
          stream = evt.stream;
          if (document.querySelector(`#player_${+stream.getId()}`)) {
            document.querySelector(`#player_${+stream.getId()}`).outerHTML = '';
          }

          peeqrPlayers = document.querySelectorAll('.peeqr-player-prepare')
          peeqrVideos = document.querySelectorAll('.peeqr-video')
          peeqrAudios = document.querySelectorAll('.peeqr-audio');
  
          peeqrPlayersLength = peeqrPlayers.length
  
          this.runResize(peeqrPlayers, peeqrVideos, wrapper, peeqrPlayersLength);
        });

        this.handleSocketActions('play');

        this.createControls();
      });
      
      client.on('mute-video', () => {
        if (peeqrPlayersLength === 1) {
          this.handleSocketActions('paused')
        }
      });
      client.on('unmute-video', () => {
        this.handleSocketActions('play')
      });
      client.on('peer-leave', (evt) => {
        stream = evt.stream;
        if (document.querySelector(`#player_${+stream.getId()}`)) {
          document.querySelector(`#player_${+stream.getId()}`).outerHTML = '';
        }
        this.handleSocketActions('stopped');
      });
      client.on('liveStreamingStarted', () => {
        this.handleSocketActions('play');
      });
      client.on('liveStreamingStopped', () => {
        this.handleSocketActions('stopped');
      });
    } else {
      setTimeout(() => this.startAgora(), 1000)
    }
  }

  createControls() {
    const { playerIsReady, currentVolume, video } = this.state;

    const changeButtonType = (btn, value) => {
      btn.title = value;
      btn.className = value;
    }
    const playPauseVideo = () => {
      if (btnPlayPause.classList.contains('pause')) {
        for (let i = 0; i < peeqrPlayersLength; ++i) {
          peeqrVideos[i].pause();
          peeqrAudios[i].pause();
        }
      } else {
        for (let i = 0; i < peeqrPlayersLength; ++i) {
          peeqrVideos[i].play();
          peeqrAudios[i].play();
        }
      }
    }
    const muteVolume = () => {
      for (let i = 0; i < peeqrPlayersLength; ++i) {
        if (!peeqrAudios[i].muted) {
          changeButtonType(btnMute, 'mute');
          peeqrAudios[i].muted = true;
          volumeBar.value = 0;
        } else {
          changeButtonType(btnMute, 'unmute');
          peeqrAudios[i].muted = false;
          volumeBar.value = currentVolume;
        }
      }
    }

    const playbackEl = document.querySelector('.playback');
    function toggleFullScreen() {

      document.addEventListener('fullscreenchange', exitHandler);
      document.addEventListener('webkitfullscreenchange', exitHandler);
      document.addEventListener('mozfullscreenchange', exitHandler);
      document.addEventListener('MSFullscreenChange', exitHandler);

      function exitHandler() {
        if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].classList.remove('fullscreen')
            playbackEl.classList.add('pad')
          }
        }
      }
      if (playbackEl.requestFullscreen) {
        if (document.fullScreenElement) {
          exitHandler();
          document.cancelFullScreen();
          document.exitFullScreen();
        } else {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].classList.add('fullscreen')
          }
          playbackEl.classList.remove('pad')
          playbackEl.requestFullscreen();
        }
      } else if (playbackEl.msRequestFullscreen) {
        if (document.msFullscreenElement) {
          exitHandler();
          document.msExitFullscreen();
        } else {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].classList.add('fullscreen')
          }
          playbackEl.classList.remove('pad')
          playbackEl.msRequestFullscreen();
        }
      } else if (playbackEl.mozRequestFullScreen) {
        if (document.mozFullScreenElement) {
          exitHandler();
          document.mozCancelFullScreen();
          document.mozExitFullScreen();
        } else {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].classList.add('fullscreen')
          }
          playbackEl.classList.remove('pad')
          playbackEl.mozRequestFullScreen();
        }
      } else if (playbackEl.webkitRequestFullscreen) {
        if (document.webkitFullscreenElement) {
          exitHandler();
          document.webkitCancelFullScreen();
        } else {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].classList.add('fullscreen')
          }
          playbackEl.classList.remove('pad')
          playbackEl.webkitRequestFullscreen();
        }
      } else {
        alert("Fullscreen API is not supported");
      }
    }

    const controls = document.getElementById('livecontrols');
    if (playerIsReady) {
      if (!controls.hasChildNodes()) {
        let btnPlayPause = document.createElement('button');
        btnPlayPause.setAttribute("id", 'btnPlayPause');
        let btnFullScreen = document.createElement('button');
        btnFullScreen.setAttribute("id", "btnFullScreen");
        let volumeBar = document.createElement('input');
        volumeBar.setAttribute("id", "volumeBar");
        volumeBar.setAttribute("type", "range");
        volumeBar.setAttribute("min", "0");
        volumeBar.setAttribute("max", "1");
        volumeBar.setAttribute("step", "0.1");
        volumeBar.setAttribute("value", "1");
        let btnMute = document.createElement('button');
        btnMute.setAttribute("id", "btnMute");
        let livePulse = document.createElement('div');
        livePulse.className = 'live';
        livePulse.innerHTML = 'LIVE';
        controls.appendChild(btnPlayPause);
        controls.appendChild(btnMute);
        controls.appendChild(volumeBar);
        controls.appendChild(livePulse);
        controls.appendChild(btnFullScreen);
        wrapper.addEventListener('click', () => {
          controls.classList.add('active')
          playPauseVideo();
        });
        wrapper.addEventListener('dblclick', () => {
          toggleFullScreen();
        });
        wrapper.addEventListener('mouseover', () => {
          controls.classList.add('active')
        });
        wrapper.addEventListener('mouseout', () => {
          setTimeout(() => controls.classList.remove('active'), 2500)
        });
        controls.addEventListener('mouseover', () => {
          controls.classList.add('active')
        });
        controls.addEventListener('mouseout', () => {
          setTimeout(() => controls.classList.remove('active'), 2500)
        });

        for (let i = 0; i < peeqrPlayersLength; ++i) {
          peeqrVideos[i].addEventListener('play', () => {
            changeButtonType(btnPlayPause, 'pause');
            this.handleSocketActions('play')
          }, false);
        }

        for (let i = 0; i < peeqrPlayersLength; ++i) {
          peeqrVideos[i].addEventListener('pause', () => {
            changeButtonType(btnPlayPause, 'play');
            this.handleSocketActions('user-paused')
          }, false);
        }

        btnPlayPause.addEventListener("click", () => {
          playPauseVideo();
        });

        peeqrAudios[0].addEventListener('loadeddata', () => {
          changeButtonType(btnMute, 'mute');
          this.setState({currentVolume: peeqrAudios[0].volume});
        }, false);
        btnMute.addEventListener("click", () => {
          muteVolume();
        });
        btnMute.addEventListener("mouseover", () => {
          volumeBar.classList.add('active')
          livePulse.classList.add('active')
          controls.classList.add('active')
        });
        btnMute.addEventListener("mouseout", () => {
          volumeBar.classList.remove('active')
          livePulse.classList.remove('active')
          controls.classList.remove('active')
        });
        volumeBar.addEventListener("change", (evt) => {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrAudios[i].volume = evt.target.value;
          }
        });
        volumeBar.addEventListener("mouseover", () => {
          volumeBar.classList.add('active')
          livePulse.classList.add('active')
          controls.classList.add('active')
        });
        volumeBar.addEventListener("mouseout", () => {
          volumeBar.classList.remove('active')
          livePulse.classList.remove('active')
          controls.classList.remove('active')
        });
        peeqrAudios[0].addEventListener('volumechange', () => {
          if (peeqrAudios[0].volume === 0 || peeqrAudios[0].muted) {
            changeButtonType(btnMute, 'unmute');
          } else if (peeqrAudios[0].volume > 0.2 && peeqrAudios[0].volume <= 0.6){
            changeButtonType(btnMute, 'mid');
            this.setState({currentVolume: peeqrAudios[0].volume})
          } else if (peeqrAudios[0].volume <= 0.2){
            changeButtonType(btnMute, 'low');
            this.setState({currentVolume: peeqrAudios[0].volume})
          } else {
            changeButtonType(btnMute, 'mute');
            this.setState({currentVolume: peeqrAudios[0].volume})
          }
        }, false);
        btnFullScreen.addEventListener("click", () => {
          toggleFullScreen();
        });
      }
    } else {
      setTimeout(() => this.createControls(), 500)
    }
  }

  joinChannel(channelKey, channel, userID) {
    const { joining } = this.state;
    if (channelKey && !joining) {
      this.setState({ joining: true })
      client.join(channelKey, channel, userID, () => {
        console.log(userID + 'Joined Video Channel.')
        this.setState({ joining: false })
      }, function(err) {
        createNotification("error", `${JOIN_VIDEO_FAILED} (${err})`);
      });
    } else {
      setTimeout(() => this.joinChannel(), 1000)
    }
  }

  handleSocketActions(type) {
    const { video } = this.props;
    switch (type.toUpperCase()) {
      case 'PLAY':
      if (video.get('stream_platform') === 1) {
        for (let i = 0; i < peeqrPlayersLength; ++i) {
          peeqrVideos[i].play();
          peeqrAudios[i].play();
        }
      }
      this.setState({ isSystemPaused: false, isSystemEnded: false, isUserPaused: false });
      break;

      case 'USER-PAUSED':
        if (video.get('stream_platform') === 1) {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].pause();
            peeqrAudios[i].pause();
          }
        }
        this.setState({ isUserPaused: true });
        break;

      case 'PAUSED':
        if (video.get('stream_platform') === 1) {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].pause();
            peeqrAudios[i].pause();
          }
        }
        this.setState({ isSystemPaused: true });
        break;

      case 'STOPPED':
        if (video.get('stream_platform') === 1) {
          for (let i = 0; i < peeqrPlayersLength; ++i) {
            peeqrVideos[i].pause();
            peeqrAudios[i].pause();
          }
        }
        this.setState({ isSystemEnded: true });
        break;

      default:
        break;
    }
  }

  render() {
    const { video, children, isConverting, socket, videoUrl } = this.props;
    const { isSystemEnded, isSystemPaused, isUserPaused, videoWidth, videoHeight, playerState } = this.state;

    return (
      <div className="video-player">
        <div className="playback pad">
          <VideoPlayerOverlays 
            video={video} 
            isSystemEnded={isSystemEnded} 
            isSystemPaused={isSystemPaused} 
            isUserPaused={isUserPaused}
            isConverting={isConverting}
          />
          <Annotations 
            video={video}
            socket={socket}
            videoWidth={videoWidth}
            videoHeight={videoHeight}
            playerState={playerState}
          />
          <div className="peeqr-wrapper">
            {
              (video.get('live') && video.get('stream_platform') === 0) ? (
                <Player ref={this.player} id={`peeqr${video.get('id')}`} autoPlay fluid controls poster={video.get('thumbnail')}>
                  <HlsProvider
                    isVideoChild
                    src={ videoUrl() }
                  />
                </Player>
              ) : (video.get('merge_status') === 2) ? (
                <Player ref={this.player} id={`peeqr${video.get('id')}`} autoPlay fluid controls poster={video.get('thumbnail')}>
                  <source src={ videoUrl() } />
                </Player>
              ) : null
            }
          </div>
          {
            (video.get('live') && video.get('stream_platform') === 1) ? (
              <div id="livecontrols" />
            ) : null
          }
        </div>
        {children}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  me: state.getIn(['user', 'me']),
  channelKey: state.getIn(['video', 'channelKey']),
  AgoraRTC: state.getIn(['video', 'AgoraRTC'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAgoraKeyRequest,
      startViewRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(VideoPlayer)
