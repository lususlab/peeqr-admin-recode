import React, { Component } from 'react';
import Hls from 'hls.js';

export default class HlsProvider extends Component {
  constructor() {
    super();
    this.hls = new Hls();
  }

  componentDidMount() {
    const { src, video } = this.props;
    if (Hls.isSupported()) {
      this.hls.loadSource(src);
      this.hls.attachMedia(video);
      this.hls.on(Hls.Events.MANIFEST_PARSED, () => {
        video.play();
      });
    } else if (video.canPlayType('application/vnd.apple.mpegurl')) {
      video.src = video.get('play_url');
      video.addEventListener('loadedmetadata',function() {
        video.play();
      });
    }
  }

  componentWillUnmount() {
    if (this.hls) {
      this.hls.destroy();
    }
  }

  reloadSrc() {
    const { src, video } = this.props;
    if (Hls.isSupported()) {
      this.hls.loadSource(src);
      this.hls.attachMedia(video);
      this.hls.on(Hls.Events.MANIFEST_PARSED, () => {
        video.play();
      });
    }
  }

  render() {
    return (
      <source
        src={ this.props.src }
        type={ this.props.type || 'application/x-mpegURL' }
      />
    );
  }

}
