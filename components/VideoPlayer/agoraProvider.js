import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import AgoraRTC from 'agora-rtc-sdk';
import { detect } from 'detect-browser';

import Spinner from "components/Spinner"

import {
  saveAgoraChannelKey
} from 'store/video/actions'

import { createNotification } from 'helpers/createnotification';

const AGORA_LOG_LEVEL = process.env.AGORA_LOG_LEVEL;

class AgoraProvider extends Component {
  constructor() {
    super();

    this.state = {
      safari: false,
      commencing: false
    }

    this.checkAgora = this.checkAgora.bind(this);
    this.commenceAgora = this.commenceAgora.bind(this)
  }

  componentDidMount () {
    const { video, agoraKey } = this.props;

    if (agoraKey.size > 0 && video.get('live') && video.get('stream_platform') === 1) {
      this.checkAgora()
    }
  }

  componentDidUpdate(prevProps) {
    const {agoraKey, video, channelKey} = this.props;
    if (
      (agoraKey.size > 0 && prevProps.agoraKey !== agoraKey) && 
      (video.get('id') !== prevProps.video.get('id')) && 
      (video.get('stream_platform') === 1)
    ) {
      this.checkAgora()
    }
    if (channelKey && prevProps.channelKey !== channelKey) {
      this.checkAgora()
    }
  }

  checkAgora() {
    const { commencing } = this.state;
    const browser = detect(); 
       
    if (!commencing) {
      if (AGORA_LOG_LEVEL === 'ERROR') {
        AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.ERROR);
      } else {
        AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.DEBUG);
      }

      if (!AgoraRTC.checkSystemRequirements()) {
        createNotification("error", BROWSER_NO_SUPPORT);
      } else {
        this.commenceAgora();
      }

      if (browser.name === 'safari') {
        this.setState({ safari: true });
      }
    }
  }

  commenceAgora() {
    const { video, agoraKey, saveAgoraChannelKey } = this.props;
    this.setState({ commencing: true })
    
    if (video.get('device_os') !== 'OBS' && video.get('live')) {
      if (agoraKey.getIn(['data', 'Channel key'])) {
        saveAgoraChannelKey(agoraKey.getIn(['data', 'Channel key']), AgoraRTC)
        this.setState({ commencing: false })
      }
    }
  }

  render () {
    const { video, channelKey, children } = this.props;
    return (
      video.get('live') && video.get('stream_platform') === 1 ? (
        channelKey && AgoraRTC ? (
          children
        ) : <Spinner type="relative" color="grey" />
      ) : children
    )
  }
}
const mapStateToProps = state => ({
  agoraKey: state.getIn(['video', 'agoraKey']),
  channelKey: state.getIn(['video', 'channelKey'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      saveAgoraChannelKey
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AgoraProvider)
