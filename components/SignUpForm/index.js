import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import ReCAPTCHA from "react-google-recaptcha";
import { Link } from "routes";
import md5 from 'md5';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import TwitterLogin from 'react-twitter-auth/lib/react-twitter-auth-component';

import { Router } from 'routes';

import Layout from 'components/Layout';
import Input from 'components/Input';
import Button from 'components/Button';
import Spinner from 'components/Spinner';

import { 
  signUpRequest
} from 'store/user/actions';

import { 
  SIGN_UP, 
  NOT_A_USER,
  SIGN_UP_NOW,
  FORGOT_PASSWORD,
  LABEL_EMAIL, 
  LABEL_PASSWORD,
  LABEL_USERNAME,
  LABEL_CONFIRM_PASSWORD,
  ACCEPT,
  TERMS,
  I_HAVE_AN_ACCOUNT
} from 'helpers/text';

import { validCheck } from 'helpers/formhelper';
import { createNotification } from 'helpers/createnotification'

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
      errors: props.errorFromState,
      acceptterms: false,
      recaptcha: false,
      validFields: [],
      allValidCheck: false,
    }
    this.handleSignUp = this.handleSignUp.bind(this);
    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onConfirmPasswordChange = this.onConfirmPasswordChange.bind(this);
    this.onAcceptTermsChange = this.onAcceptTermsChange.bind(this);
    this.onRecaptcha = this.onRecaptcha.bind(this);
    this.handleErrors = this.handleErrors.bind(this);
    this.validCheck = validCheck.bind(this);
  }
  componentDidUpdate(prevProps) {
    const { errorFromState } = this.props;
    if (errorFromState !== prevProps.errorFromState && errorFromState) {
      this.setState({ errors: errorFromState })
    }
  }
  onUsernameChange(e) {
    this.setState({ username: e.target.value, errors: '' });
  }
  onEmailChange(e) {
    this.setState({ email: e.target.value, errors: '' });
  }
  onPasswordChange(e) {
    this.setState({ password: e.target.value, errors: '' });
  }
  onConfirmPasswordChange(e) {
    this.setState({ confirmPassword: e.target.value, errors: '' });
  }
  onRecaptcha(e) {
    this.setState({ recaptcha: true });
  }
  onAcceptTermsChange(e) {
    this.setState({ acceptterms: e.target.checked });
  }
  handleSignUp() {
    const { signUpRequest } = this.props;
    const { username, email, password, confirmPassword } = this.state;
    signUpRequest(username, email, md5(password), md5(confirmPassword));
  }
  handleErrors(errors) {
    errors.forEach((error) => {
      createNotification('error', error.toJS().toString())
    })
  }

  render() {
    const { signingUp } = this.props;
    const { username, email, password, confirmPassword, errors, acceptterms, recaptcha, allValidCheck } = this.state;
    const fields = [ username, email, password, confirmPassword ];
    const formIsNotEmpty = fields.every(field => field.length > 0);
    console.log(errors)
    return (
      <form id="signup-form">
        <Input 
          type="text"
          theme="boxed"
          name={LABEL_USERNAME}
          placeholder={LABEL_USERNAME}
          value={username}
          onChange={this.onUsernameChange}
          required
          validator="username"
          isValid={this.validCheck}
        />
        <Input 
          type="text"
          theme="boxed"
          name={LABEL_EMAIL}
          placeholder={LABEL_EMAIL}
          value={email}
          onChange={this.onEmailChange}
          required
          validator="email"
          isValid={this.validCheck}
        />
        <Input 
          type="password"
          theme="boxed"
          name={LABEL_PASSWORD}
          placeholder={LABEL_PASSWORD}
          value={password}
          showPassword
          onChange={this.onPasswordChange}
          required
          validator="password"
          isValid={this.validCheck}
        />
        <Input 
          type="password"
          theme="boxed"
          name={LABEL_CONFIRM_PASSWORD}
          placeholder={LABEL_CONFIRM_PASSWORD}
          value={confirmPassword}
          showPassword
          onChange={this.onConfirmPasswordChange}
          required
          validator="confirmpassword"
          customValidator={password}
          isValid={this.validCheck}
        />
        <div id="recaptcha">
          <ReCAPTCHA
            ref="recaptcha"
            sitekey={process.env.RECAPTCHA}
            onChange={this.onRecaptcha}
          />
        </div>
        <Input 
          type="checkbox"
          theme="checkbox"
          label={ACCEPT}
          name={ACCEPT}
          linkLabel={TERMS}
          route="/terms"
          defaultChecked={acceptterms}
          onChange={this.onAcceptTermsChange}
          required
        />
        {
          errors ? (
            this.handleErrors(errors)
          ) : null
        }
        {
          (signingUp || !allValidCheck || !formIsNotEmpty || !acceptterms || !recaptcha) ? (
            <Button color="white" background="bggrey">
              <a className="disabled">
                {signingUp ? (
                  <Spinner color="white" />
                ) : null}<span>{ SIGN_UP }</span>
              </a>
            </Button>
          ) : (
            <Button color="white" background="bglightpurple">
              <a onClick={this.handleSignUp}><span>{ SIGN_UP }</span></a>
            </Button>
          )
        }
        <p>
          <Link route="signin">
            <a>{ I_HAVE_AN_ACCOUNT }</a>
          </Link>
        </p>
      </form>
    )
  }
}
const mapStateToProps = state => ({
  signingUp: state.getIn(['user', 'signingUp']),
  errorFromState: state.getIn(['user', 'error']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    signUpRequest
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm)

