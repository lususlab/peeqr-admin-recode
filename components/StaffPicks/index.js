import React from "react";

import SlidingBoxes from 'components/SlidingBoxes';
import VideoItem from 'components/VideoItem';

const StaffPicks = ({staffPicks}) => (
  <section id="staff-picks">
    <SlidingBoxes>
      {
        staffPicks.size > 0 ? (
          staffPicks.get('results').map(staffPick => (
          <VideoItem 
            key={staffPick.get('id')}
            className="staff-pick"
            item={staffPick}
          />
        ))
      ) : null}
    </SlidingBoxes>
  </section>
)

export default StaffPicks
