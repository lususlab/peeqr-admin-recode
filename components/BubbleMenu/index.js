
import React from 'react';

const BubbleMenu = (props) => (
  <div className={`bubble fade ${props.classes ? props.classes : ''}`}>
    <div className="bubble-content">
      {props.children}
    </div>
  </div>
)

export default BubbleMenu