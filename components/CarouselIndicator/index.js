import React, { Component, PropTypes } from 'react';
import styled from 'styled-components'

class CarouselIndicator extends Component {
  constructor() {
    super();
    this.handleChangeSlide = this.handleChangeSlide.bind(this);
  }

  handleChangeSlide(i) {
    const { goTo } = this.props;
    goTo(i);
  }
  render() {
    const { length, position } = this.props;
  return (
    <div className="bullets">
      {
        Array.from({ length }, (pip, i) => 
          (<span
            className={`pip ${i === position  ? 'current' : '' }`}
            key={i}
            onClick={() => this.handleChangeSlide(i)}
          />)
        )
      }
    </div>
    )
  }
}

export default CarouselIndicator;