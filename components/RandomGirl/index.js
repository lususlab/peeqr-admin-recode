import React, { Component } from 'react';
import pchan1 from 'static/img/girl-spinner/spinner-1.png';
import pchan2 from 'static/img/girl-spinner/spinner-2.png';
import pchan3 from 'static/img/girl-spinner/spinner-3.png';
import pchan4 from 'static/img/girl-spinner/spinner-4.png';
import pchan5 from 'static/img/girl-spinner/spinner-5.png';
import pchan6 from 'static/img/girl-spinner/spinner-6.png';
import pchan7 from 'static/img/girl-spinner/spinner-7.png';

export default class RandomGirl extends Component {
  constructor() {
    super();
    this.state = {
      selectedPChan: '',
      pchans: [
        pchan1,
        pchan2,
        pchan3,
        pchan4,
        pchan5,
        pchan6,
        pchan7,
      ]
    }
  }
  componentDidMount() {
    this.selectPChan();
  }
  selectPChan() {
    const { pchans } = this.state;
    this.setState({selectedPChan: pchans[Math.floor(Math.random() * pchans.length)]});
  }
  render() {
    const {centered, alt, title} = this.props;
    const {selectedPChan} = this.state;
    return <img className={centered ? 'centered' : ''} alt={alt} title={title} src={selectedPChan} />
  }
}