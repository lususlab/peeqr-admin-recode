import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV, faUserAltSlash, faVolumeMute, faCheck } from '@fortawesome/free-solid-svg-icons';
import Transition from 'react-transition-group/Transition';
import MediaQuery from 'react-responsive';

import BubbleMenu from 'components/BubbleMenu';
import Modal from 'components/Modal';
import Spinner from 'components/Spinner';

import { 
  blockRequest,
  unblockRequest,
  muteRequest,
  unmuteRequest
} from 'store/user/actions';

import { closeonclick } from "helpers/closeonclick";

import {
  BLOCK,
  UNBLOCK,
  MUTE,
  UNMUTE,
} from "helpers/text"

class CommentsMuteBlockButton extends Component {
  constructor(props) {
    super(props)

    this.state = {
      moreActive: false,
      isSelf: props.me.get('id') === props.userId,
      blockedUsers: props.blockedList.size > 0 ? props.blockedList.map(user => user.get('username')) : [],
      mutedUsers: props.mutedList.size > 0 ? props.mutedList.map(user => user.get('username')) : [],
    }

    this.handleActive = this.handleActive.bind(this);
    this.handleInactive = this.handleInactive.bind(this);
    this.handleBlock = this.handleBlock.bind(this);
    this.handleMute = this.handleMute.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    const { blockedList, mutedList } = this.props;
    const { moreActive, blockedUsers, mutedUsers } = this.state;
    if (moreActive !== prevState.moreActive && moreActive) {
      closeonclick(this.handleInactive);
    }

    if (blockedList !== prevProps.blockedList && blockedList) {
      this.setState({ blockedUsers: blockedList.size > 0 ? blockedList.map(user => user.get('username')) : [] })
    }
    if (mutedList !== prevProps.mutedList && mutedList) {
      this.setState({ mutedUsers: mutedList.size > 0 ? mutedList.map(user => user.get('username')) : [] })
    }
  }
  handleActive() {
    this.setState({ moreActive: true })
  }
  handleInactive() {
    this.setState({ moreActive: false })
  }
  handleMute() {
    const {unmuteRequest, muteRequest, userId, videoId, username, jwt, me} = this.props;
    const {mutedUsers} = this.state;
    if (me.size > 0) {
      if (mutedUsers.indexOf(username) > -1) {
        unmuteRequest(videoId, userId, username, jwt) 
      } else {
        muteRequest(videoId, userId, username, jwt)
      }
    }
  }
  handleBlock() {
    const {unblockRequest, blockRequest, userId, username, jwt, me} = this.props;
    const {blockedUsers} = this.state;
    if (me.size > 0) {
      if (blockedUsers.indexOf(username) > -1) {
        unblockRequest(userId, username, jwt) 
      } else {
        blockRequest(userId, username, jwt)
      }
    }
  }
  renderMoreButton() {
    const {blocking, muting, username, videoId, nameDisplay, jwt} = this.props;
    const {blockedUsers, mutedUsers} = this.state;
    return (
      <div>
        {
          blocking || muting ? (
            <div>
              <Spinner type="relative" color="purple" />
            </div>
          ) : (
            <div>
              <div className="mute">
                <a onClick={this.handleMute}>
                  <div className={`circle ${mutedUsers.indexOf(username) > -1 ? 'grey' : ''}`}><FontAwesomeIcon icon={mutedUsers.indexOf(username) > -1 ? faCheck : faVolumeMute} /></div>
                  <span>{mutedUsers.indexOf(username) > -1 ? UNMUTE : MUTE} {nameDisplay}</span>
                </a>
              </div>
              <div className="block">
                <a onClick={this.handleBlock}>
                  <div className={`circle ${blockedUsers.indexOf(username) > -1 ? 'red' : ''}`}><FontAwesomeIcon icon={blockedUsers.indexOf(username) > -1 ? faCheck : faUserAltSlash} /></div>
                  <span>{blockedUsers.indexOf(username) > -1 ? UNBLOCK : BLOCK} {nameDisplay}</span>
                </a>
              </div>
            </div>
          )
        }
      </div>
    )
  }
  render() {
    const {position} = this.props;
    const {moreActive, isSelf} = this.state;
    return (
      !isSelf ? (
        <div className="comment-more">
          <a onClick={this.handleActive}>
            <FontAwesomeIcon icon={faEllipsisV} />
          </a>
          <MediaQuery minWidth={768}>
            <Transition
              timeout={150}
              in={moreActive}
              mountOnEnter
              unmountOnExit
            >
              {status => (
                <BubbleMenu classes={`fade-${status} ${position}`} >
                  {this.renderMoreButton()}
                </BubbleMenu>
              )}
            </Transition>
          </MediaQuery>
          <MediaQuery maxWidth={767}>
            <Transition
              timeout={150}
              in={moreActive}
              mountOnEnter
              unmountOnExit
            >
              {status => (
                <Modal classes={`fade-${status}`} close={() => this.handleInactive}>
                  {this.renderMoreButton()}
                </Modal>
              )}
            </Transition>
          </MediaQuery>
        </div>
      ) : null
    )
  }
}
const mapStateToProps = state => ({
  blocking: state.getIn(['user', 'blocking']),
  blockedList: state.getIn(['user', 'blockedList']),
  muting: state.getIn(['user', 'muting']),
  mutedList: state.getIn(['user', 'mutedList']),
  me: state.getIn(['user', 'me']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      blockRequest,
      unblockRequest,
      muteRequest,
      unmuteRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CommentsMuteBlockButton)
