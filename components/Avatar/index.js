import React, {Component} from 'react';
import RandomGirl from 'components/RandomGirl'

class Avatar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: false,
      image: props.photo,
    }
    this.handleError = this.handleError.bind(this)
  }

  handleError = () => {
    this.setState({ error: true })
  }

  render() {
    const { size, username } = this.props;
    const { error, image } = this.state;
    const style = !error ? {} : {visibility: 'hidden'}

    return (
      <div className={`avatar ${size} ${error ? 'error' : ''}`}>
        <img 
          style={style}
          alt={username} 
          src={image}
          onError={this.handleError} 
          title={username}
        />
        {
          error ? (
            <RandomGirl title={username} alt={username} />
          ) : null
        }
      </div>
    )
  }
}

export default Avatar