import React, { Component, Children, cloneElement } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { throttle } from 'lodash';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';

import CarouselIndicator from "components/CarouselIndicator";
import Spinner from "components/Spinner";

const LoadableSwipeable = dynamic(
  import("react-swipeable").then(m => {
    const Swipeable = m;
    Swipeable.__webpackChunkName = m.__webpackChunkName;
    return Swipeable;
  }),
  {
    loading: () => <Spinner type="large" color="white" />,
    ssr: false,
  }
)

const CarouselContainer = styled.div`
  transition: ${(props) => props.sliding ? 'none' : 'transform 0.2s ease'};
  transform: ${(props) => {
    const amount = "100%";
    if (props.numslides === 1) return 'translateX(0%)'
    if (props.numslides === 2) {
      if (!props.sliding && props.direction === 'next') return `translateX(-${amount})`
      if (!props.sliding && props.direction === 'prev') return 'translateX(0%)'
      if (props.direction === 'prev') return `translateX(-${amount})`
      return 'translateX(0%)'
    }
    if (!props.sliding) return `translateX(-${amount})`
    if (props.direction === 'prev') return `translateX(calc(2 * (-${amount})))`
    return 'translateX(0%)'
  }};
`
const Slot = styled.div`
  order: ${(props) => props.order};
`

const Navigate = styled.button`
opacity: ${(props) => {
  if (props.type === 'prev') {
    if (props.position === 0) { 
      return '0.2'
    }
  } else {
    if (props.position === props.numItems - 1) { 
      return '0.2'
    }
  }
}};
`

class Carousel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      position: 0,
      direction: props.children.size === 2 ? 'prev' : 'next',
      sliding: false,
    }

    this.handleSwipe = this.handleSwipe.bind(this);
    this.nextSlide = this.nextSlide.bind(this);
    this.prevSlide = this.prevSlide.bind(this);
  }
  componentDidMount() {
    this.prevSlide()
  }
  getOrder(itemIndex) {
    const { position } = this.state;
    const { children } = this.props;
    const numItems = children.size
    
    if (numItems === 2) return itemIndex

    if (itemIndex - position < 0) {
      return numItems - Math.abs(itemIndex - position)
    }
    return itemIndex - position
  }

  handleSwipe = throttle((isNext) => {
    const { children } = this.props;
    const numItems = children.size || 1;
    console.log('swiping');

    if (isNext && numItems > 1) {
      this.nextSlide()
    } else if (numItems > 1) {
      this.prevSlide()
    }
  }, 500, { trailing: false })

  goTo(i) {
    const { position } = this.state;
    if (i > position) {
      this.doSliding('next', i)
    } else if (i < position) {
      this.doSliding('prev', i)
    }
    this.setState({position: i});
  }

  doSliding = (direction, position) => {
    this.setState({
      sliding: true,
      direction,
      position
    })
    setTimeout(() => {
     this.setState({
        sliding: false
      })
    }, 50)
  }

  nextSlide = () => {
    const { position } = this.state;
    const { children } = this.props;
    const numItems = children.size
    if (numItems === 2 && position === 1) return

    this.doSliding('next', position === numItems - 1 ? 0 : position + 1)
  }

  prevSlide = () => {
    const { position } = this.state;
    const { children } = this.props;
    const numItems = children.size;
    if (numItems === 2 && position === 0) return

    this.doSliding('prev', position === 0 ? numItems - 1 : position - 1)
  }

  render() {
    const { children, bullets } = this.props;
    const { sliding, direction, position } = this.state;
    const numItems = children.size;
    const childrenWithProps = Children.map(children,
      (child) => cloneElement(child, {
        numslides: children.size || 1
      })
    )
    return (
      <div className="carousel-wrapper">
        {
          (numItems > 1) ? (
            <Navigate type="prev" numItems={numItems} position={position} className="navigate prev" onClick={this.prevSlide}><FontAwesomeIcon icon={faChevronLeft} size="lg" /></Navigate>
          ) : null
        }
        <LoadableSwipeable
          preventDefaultTouchmoveEvent
          onSwipingLeft={() => this.handleSwipe(true)}
          onSwipingRight={() => this.handleSwipe()}
          className="swipeable"
        >
          {/* {TODO: Get touchable events to work with Swipeable} */}

          <CarouselContainer
            className="carousel"
            sliding={sliding}
            direction={direction}
            numslides={childrenWithProps.length}
          >
            { 
              childrenWithProps.map((child, index) => (
                <Slot 
                  className="slot"
                  key={index}
                  order={this.getOrder(index)}
                  position={position}
                  numslides={childrenWithProps.length}
                >
                  {child}
                </Slot>
              )) 
            }
          </CarouselContainer>
        </LoadableSwipeable>
        {
          (numItems > 1) ? (
            <Navigate type="next" numItems={numItems} position={position} className="navigate next" onClick={this.nextSlide}><FontAwesomeIcon icon={faChevronRight} size="lg" /></Navigate>
          ) : null
        }
        {
          (bullets) ? (
            <CarouselIndicator
              length={childrenWithProps.length}
              position={position}
              goTo={(i) => this.goTo(i)}
            />
          ) : null
        }
      </div>
    )
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Carousel)
