import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { Link } from 'routes';

import Avatar from "components/Avatar";

import { videoCurrentTime } from 'helpers/videohelper';

import {
  VIEWING_NOW,
  MORE
} from 'helpers/text';

import {lowerCase} from 'helpers/lowercase'

class ViewerList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      viewers: props.viewers,
    }

    this.renderViewer = this.renderViewer.bind(this);
  }

  componentDidMount() {
    const { socket } = this.props;
    
    socket.on('comment', data => {
      this.handleSocketView(data);
    });
  }

  handleSocketView(data) {
    const { viewers } = this.state;
    const updatedViewers = viewers.toJS();
    if (data.cmd === "view") {
      updatedViewers.unshift({
        id: data.user_id,
        username: data.user,
        name_display: data.name_display,
        photo: data.photo,
      });
      this.setState({ viewers: viewers.merge(fromJS(updatedViewers)) });
    } else if (data.cmd === "exit_view") {
      this.setState({ viewers: fromJS(updatedViewers.filter(o => o.id !== data.user_id)) });
    }
  }

  renderViewer = viewer => (
    <div className="viewer" key={viewer.get('id')}>
      <Link route="profile" params={{slug: viewer.get('username')}}>
        <a target="_blank" title={viewer.get('name_display')}>
          <Avatar size="medium" username={`${viewer.get('name_display')}${VIEWING_NOW}`} photo={viewer.get('photo')} />
        </a>
      </Link>
    </div>
  )

  getUniqueViewers = (array, propertyName) => {
    return array.filter((e, i) => array.findIndex(a => a.get(propertyName) === e.get(propertyName)) === i);
  }

  render() {
    const { viewerCount, viewerSize } = this.props;
    const { viewers } = this.state;

    return (
      <div className={`viewer-list ${viewerCount > 0 ? 'active' : ''}`}>
        {
          viewerCount > 0 ? (
            this.getUniqueViewers(viewers, 'id').map(viewer => this.renderViewer(viewer))
          ) : null
        }
        {
          viewerCount > viewerSize ? (
            <div className="gradient-block">{viewerSize - viewerCount} {lowerCase(MORE)}</div>
          ) : null
        }
      </div>
    )
  }
}

export default ViewerList
