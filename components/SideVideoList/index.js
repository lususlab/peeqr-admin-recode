import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Truncate from 'react-truncate';
import TimeAgo from 'react-timeago';
import { Link } from "routes";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Avatar from 'components/Avatar';
import MergeStatus from 'components/MergeStatus';
import CheckLink from 'components/CheckLink';
import LoadMore from 'components/LoadMore';

import { LOAD_MORE, CURRENCY } from "helpers/text"

class SideVideoList extends Component {
  constructor() {
    super()

    this.handleLoadMore = this.handleLoadMore.bind(this)
  }

  handleLoadMore = (e) => {
    e.preventDefault();
    const { next, loadMore } = this.props;

    loadMore(next)
  }
  renderVideoList = (item, i) => (
    <div key={`${item.get('id')}-${i}`} className="side">
      <div className="user">
        {
          item.get('show_id') ? (
            <span className={`tier_avatar circle ${item.getIn(['show_description', 'purchase_status']) ? 'green' : 'red' }`}> {CURRENCY} </span>
          ) : null
        }
        <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
          <a target="_blank" title={item.getIn(['user', 'name_display'])}>
            <Avatar size="medium" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} />
          </a>
        </Link>
      </div>
      <div className="description">
        <MergeStatus item={item} /><span className="ago"><TimeAgo date={item.get('upload_date')} /></span>
        <CheckLink route="video" params={{id: item.get('id')}} item={item}>
          <p>
            <Truncate lines={1} ellipsis={<span>...</span>}>{item.get('title')}</Truncate>
          </p>
        </CheckLink>
      </div>
    </div>
  )
  render() {
    const { videoList, gettingVideos, loadingMore, heading, icon, iconColor, next } = this.props;
    return (
      <div className="side-list">
        <h4 className={iconColor}>{
          icon ? (
            <FontAwesomeIcon icon={icon} size="lg" />
          ) : null
        }<span>{heading}</span></h4>
        {
          videoList.size > 0 ? (
            videoList.valueSeq().map((item, i) => this.renderVideoList(item, i))
          ) : null
        }
        {
          (next && !gettingVideos) ? (
            <div className="centered">
              <LoadMore size="small" color="white" background="bggrey" onClick={this.handleLoadMore} loading={loadingMore} />
            </div>
          ) : null
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loadingMore: state.getIn(['video', 'loadingMore'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(SideVideoList)
