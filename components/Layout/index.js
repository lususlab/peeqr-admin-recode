import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import { Link } from 'routes';
import Head from "next/head";
import Transition from 'react-transition-group/Transition';
import { throttle } from 'lodash';
import { ToastContainer } from 'react-toastify';
import WithCustomCookies from 'hoc/WithCustomCookies'
import { geolocated } from 'react-geolocated';

import Menu from 'components/Menu';
import NProgress from "components/NProgress";
import BackToTop from "components/BackToTop";
import Modal from 'components/Modal';
import Button from 'components/Button';
import Spinner from 'components/Spinner';

import {
  hideModal,
  deleteSingleVideoRequest
} from 'store/video/actions'

import {
  saveLocationRequest
} from 'store/user/actions'

import { 
  RESTRICTED,
  USE_MOBILE,
  CURRENCY,
  MAIN_META_TITLE,
  BRAND,
  SEPARATOR,
  GUIDELINES,
  NUDITY,
  ABUSIVE,
  VIOLENT,
  SPAM,
  YOU_HAVE_BLOCKED,
  BLOCK_GUIDELINE,
  CONFIRM_DELETE,
  DELETE,
  YES,
  NO,
  CANCEL
} from 'helpers/text';

import defaultImage from 'static/img/defaultShare.jpg';
import abusive from 'static/img/guidelines/abusive.png';
import nudity from 'static/img/guidelines/nudity.png';
import violent from 'static/img/guidelines/violent.png';
import spam from 'static/img/guidelines/spam.png';

class Layout extends Component{
  constructor() {
    super();

    this.handleShowModalInactive = this.handleShowModalInactive.bind(this)
    this.handleDeleteVideo = this.handleDeleteVideo.bind(this)
  }
  async componentDidMount() {
    if (document.querySelector('.main-area')) {
      document.querySelector('.main-area').style.minHeight = await this.resizeHeight();
      window.addEventListener('resize', throttle(() => { 
        document.querySelector('.main-area').style.minHeight = this.resizeHeight();
      }, 500))
    }
    if (document.querySelector('main')) {
      document.querySelector('main').style.minHeight = await this.resizeHeight();
      window.addEventListener('resize', throttle(() => { 
        document.querySelector('main').style.minHeight = this.resizeHeight();
      }, 500))
    }
  }

  componentDidUpdate(prevProps) {
    const { coords, isGeolocationEnabled, saveLocationRequest, lat, long, cookies, deleting } = this.props;

    if (isGeolocationEnabled && coords && prevProps.coords !== coords) {
      saveLocationRequest(coords)
    }

    if (lat && prevProps.lat !== lat) {
      cookies.set('lat', lat);
      cookies.set('long', long);
    }

    if (!deleting && prevProps.deleting !== deleting) {
      this.handleShowModalInactive()
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => { 
      return resizeHeight()
    });
  }

  resizeHeight() {
    const header = document.querySelector('header');
    let headerHeight, windowHeight;
    
    headerHeight = header ? header.offsetHeight : "0";
    windowHeight = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;
  
    return `${windowHeight - headerHeight}px`;
  }
  handleShowModalInactive() {
    this.props.hideModal()
  }
  handleDeleteVideo(item) {
    const {deleteSingleVideoRequest, jwt} = this.props;
    deleteSingleVideoRequest(item.get('id'), encodeURI(item.getIn(['user', 'username'])), jwt, 1, 10)
  }

  render() {
    const { 
      children, 
      showModal, 
      modalType, 
      modalDetails,
      router,
      metaTitle,
      metaType,
      metaImage,
      metaDescription,
      deleting
    } = this.props;

    return (
      <div className="main-wrapper">
        <Head>
          <title>{BRAND}{metaTitle ? SEPARATOR : ''}{metaTitle ? metaTitle : ''}</title>
          <meta property="og:url" content={`${process.env.HOSTNAME}${router.asPath}`} />
          <meta property="og:type" content={metaType || 'website'} />
          <meta property="og:title" content={`${BRAND}${metaTitle ? SEPARATOR : ''}${metaTitle ? metaTitle : ''}`} />
          <meta property="og:image" content={metaImage || defaultImage} />
          <meta property="og:description" content={metaDescription || MAIN_META_TITLE} />
        </Head>
        <NProgress />
        <ToastContainer
          newestOnTop={ true }
        />
        <Menu />
        <div className="main-area">
          {children}
        </div>
        <BackToTop />
        <Transition
          timeout={150}
          in={showModal}
          mountOnEnter
          unmountOnExit
        >
          {status => (
            <Modal classes={`fade-${status}`} close={() => this.handleShowModalInactive}>
              {
                modalType === 'access' ? (
                  <div className="show-details">
                    <h4 className="title">
                      {modalDetails.getIn(['show_description', 'show_name'])}
                      <span className="tier"><span className="tier-wrap">{modalDetails.getIn(['show_description', 'purchase_status']) ? <span className="circle green" /> : <span className="circle red" />} {CURRENCY} {modalDetails.get('ticket_tier') ? modalDetails.getIn(['ticket_tier', 'tier_name']) : ''}</span></span>
                    </h4>
                    {
                      modalDetails.getIn(['show_description', 'description']) ? (
                        <div>
                          <hr />
                          <p>{modalDetails.getIn(['show_description', 'description'])}</p>
                          <hr />
                        </div>
                      ) : null
                    }
                    <p>{RESTRICTED} <br /><br />{USE_MOBILE}</p>
                  </div>
                ) : modalType === 'guidelines' ? (
                  <div className="guidelines">
                    <h4 className="title">
                      {GUIDELINES}
                    </h4>
                    <div class="guidelines-grid">
                      <div class="guideline">
                        <img src={nudity} />
                        {NUDITY}
                      </div>
                      <div class="guideline">
                        <img src={violent} />
                        {VIOLENT}
                      </div>
                      <div class="guideline">
                        <img src={abusive} />
                        {ABUSIVE}
                      </div>
                      <div class="guideline">
                        <img src={spam} />
                        {SPAM}
                      </div>
                    </div>
                  </div>
                ) : modalType === 'block' ? (
                  <div className="blocked-message blocked-by-you">
                    <div>{YOU_HAVE_BLOCKED}
                      <Link route="profile" params={{slug: modalDetails.getIn(['user', 'username'])}}>
                        <a target="_blank" title={modalDetails.getIn(['user', 'name_display'])}>
                          {modalDetails.getIn(['user', 'username'])}
                        </a>
                      </Link>
                    </div>
                    <div className="tip">{BLOCK_GUIDELINE}</div>
                  </div>
                ) : modalType === 'delete' ?  (
                  <div className="confirmation">
                    <div className="separator">{ CONFIRM_DELETE }<span>{modalDetails.get('title')}</span>?</div>
                    {
                      deleting ? (
                        <div className="button-grid">
                          <Button color="white" background="bggrey">
                            <a className="disabled"><Spinner color="white" /><span>{ NO }, { CANCEL }</span></a>
                          </Button>
                          <Button color="white" background="bggrey">
                            <a className="disabled"><Spinner color="white" /><span>{ YES }, { DELETE }</span></a>
                          </Button>
                        </div>
                      ) : (
                        <div className="button-grid">
                          <Button color="grey" background="bgwhite">
                            <a onClick={this.handleShowModalInactive}><span>{ NO }, { CANCEL }</span></a>
                          </Button>
                          <Button color="white" background="bggored">
                            <a onClick={() => this.handleDeleteVideo(modalDetails)}><span>{ YES }, { DELETE }</span></a>
                          </Button>
                        </div>
                      )
                    }
                  </div>
                ) : null
              }
            </Modal>
          )}
        </Transition>
      </div>
    )
  }
} 
const mapStateToProps = state => ({
  modalType: state.getIn(['video', 'modalType']),
  modalDetails: state.getIn(['video', 'modalDetails']),
  jwt: state.getIn(['video', 'modalJwt']),
  showModal: state.getIn(['video', 'showModal']),
  lat: state.getIn(['user', 'lat']),
  long: state.getIn(['user', 'long']),
  deleting: state.getIn(['video', 'deleting'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    hideModal,
    deleteSingleVideoRequest,
    saveLocationRequest
  },
  dispatch
);

export default WithCustomCookies(geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(connect(mapStateToProps, mapDispatchToProps)(withRouter(Layout))))
