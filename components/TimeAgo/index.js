import React from 'react';
import TimeAgo from 'react-timeago';

import {DOT} from 'helpers/text'

const Time = ({date}) => (
  <div className="ago"><span className="dot">{DOT}</span><TimeAgo date={date} /></div>
)

export default Time;