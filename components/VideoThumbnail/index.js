import React, {Component} from 'react';
import RandomGirl from 'components/RandomGirl';

class VideoThumbnail extends Component {
  constructor() {
    super();

    this.state = {
      error: false
    }
    this.handleError = this.handleError.bind(this)
  }

  handleError = () => {
    this.setState({error: true})
  }

  render() {
    const {alt, thumbnail, hasOverlays} = this.props;
    const {error} = this.state;
    const style = !error ? {} : {visibility: 'hidden'}
    return (
      <div className={`video-thumbnail ${hasOverlays ? 'overlayed' : '' }`}>
        <img 
          style={style}
          alt={alt} 
          src={thumbnail} 
          onError={this.handleError}
          title={alt}
        />
        {
          error ? (
            <RandomGirl centered title={alt} alt={alt} />
          ) : null
        }
      </div>
    )
  }
}

export default VideoThumbnail