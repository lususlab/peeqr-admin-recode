import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import withReduxSaga from "next-redux-saga";
import TimeAgo from 'react-timeago';
import { Link } from 'routes';

import { 
  DOT,
  NOTHING_TO_SEE_HERE,
  JUST_WENT_LIVE,
  COMMENTED,
  ON,
  LIKED,
  VIEWED,
  SHARED,
  SUBSCRIBED_TO_YOU,
  VIEW_ALL_NOTIFICATIONS
} from 'helpers/text';

import {
  loadMoreNotificationsRequest
} from 'store/user/actions'

import RandomGirl from 'components/RandomGirl';
import Avatar from 'components/Avatar';
import Button from 'components/Button';
import LoadMore from 'components/LoadMore';
import CheckLink from 'components/CheckLink';
import Spinner from 'components/Spinner'

class NotificationsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pagedNotifications: props.notifications
    }

    this.onMore = this.onMore.bind(this);
  }
  onMore = () => {
    const { notifications, jwt } = this.props;
    this.props.loadMoreNotificationsRequest(notifications.get('next'), jwt);
  }
  renderList = () => {
    const { notifications, isNotificationsPage, loadingMoreNotifications } = this.props;
    const { pagedNotifications } = this.state;

    return (
      <div>
        {
          notifications.get('results').map(item => (
            !item.get('is_read') || isNotificationsPage ? (
              <div className="notification" key={item.get('id')}>
                {
                  item.get('type') === "new_stream" ? (
                    <div>
                      <CheckLink route="video" params={{id: item.getIn(['video', 'id'])}} item={item}>
                        <div className="details">
                          <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                          <div>
                            {item.getIn(['user', 'name_display'])} {DOT} {item.getIn(['user', 'username'])} {JUST_WENT_LIVE}
                            <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                          </div>
                        </div>
                      </CheckLink>
                    </div>
                  ) : item.get('type') === "view" ? (
                    <div>
                      <CheckLink route="video" params={{id: item.getIn(['video', 'id'])}} item={item}>
                        <div className="details">
                          <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                          <div>
                            {item.getIn(['user', 'name_display'])} {DOT} {item.getIn(['user', 'username'])} {VIEWED} <span className="highlight">{item.getIn(['video', 'title'])}</span>
                            <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                          </div>
                        </div>
                      </CheckLink>
                    </div>
                  ) : item.get('type') === "comment" ? (
                    <div>
                      <CheckLink route="video" params={{id: item.getIn(['video', 'id'])}} item={item}>
                        <div className="details">
                          <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                          <div>
                            {item.getIn(['user', 'name_display'])} {DOT} {item.getIn(['user', 'username'])} {COMMENTED} <span className="highlight">{`"${item.get('message')}"`}</span> {ON} <span className="highlight">{item.getIn(['video', 'title'])}</span>
                            <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                          </div>
                        </div>
                      </CheckLink>
                    </div>
                  ) : item.get('type') === "like" ? (
                    <div>
                      <CheckLink route="video" params={{id: item.getIn(['video', 'id'])}} item={item}>
                        <div className="details">
                          <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                          <div>
                            {item.getIn(['user', 'name_display'])} {DOT} {item.getIn(['user', 'username'])} {LIKED} <span className="highlight">{item.getIn(['video', 'title'])}</span>
                            <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                          </div>
                        </div>
                      </CheckLink>
                    </div>
                  ) : item.get('type') === "share" ? (
                    <div>
                      <CheckLink route="video" params={{id: item.getIn(['video', 'id'])}} item={item}>
                        <div className="details">
                          <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                          <div>
                            {item.getIn(['user', 'name_display'])} {DOT} {item.getIn(['user', 'username'])} {SHARED} <span className="highlight">{item.getIn(['video', 'title'])}</span>
                            <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                          </div>
                        </div>
                      </CheckLink>
                    </div>
                  ) : item.get('type') === "subscribes" ? (
                    <div>
                      <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
                        <a>
                          <div className="details">
                            <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                            <div>
                              {item.getIn(['user', 'name_display'])} {DOT} {item.getIn(['user', 'username'])} {SUBSCRIBED_TO_YOU}
                              <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                            </div>
                          </div>
                        </a>
                      </Link>
                    </div>
                  ) : item.get('type') === "messages" ? (
                    <div>
                      <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
                        <a>
                          <div className="details">
                            <div><Avatar size="small" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} /></div>
                            <div>
                              {item.getIn(['user', 'name_display'])} {DOT} {item.get('text')}
                              <div className="ago"><TimeAgo date={item.get('history_date')} /></div>
                            </div>
                          </div>
                        </a>
                      </Link>
                    </div>
                  ) : null
                }
              </div>
            ) : null
          ))
        }
        {
          isNotificationsPage && notifications.get('next') ? (
            <div className="centered">
              <LoadMore size="small" color="white" background="bggrey" onClick={this.onMore} loading={loadingMoreNotifications} />
            </div>
          ) : null
        }
      </div>
    )
  }
  render() {
    const { gettingNotifications, notifications, isNotificationsPage } = this.props;
    const { pagedNotifications } = this.state;
    return (
      <div className="notifications">
        {
          gettingNotifications ? (
            <Spinner type="relative" color="grey" />
          ) : (
            pagedNotifications.get('count') > 0 && isNotificationsPage ? (
              <div>
                {this.renderList()}
              </div>
            ) : notifications.get('count') > 0 && !isNotificationsPage && notifications.get('results').some(item => !item.get('is_read')) ? (
              <div>
                <div className="view_all">
                  <Button size="small" background="bggrey" color="white">
                    <Link route="notifications">
                      <a target="_blank">{VIEW_ALL_NOTIFICATIONS}</a>
                    </Link>
                  </Button>
                </div>
                {this.renderList()}
              </div>
            ) : (notifications.get('count') === 0 || notifications.get('results').some(item => item.get('is_read')) && !isNotificationsPage)  ? (
              <div className="notification none">
                <div className="pchan">
                  <RandomGirl />
                </div>
                {NOTHING_TO_SEE_HERE}
                {
                  !isNotificationsPage ? (
                    <div className="view_all">
                      <Button size="small" background="bggrey" color="white">
                        <Link route="notifications">
                          <a target="_blank">{VIEW_ALL_NOTIFICATIONS}</a>
                        </Link>
                      </Button>
                    </div>
                  ) : null
                }
              </div>
            ) : null
          )
        }
      </div>
    )
  }
}


const mapStateToProps = state => ({
  loadingMoreNotifications: state.getIn(['user', 'loadingMoreNotifications'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    loadMoreNotificationsRequest,
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsList)
