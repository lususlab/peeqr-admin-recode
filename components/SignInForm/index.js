import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Link } from "routes";
import md5 from 'md5'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import TwitterLogin from 'react-twitter-auth/lib/react-twitter-auth-component';

import { Router } from 'routes';

import hairimg from 'static/img/girl-auth/hair.png';
import reflectimg from 'static/img/girl-auth/reflect.png';
import headimg from 'static/img/girl-auth/head.png';
import blushimg from 'static/img/girl-auth/blush.png';
import eyelimg from 'static/img/girl-auth/eye-l.png';
import eyexlimg from 'static/img/girl-auth/eye-closed-l.png';
import eyebrowlimg from 'static/img/girl-auth/eyebrow-l.png';
import eyebrowxlimg from 'static/img/girl-auth/eyebrow-closed-l.png';
import eyerimg from 'static/img/girl-auth/eye-r.png';
import eyexrimg from 'static/img/girl-auth/eye-closed-r.png';
import eyebrowrimg from 'static/img/girl-auth/eyebrow-r.png';
import eyebrowxrimg from 'static/img/girl-auth/eyebrow-closed-r.png';
import mouthimg from 'static/img/girl-auth/mouth.png';

import Layout from 'components/Layout';
import Input from 'components/Input';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Error from 'components/Error';

import { 
  emailLoginRequest, 
  facebookLoginRequest,
  twitterLoginRequest
} from 'store/user/actions';

import { 
  LOGIN, 
  LOGIN_TWITTER, 
  LOGIN_FACEBOOK, 
  OR,
  NOT_A_USER,
  SIGN_UP_NOW,
  FORGOT_PASSWORD,
  LABEL_EMAIL, 
  LABEL_PASSWORD
} from 'helpers/text';

const API_ROOT = process.env.API_ROOT;
const API_VERSION = process.env.API_VERSION;
const HOSTNAME = process.env.HOSTNAME

class SignInForm extends Component {
  constructor() {
    super();

    this.state = {
      email: '',
      password: '',
      error: '',
    }
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleFacebookSignIn = this.handleFacebookSignIn.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.handleTwitterSignIn = this.handleTwitterSignIn.bind(this);
    this.handleTwitterFailed = this.handleTwitterFailed.bind(this);
  }
  componentDidMount() {
    this.runGirlAuth();
  }
  componentDidUpdate(prevProps) {
    const { errorFromState } = this.props;
    if (errorFromState !== prevProps.errorFromState && errorFromState) {
      this.setState({ error: errorFromState })
    }
  }
  onEmailChange(e) {
    this.setState({ email: e.target.value, error: '' });
  }
  onPasswordChange(e) {
    this.setState({ password: e.target.value, error: '' });
  }
  handleSignIn() {
    const { email, password, emailLoginRequest } = this.state;
    emailLoginRequest(email, md5(password));
  }

  handleTwitterSignIn(res) {
    const {twitterLoginRequest} = this.props;
    res.json().then(body => {
      twitterLoginRequest(body)
    });
  }
  handleTwitterFailed(e) {
    alert(e);
  }
  handleFacebookSignIn(res) {
    const {facebookLoginRequest} = this.props;
    this.setState({ error: '' })

    facebookLoginRequest(res.accessToken)
  }
  runGirlAuth() {
    const email = document.querySelector('#email');
    const password = document.querySelector('#password');
    const head = document.querySelector('.girl-auth .head');
    const reflect = document.querySelector('.girl-auth .reflect');
    const eyes = document.querySelector('.girl-auth .eyes');
    const eyesopen = eyes.getElementsByClassName('open');
    const eyesclosed = eyes.getElementsByClassName('closed');
    const eyel = document.querySelector('.girl-auth .eyes .eye-left');
    const eyebrowl = document.querySelector('.girl-auth .eyes .eyebrow-left');
    const eyer = document.querySelector('.girl-auth .eyes .eye-right');
    const eyebrowr = document.querySelector('.girl-auth .eyes .eyebrow-right');
    const blushl = document.querySelector('.girl-auth .eyes .blush-left');
    const blushr = document.querySelector('.girl-auth .eyes .blush-right');
    const mouth = document.querySelector('.girl-auth .mouth');

    const Yeyebrowl = '2px';
    const Yeyebrowr = '2px';
    const Yeyel = '4px';
    const Yeyer = '4px';
    const Yblushl = '4px';
    const Yblushr = '4px';
    const Ymouth = '6px';
    const Yhead = '1px';
    const Yreflect = '-2px';

    function getCoord() {
      const carPos = email.selectionEnd;
      const div = document.createElement('div');
      const span = document.createElement('span');
      const copyStyle = getComputedStyle(email);
      let emailCoords = {};
      let caretCoords = {};
      div.setAttribute('class', 'helper');

      [].forEach.call(copyStyle, (prop) => {
        div.style[prop] = copyStyle[prop];
      });

      div.style.position = 'absolute';
      document.body.appendChild(div);
      div.textContent = email.value.substr(0, carPos);
      span.textContent = email.value.substr(0) || '.';
      div.appendChild(span);

      emailCoords = email.offsetWidth;
      caretCoords = span.offsetWidth;

      function calculateMove(amount) {
        const coords = amount / emailCoords;
        const defaultValue = amount * -0.5;
        const finalValue = `${ Math.min(Math.max(defaultValue + (coords * caretCoords), defaultValue), amount * 0.5) }px`;
        return finalValue;
      }

      const headDistance = 6;
      const Xhead = calculateMove(headDistance);
      const reflectDistance = 4;
      const Xreflect = calculateMove(reflectDistance);
      const eyeDistance = 16;
      const Xeye = calculateMove(eyeDistance);
      const eyebrowDistance = 8;
      const Xeyebrow = calculateMove(eyebrowDistance);
      const blushDistance = 12;
      const Xblush = calculateMove(blushDistance);
      const mouthDistance = 12;
      const Xmouth = calculateMove(mouthDistance);

      head.style.transform = `translate(${ Xhead },${ Yhead })`;
      reflect.style.transform = `translate(${ Xreflect },${ Yreflect })`;
      eyel.style.transform = `translate(${ Xeye },${ Yeyel })`;
      eyebrowl.style.transform = `translate(${ Xeyebrow },${ Yeyebrowl })`;
      eyer.style.transform = `translate(${ Xeye },${ Yeyer })`;
      eyebrowr.style.transform = `translate(${ Xeyebrow },${ Yeyebrowr })`;
      blushl.style.transform = `translate(${ Xblush },${ Yblushl })`;
      blushr.style.transform = `translate(${ Xblush },${ Yblushr })`;
      mouth.style.transform = `translate(${ Xmouth },${ Ymouth })`;

      document.body.removeChild(div);
    }

    function resetState() {
      mouth.classList.remove('closed');
      eyes.classList.remove('closed');
      head.style.transform = 'translate(0px,0px)';
      reflect.style.transform = 'translate(0px,0px)';
      eyel.style.transform = 'translate(0px,0px)';
      eyebrowl.style.transform = 'translate(0px,0px)';
      eyer.style.transform = 'translate(0px,0px)';
      eyebrowr.style.transform = 'translate(0px,0px)';
      blushl.style.transform = 'translate(0px,0px)';
      blushr.style.transform = 'translate(0px,0px)';
      mouth.style.transform = 'translate(0px,0px)';

      for (let i = 0; i < eyesopen.length; i++) {
        eyesopen[i].setAttribute('style', 'opacity:1;');
      }
      for (let i = 0; i < eyesclosed.length; i++) {
        eyesclosed[i].setAttribute('style', 'opacity:0;');
      }
    }

    function emailBlur() {
      resetState();
    }

    function emailInput(e) {
      getCoord(e);
      const value = e.target.value;

      if (!value.includes('@')) {
        mouth.classList.add('closed');
        eyes.classList.add('closed');
      } else {
        mouth.classList.remove('closed');
        eyes.classList.remove('closed');
      }
    }
    function passwordFocus() {
      for (let i = 0; i < eyesopen.length; i++) {
        eyesopen[i].setAttribute('style', 'opacity:0; transform:scaleY(0.2)');
      }
      for (let i = 0; i < eyesclosed.length; i++) {
        eyesclosed[i].setAttribute('style', 'opacity:1;');
      }
    }
    function passwordBlur() {
      resetState();
    }
    email.addEventListener('focus', emailInput);
    email.addEventListener('blur', emailBlur);
    email.addEventListener('input', emailInput);
    password.addEventListener('focus', passwordFocus);
    password.addEventListener('blur', passwordBlur);
  }

  render() {
    const { loggingIn } = this.props;
    const { email, password, error } = this.state;
    const fields = [ email, password ];
    const formIsNotEmpty = fields.every(field => field.length > 0);

    return (
      <form id="signin-form">
        <div className="hero">
          <div className="girl-auth">
            <div className='hair'><img alt='icongirl' src={hairimg} /></div>
            <div className='reflect'><img alt='icongirl' src={reflectimg} /></div>
            <div className='head'><img alt='icongirl' src={headimg} /></div>
            <div className='eyes'>
              <div className='blush-left'><img alt='icongirl' src={blushimg} /></div>
              <div className='blush-right'><img alt='icongirl' src={blushimg} /></div>
              <div className='eye-left'><img alt='icongirl' ref='open' className='open' src={eyelimg} /><img alt='icongirl' ref='closed' className='closed' src={eyexlimg} /></div>
              <div className='eyebrow-left'><img alt='icongirl' ref='open' className='open' src={eyebrowlimg} /><img alt='icongirl' ref='closed' className='closed' src={eyebrowxlimg} /></div>
              <div className='eye-right'><img alt='icongirl' ref='open' className='open' src={eyerimg} /><img alt='icongirl' ref='closed' className='closed' src={eyexrimg} /></div>
              <div className='eyebrow-right'><img alt='icongirl' ref='open' className='open' src={eyebrowrimg} /><img alt='icongirl' ref='closed' className='closed' src={eyebrowxrimg} /></div>
            </div>
            <div className='mouth'><img alt='icongirl' src={mouthimg} /></div>
          </div>
        </div>
        <Input 
          type="text"
          theme="boxed"
          name={LABEL_EMAIL}
          placeholder={LABEL_EMAIL}
          value={email}
          onChange={this.onEmailChange}
          required
        />
        <Input 
          type="password"
          theme="boxed"
          name={LABEL_PASSWORD}
          placeholder={LABEL_PASSWORD}
          value={password}
          forgot
          showPassword
          onChange={this.onPasswordChange}
          required
        />
        {
          error ? (
            <Error error={error}/>
          ) : null
        }
        {
          (loggingIn || !formIsNotEmpty) ? (
            <Button color="white" background="bggrey">
              <a className="disabled">
                {(loggingIn) ? (
                  <Spinner color="white" />
                ) : null}<span>{ LOGIN }</span>
              </a>
            </Button>
          ) : (
            <Button color="white" background="bglightpurple">
              <a onClick={this.handleSignIn}><span>{ LOGIN }</span></a>
            </Button>
          )
        }
        <div className="separator">{ OR }</div>
        <div className="input-group">
          {
            loggingIn ? (
              <Button size="small" color="white" background="bggrey">
                <a className="disabled">
                  <Spinner color="white" />
                  <span>{ LOGIN_FACEBOOK }</span>
                </a>
              </Button>
            ) : (
              <FacebookLogin
                appId={process.env.FACEBOOK_CLIENT_ID}
                autoLoad={false}
                callback={res => this.handleFacebookSignIn(res)}
                render={renderProps => (
                  <Button size="small" color="white" background="bgfbblue">
                    <a onClick={renderProps.onClick}>
                      <FontAwesomeIcon icon={faFacebookF} size="sm" />
                      <span>{ LOGIN_FACEBOOK }</span>
                    </a>
                  </Button>
                )}
              />
            )
          }
        </div>
        <div className="input-group">
          {
            loggingIn ? (
                <Button size="small" color="white" background="bggrey">
                  <a className="disabled">
                    <Spinner color="white" />
                    <span>{ LOGIN_TWITTER }</span>
                  </a>
                </Button>
              
            ) : (
              <TwitterLogin 
                loginUrl={`${ API_ROOT }/${ API_VERSION }/twitter/access_token/`}
                onFailure={this.handleTwitterFailed}
                onSuccess={this.handleTwitterSignIn}
                requestTokenUrl={`${ API_ROOT }/${ API_VERSION }/twitter/oauth_token/`}
              >
                <Button size="small" color="white" background="bgtwblue">
                  <a>
                    <FontAwesomeIcon icon={faTwitter} size="sm" />
                    <span>{ LOGIN_TWITTER }</span>
                  </a>
                </Button>
              </TwitterLogin>
            )
          }
        </div>
        <p>
          { NOT_A_USER }
          <Link route="signup">
            <a>{ SIGN_UP_NOW }</a>
          </Link>
        </p>
        <p>
          <Link route="forgot">
            <a>{ FORGOT_PASSWORD }</a>
          </Link>
        </p>
      </form>
    )
  }
}
const mapStateToProps = state => ({
  loggingIn: state.getIn(['user', 'loggingIn']),
  errorFromState: state.getIn(['user', 'error']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    emailLoginRequest,
    facebookLoginRequest,
    twitterLoginRequest
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm)

