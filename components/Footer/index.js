import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import {Link} from "routes";
import {
  ABOUT,
  BLOG,
  TERMS, 
  PRIVACY,
  SEND_FEEDBACK,
  COPYRIGHT,
  COMPANY,
  GUIDELINES
} from 'helpers/text'

import {
  showModal,
} from 'store/video/actions';

class Footer extends Component {
  constructor() {
    super();
    
    this.handleShowModalActive = this.handleShowModalActive.bind(this)
  }

  handleShowModalActive = () => {
    const { showModal } = this.props;
    showModal('', 'guidelines')
  }

  render() {
    return (
      <footer>
        <ul className="top">
          <li>
            <Link route="about">
              <a>
                {ABOUT}
              </a>
            </Link>
          </li>
          <li>
            <Link route="http://blog.peeqr.com">
              <a target="_blank">
                {BLOG}
              </a>
            </Link>
          </li>
        </ul>
        <ul className="bottom">
          <li>
            <Link route="terms">
              <a>
                {TERMS}
              </a>
            </Link>
          </li>
          <li>
            <Link route="privacy">
              <a>
                {PRIVACY}
              </a>
            </Link>
          </li>
          <li>
            <Link route="feedback">
              <a>
                {SEND_FEEDBACK}
              </a>
            </Link>
          </li>
          <li>
            <a onClick={this.handleShowModalActive}>
              {GUIDELINES}
            </a>
          </li>
          <li>
            {COPYRIGHT} {(new Date()).getFullYear()} {COMPANY}
          </li>
        </ul>
      </footer>
    );
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    showModal,
  },
  dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(Footer)
