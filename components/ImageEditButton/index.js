import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTimes, faPlus } from '@fortawesome/free-solid-svg-icons';
import Transition from 'react-transition-group/Transition';
import MediaQuery from 'react-responsive';

import Modal from 'components/Modal';
import Spinner from 'components/Spinner';
import Button from 'components/Button';

import { 
  getAWSSignatureRequest,
  clearAWSSignatureRequest,
  uploadProfileImageRequest,
  uploadCoverImageRequest
} from 'store/user/actions';

import {
  IMAGE_GUIDE,
  IMAGE_RULES,
  UPLOAD
} from 'helpers/text'

class ImageEditButton extends Component {
  constructor(props) {
    super(props)

    this.state = {
      uploadModalActive: false,
      profileImage: {},
      coverImage: {},
    }

    this.handleActive = this.handleActive.bind(this);
    this.handleInactive = this.handleInactive.bind(this);
    this.handleRemoveImage = this.handleRemoveImage.bind(this);
    this.onImageDrop = this.onImageDrop.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
  }
  
  componentWillUnmount() {
    this.props.clearAWSSignatureRequest();
  }
  onImageDrop(files) {
    const {type} = this.props;
    files.forEach(file => {
      const reader = new FileReader();

      reader.onload = () => {
        if (type === 'avatar') {
          this.setState({profileImage: file})
        } else {
          this.setState({coverImage: file})
        }
      };
      reader.readAsDataURL(file);
    });
  }
  handleUpload() {
    const {type, username, uploadProfileImageRequest, uploadCoverImageRequest, awsSignatureData, jwt} = this.props;
    const {profileImage, coverImage} = this.state;

    if (type === 'avatar') {
      const filename = `${username}_profile.${profileImage.name.split('.').pop()}`;
      uploadProfileImageRequest(profileImage, filename, username, awsSignatureData, jwt)
    } else {
      const filename = `${username}_coverphoto.${coverImage.name.split('.').pop()}`;
      uploadCoverImageRequest(coverImage, filename, username, awsSignatureData, jwt)
    }
  }
  handleRemoveImage() {
    this.setState({ profileImage: {}, coverImage: {}, })
  }
  handleActive() {
    const {jwt, getAWSSignatureRequest} = this.props;

    getAWSSignatureRequest(jwt);
    this.setState({ uploadModalActive: true })
  }
  handleInactive() {
    this.setState({ uploadModalActive: false })
  }
  
  renderUploadModal() {
    const { uploading, type, gettingSignature, updatingProfile } = this.props;
    const { profileImage, coverImage } = this.state;

    return (
      <div className={`upload type-${type}`}>
        <Dropzone
          className="dropzone"
          activeClassName="dropaccept"
          rejectClassName="dropreject"
          multiple={false}
          accept="image/jpeg, image/png"
          onDrop={this.onImageDrop}
          disabled={uploading || Object.keys(profileImage).length > 0 || Object.keys(coverImage).length > 0}
        >
          {
            uploading || gettingSignature || updatingProfile ? (
              <div className="image-feedback active">
                <Spinner type="relative" color="grey" />
              </div>
            ) : null
          }
          {
            type === 'avatar' ? (
              (Object.keys(profileImage).length > 0) ? (
                <div className="image-thumbnail">
                  <a className="destroy" onClick={this.handleRemoveImage}>
                    <FontAwesomeIcon icon={faTimes} size="2x" />
                  </a>
                  <img src={profileImage.preview} />
                </div>
              ) : (
                <FontAwesomeIcon icon={faPlus} size="2x" />
              )
            ) : (
              (Object.keys(coverImage).length > 0) ? (
                <div className="image-thumbnail">
                  <a className="destroy" onClick={this.handleRemoveImage}>
                    <FontAwesomeIcon icon={faTimes} size="2x" />
                  </a>
                  <img src={coverImage.preview} />
                </div>
              ) : (
                <FontAwesomeIcon icon={faPlus} size="2x" />
              )
            )
          }
        </Dropzone>
        <p>{ IMAGE_GUIDE }<br /><span>{ IMAGE_RULES }</span></p>
        <div className="centered">
          {
            Object.keys(profileImage).length > 0 || Object.keys(coverImage).length > 0 ? (
              <Button size="small" color="white" background="bglightpurple">
                {
                  uploading ? (
                    <a className="disabled"><Spinner color="purple" /><span>{UPLOAD}</span></a>
                  ) : (
                    <a onClick={this.handleUpload}><span>{UPLOAD}</span></a>
                  )
                }
              </Button>
            ) : null
          }
        </div>
      </div>
    )
  }
  render() {
    const {position, type} = this.props;
    const {uploadModalActive, isSelf} = this.state;

    return (
      !isSelf ? (
        <div className={`image-edit type-${type}`}>
          <a onClick={this.handleActive}>
            <FontAwesomeIcon icon={faPencilAlt} />
          </a>
          <Transition
            timeout={150}
            in={uploadModalActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <Modal classes={`fade-${status}`} close={() => this.handleInactive}>
                {this.renderUploadModal()}
              </Modal>
            )}
          </Transition>
        </div>
      ) : null
    )
  }
}
const mapStateToProps = state => ({
  me: state.getIn(['user', 'me']),
  uploading: state.getIn(['user', 'uploading']),
  gettingSignature: state.getIn(['user', 'gettingSignature']),
  awsSignatureData: state.getIn(['user', 'awsSignature']),
  updatingProfile: state.getIn(['user', 'updatingProfile'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      uploadCoverImageRequest,
      uploadProfileImageRequest,
      getAWSSignatureRequest,
      clearAWSSignatureRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ImageEditButton)
