import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Link } from "routes";

import VideoItem from 'components/VideoItem';
import Spinner from 'components/Spinner';

import {
  getCategoryVideosRequest
} from 'store/video/actions'

import {
  getVideoEntities,
  getCategoryVideos
} from 'store/video/selectors'

import {lowerCase} from 'helpers/lowercase'

import {VIEW_ALL} from 'helpers/text'

class Collection extends Component {
  constructor(props) {
    super(props)

    this.state = {
      title: props.collection.get('name')
    }
  }

  async componentDidMount() {
    const {getCategoryVideosRequest, videoType, collection, jwt} = this.props;
    if (videoType === 'category') {
      await getCategoryVideosRequest(collection.get('id'), 1, 5, jwt)
    }
  }

  render() {
    const { videoType, collection, videos, gettingCategoryVideos } = this.props;
    const { title } = this.state;

    return (
      !gettingCategoryVideos ? (
        videos.size > 0 ? (
          <div id={lowerCase(title)} className={`collection ${videoType}`}>
            <div className="collection-header">
              <h4 className="collection-title">
                <span>{videoType === "category" ? <img className="icon" src={collection.get('icons')} /> : null}</span>
                <span>{title}</span>
              </h4>
              <Link route="collection">
                <a target="_blank">
                  <div className="view-all">
                    <span>{VIEW_ALL}</span>
                    <FontAwesomeIcon icon={faChevronRight} />
                  </div>
              </a>
              </Link>
            </div>
            <div className="deck">
              {
                videos.sort((a, b) => {
                  if (a.get('score') > b.get('score')) return -1
                  if (a.get('score') < b.get('score')) return 1
                  if (new Date(a.get('upload_date')).getTime() > new Date(b.get('upload_date')).getTime()) return -1
                  if (new Date(a.get('upload_date')).getTime() < new Date(b.get('upload_date')).getTime()) return 1
                }).slice(0, 5).map((item, i) => (
                  <VideoItem 
                    key={`${item.get('id')}-${i}`}
                    className="deck-item"
                    item={item}
                  />
                ))
              }
            </div>
          </div>
        ) : null
      ) : <Spinner type="relative" color="grey" />
    )
  }
}
const mapStateToProps = (state, props) => ({
  gettingCategoryVideos: state.getIn(['video', 'gettingCategoryVideos']),
  videos: props.videoType === 'category' ? getCategoryVideos(getVideoEntities(state), props.collection.get('id')) : props.collection.get('video')
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCategoryVideosRequest,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Collection)
