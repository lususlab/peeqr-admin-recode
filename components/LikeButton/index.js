import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import CountUp from 'react-countup';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as faHeartFilled } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from '@fortawesome/free-regular-svg-icons';

import { 
  likeRequest,
  unlikeRequest,
} from 'store/user/actions';

import {
  TOTAL_LIKES,
  SIGN_IN_TO_LIKE,
} from "helpers/text";

import {
  NO_ACCESS
} from "helpers/errors"

import { createNotification } from 'helpers/createnotification';
import { videoCurrentTime } from 'helpers/videohelper';

import Spinner from "components/Spinner";

class LikeButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      like_count_start: 0,
      like_count_end: props.likes,
    }
    this.heartRef = React.createRef()
    this.handleLike = this.handleLike.bind(this)
    this.onCompleteLikes = this.onCompleteLikes.bind(this)
  }
  componentDidMount() {
    const { socket } = this.props;

    socket.on('videoActions', data => {
      this.handleSocketData(data);
    });

  }
  handleSocketData = (data) => {
    if (data.cmd === "like" || data.cmd === "unlike") {
      this.setState({
        like_count_end: data.like_count,
      })
    }
  }
  onCompleteLikes = () => {
    this.setState({
      like_count_start: this.state.like_count_end
    })
  }

  handleLike() {
    const {stat, likeRequest, unlikeRequest, videoId, mergeStatus, playerState, jwt, me, hasAccess, isShow} = this.props;
    const timestamp = videoCurrentTime(playerState, mergeStatus);

    if (!isShow || hasAccess && isShow) {
      if (me.size > 0) {
        if (stat) {
          unlikeRequest(videoId, timestamp, jwt) 
        } else {
          const node = this.heartRef.current;
          node.classList.add('jumpUp')
          setTimeout(() => {
            node.classList.remove('jumpUp')
          }, 800)
          likeRequest(videoId, timestamp, jwt)
        }
      } else {
        createNotification('error', SIGN_IN_TO_LIKE)
      }
    } else {
      createNotification('error', NO_ACCESS)
    }
  }
  render() {
    const { stat, liking } = this.props;
    const { like_count_start, like_count_end } = this.state;

    return (
      <a className={liking ? 'disabled' : ''} onClick={this.handleLike} title={TOTAL_LIKES}>
        <div className={`item likes ${stat ? 'liked' : ''}`}>
          {
            liking ? (
              <Spinner color="grey" />
            ) : (
              <FontAwesomeIcon icon={stat ? faHeartFilled : faHeart} />
            )
          }
          <span ref={this.heartRef} className="hidden-heart hidden-icon"><FontAwesomeIcon icon={faHeartFilled} /></span>
          <CountUp className="value" start={like_count_start} end={like_count_end} onEnd={this.onCompleteLikes}/>
        </div>
      </a>
    )
  }
}
const mapStateToProps = state => ({
  liking: state.getIn(['user', 'liking']),
  me: state.getIn(['user', 'me'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      likeRequest,
      unlikeRequest,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(LikeButton)
