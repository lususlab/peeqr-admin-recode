import React, {Component} from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Transition from 'react-transition-group/Transition';
import MediaQuery from 'react-responsive';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserAltSlash, faCheck } from '@fortawesome/free-solid-svg-icons';

import { 
  blockRequest,
  unblockRequest,
} from 'store/user/actions';

import {
  BLOCK,
  UNBLOCK, 
  SIGN_IN_TO_BLOCK,
  CONFIRM,
  CANCEL
} from "helpers/text";

import {createNotification} from 'helpers/createnotification'
import { closeonclick } from "helpers/closeonclick";
import { lowerCase } from "helpers/lowercase";

import Spinner from "components/Spinner";
import BubbleMenu from 'components/BubbleMenu';
import Modal from 'components/Modal';
import Button from "components/Button";

class BlockButton extends Component {
  constructor() {
    super();

    this.state = {
      modalActive: false
    }
    this.handleActive = this.handleActive.bind(this)
    this.handleInactive = this.handleInactive.bind(this)
    this.handleBlock = this.handleBlock.bind(this)
  }
  componentDidUpdate(prevProps, prevState) {
    const { modalActive } = this.state;
    if (modalActive !== prevState.modalActive && modalActive) {
      closeonclick(this.handleInactive);
    }
  }
  handleActive() {
    this.setState({ modalActive: true })
  }

  handleInactive() {
    this.setState({ modalActive: false })
  }

  handleBlock() {
    const {stat, unblockRequest, blockRequest, userId, username, jwt, me} = this.props;
    if (me.size > 0) {
      if (stat) {
        unblockRequest(userId, username, jwt) 
      } else {
        blockRequest(userId, username, jwt)
      }
      this.handleInactive();
    } else {
      createNotification('error', SIGN_IN_TO_BLOCK)
    }
  }

  renderContent() {
    const {name, blocking} = this.props;
    return (
      <div className="confirmation">
        <div className="confirm-body">
          {BLOCK} {name}?
        </div>
        <div className="confirm-buttons">
          {
            blocking ? (
              <Button color="white" background="bggrey" size="small">
                <a className="disabled">
                  <Spinner color="grey" />
                  <span>{ CONFIRM }</span>
                </a>
              </Button>
            ) : (
              <Button color="white" background="bggored" size="small">
                <a onClick={this.handleBlock}><span>{ CONFIRM }</span></a>
              </Button>
            )
          }
          <Button color="white" background="bggrey" size="small">
            <a onClick={this.handleInactive}><span>{ CANCEL }</span></a>
          </Button>
        </div>
      </div>
    )
  }

  render() {
    const { stat, blocking, position } = this.props;
    const { modalActive } = this.state;
    return (
      <div className="item block">
        <a className={blocking ? 'disabled' : ''} onClick={stat ? this.handleBlock : this.handleActive}>
          <div className={`circle ${stat ? 'red' : ''}`}>
            {
              blocking ? (
                <Spinner color="white" />
              ) : (
                <FontAwesomeIcon icon={stat ? faCheck : faUserAltSlash} size="xs" />
              )  
            }
          </div>
          <span>{stat ? lowerCase(UNBLOCK) : lowerCase(BLOCK)}</span>
        </a>
        <MediaQuery minWidth={768}>
          <Transition
            timeout={150}
            in={modalActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <BubbleMenu classes={`fade-${status} ${position}`} >
                {this.renderContent()}
              </BubbleMenu>
            )}
          </Transition>
        </MediaQuery>
        <MediaQuery maxWidth={767}>
          <Transition
            timeout={150}
            in={modalActive}
            mountOnEnter
            unmountOnExit
          >
            {status => (
              <Modal classes={`fade-${status}`} close={() => this.handleInactive}>
                {this.renderContent()}
              </Modal>
            )}
          </Transition>
        </MediaQuery>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  blocking: state.getIn(['user', 'blocking']),
  me: state.getIn(['user', 'me'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      blockRequest,
      unblockRequest,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(BlockButton)
