import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import Transition from 'react-transition-group/Transition';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
  faBars, 
  faTimes,
  faCameraRetro,
  faSignOutAlt,
  faCogs,
  faUser,
  faSearch
} from '@fortawesome/free-solid-svg-icons';

import { Link } from "routes";
import WithCustomCookies from 'hoc/WithCustomCookies'

import { closeonclick } from "helpers/closeonclick";
import { 
  AT,
  BE_A_PARTNER, 
  SIGN_IN_SIGN_UP, 
  GET_THE_APP,
  GO_LIVE,
  MY_PROFILE,
  SETTINGS,
  SIGN_OUT,
} from 'helpers/text';
import SvgLogo from 'static/svg/svg-logo.svg';

import { 
  getSelfRequest,
  logOutRequest,
  getBlockedListRequest
} from 'store/user/actions';

import BubbleMenu from 'components/BubbleMenu';
import SearchBar from 'components/SearchBar';
import Avatar from 'components/Avatar';
import Notifications from 'components/Notifications';
import Spinner from 'components/Spinner';

class Menu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      profileActive: false,
      search: false,
      mobileMenu: false,
      jwt: props.cookies.get('jwt'),
    }
    this.handleMobileMenuActive = this.handleMobileMenuActive.bind(this);
    this.handleMobileMenuInactive = this.handleMobileMenuInactive.bind(this);
    this.handleProfileActive = this.handleProfileActive.bind(this);
    this.handleProfileInactive = this.handleProfileInactive.bind(this);
    this.handleSearchActive = this.handleSearchActive.bind(this);
    this.handleSearchInactive = this.handleSearchInactive.bind(this);
  }
  async componentDidMount() {
    const { getSelfRequest, getBlockedListRequest, me, cookies } = this.props;
    const { jwt } = this.state;

    if (jwt ) {
      await getBlockedListRequest(jwt)
      if (me.size === 0) {
        await getSelfRequest(jwt)
      }
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { profileActive } = this.state;

    if (profileActive !== prevState.profileActive && profileActive) {
      closeonclick(this.handleProfileInactive);
    }
  }

  handleMobileMenuActive() {
    this.setState({ mobileMenu: true,  })
  }
  handleMobileMenuInactive() {
    this.setState({ mobileMenu: false })
  }
  handleProfileActive() {
    this.setState({ profileActive: true })
  }
  handleProfileInactive() {
    this.setState({ profileActive: false })
  }
  handleSearchActive() {
    this.setState({ search: true })
  }
  handleSearchInactive () {
    this.setState({ search: false })
  }
  renderProfile = (profileActive, me) => (
    <Transition
      timeout={150}
      in={profileActive}
      mountOnEnter
      unmountOnExit
    >
      {status => (
        <BubbleMenu classes={`fade-${status} bottom right`}>
          <p>{ AT }{me.get('username')}</p>
          <div className="tip">{me.get('name_display')}</div>
          <div className="hr" />
          <div className="option">
            <Link prefetch route="profile" params={{slug: me.get('username')}}>
              <a><FontAwesomeIcon icon={faUser} /><span>{ MY_PROFILE }</span></a>
            </Link>
          </div>
          <div className="hr" />
          <div className="option">
            <Link route="settings">
              <a><FontAwesomeIcon icon={faCogs} /><span>{ SETTINGS }</span></a>
            </Link>
          </div>
          <div className="hr" />
          <div className="option">
            <Link route="signout">
              <a><FontAwesomeIcon icon={faSignOutAlt} /><span>{ SIGN_OUT }</span></a>
            </Link>
          </div>
        </BubbleMenu>
      )}
    </Transition>
  )
  render() {
    const { loggingIn, obsAccess, me, notifications, gettingNotifications, clearedNotifications } = this.props;
    const { profileActive, mobileMenu, jwt } = this.state;
    return (
      <header>
        <div className="main-menu">
          <div className="header-grid">
            <div className="logo">
              <Link route="/">
                <a><SvgLogo /></a>
              </Link>
            </div>
            <MediaQuery minWidth={768}>
              <div className="search">
                <SearchBar />
              </div>
              {/* TODO: auth check */}
              <nav className="user">
                {
                  me && me.size > 0 && obsAccess ? (
                    <div>
                      <div className="golive">
                        <Link route="golive">
                          <a target="_blank"><FontAwesomeIcon icon={faCameraRetro} /><span>{ GO_LIVE }</span></a>
                        </Link>
                      </div>
                    </div>
                  ) : null
                }
                <div>
                  <div>
                    <Link route="http://blog.peeqr.com/2017/12/08/be-a-partner/">
                      <a target="_blank">{ BE_A_PARTNER }</a>
                    </Link>
                  </div>
                </div>
                <div>
                  <div className="app">
                    <Link route="app">
                      <a target="_blank">{ GET_THE_APP }</a>
                    </Link>
                  </div>
                </div>
                {
                  loggingIn ? (
                    <Spinner color="grey" />
                  ) : (
                    me && me.size > 0 ? (
                      <div>
                        <Notifications 
                          jwt={jwt}
                          notifications={notifications} 
                          gettingNotifications={gettingNotifications} 
                          clearedNotifications={clearedNotifications} 
                        />
                      </div>
                    ) : null
                  )
                }
                {
                  loggingIn ? (
                    <Spinner color="grey" />
                  ) : (
                    !me || me.size === 0 ? (
                      <div>
                        <div>
                          <Link route="signin">
                            <a>{ SIGN_IN_SIGN_UP }</a>
                          </Link>
                        </div>
                      </div>
                    ) : (
                      <div>
                        <div className="user-details">
                          {/* {TODO: get user link} */}
                          <div className="profile-dropdown">
                            <a onClick={this.handleProfileActive}>
                              <Avatar size="medium" username={me.get('name_display')} photo={me.get('photo')} />
                            </a>
                            { this.renderProfile(profileActive, me) }
                          </div>
                        </div>
                      </div>
                    )
                  )
                }
              </nav>
            </MediaQuery>
            <MediaQuery maxWidth={767}>
              <nav className="user">
                <div>
                  <div className="search">
                    <a onClick={this.handleSearchActive}><FontAwesomeIcon icon={faSearch} size="lg" /></a>
                  </div>
                </div>
                {
                  me && me.size > 0 ? (
                    <div>
                      <Notifications 
                        notifications={notifications} 
                        gettingNotifications={gettingNotifications} 
                        clearedNotifications={clearedNotifications} 
                      />
                    </div>
                  ) : null
                }
                {
                  me && me.size > 0 ? (
                    <div>
                      <div className="user-details">
                        {/* {TODO: get user link} */}
                        <div className="profile-dropdown">
                          <a onClick={this.handleProfileActive}>
                            <Avatar size="medium" username={me.get('name_display')} photo={me.get('photo')} />
                          </a>
                          { this.renderProfile(profileActive, me) }
                        </div>
                      </div>
                    </div>
                  ) : null
                }
                <div className="menu-initiate"><a onClick={this.handleMobileMenuActive}><FontAwesomeIcon icon={faBars} size="lg" /></a></div>
              </nav>
              <div className={`mobile-menu ${mobileMenu ? 'active' : ''}`}>
                <div className="close-wrapper">
                  <div className="close"><a onClick={this.handleMobileMenuInactive}><FontAwesomeIcon icon={faTimes} size="lg" /></a></div>
                </div>
                <nav className="user">
                  <ul>
                    {
                      me && me.size > 0 && obsAccess ? (
                        <li className="sign">
                          <Link route="golive">
                            <a target="_blank"><FontAwesomeIcon icon={faCameraRetro} /><span>{ GO_LIVE }</span></a>
                          </Link>
                        </li>
                      ) : null
                    }
                    <li>
                      <Link route="http://blog.peeqr.com/2017/12/08/be-a-partner/">
                        <a target="_blank">{ BE_A_PARTNER }</a>
                      </Link>
                    </li>
                    <li className="app">
                      <a onClick={this.handleAppModalActive} >{ GET_THE_APP }</a>
                    </li>
                    {
                      !me || me.size === 0 ? (
                        <li className="sign">
                          <Link route="signin">
                            <a>{ SIGN_IN_SIGN_UP }</a>
                          </Link>
                        </li>
                      ) : null
                    }
                  </ul>
                </nav>
              </div>
            </MediaQuery>
            {/* TODO: end auth check */}
          </div>
        </div>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  loggingIn: state.getIn(['user', 'loggingIn']),
  me: state.getIn(['user', 'me']),
  obsAccess: state.getIn(['user', 'me', 'obs_access']),
  gettingNotifications: state.getIn(['user', 'gettingNotifications']),
  notifications: state.getIn(['user', 'notifications']),
  clearedNotifications: state.getIn(['user', 'clearedNotifications']),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    getSelfRequest,
    logOutRequest,
    getBlockedListRequest
  },
  dispatch
);

export default WithCustomCookies(connect(mapStateToProps, mapDispatchToProps)(Menu))
