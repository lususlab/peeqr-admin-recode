import React, { Component } from 'react';
import { Link } from "routes";
import * as _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

import customValidators from 'helpers/validation';
import { FORGOT_PASSWORD, CURRENCY } from 'helpers/text';
import { errors } from 'helpers/errors';
import { lowerCase } from 'helpers/lowercase';


class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPassword: false,
      isValid: false,
      fieldType: props.name ? lowerCase(props.name) : '',
      onValidatorExecuted: null,
      showError: false,
      textAreaLength: 0,
    }

    this.handleShowPassword = this.handleShowPassword.bind(this);
    this.handleExecute = this.handleExecute.bind(this);
    this.handleTextarea - this.handleTextarea.bind(this);

    this.setTextInputRef = React.createRef();
  }

  componentDidMount() {
    const { validatorexecutiondelay } = this.props;
    
    this.executeValidators = _.debounce(this.executeValidators, validatorexecutiondelay);
  }

  componentWillUnmount() {
    const blankState = {};
    Object.keys(this.state).forEach(stateKey => {
      blankState[stateKey] = undefined;
    });
    this.setState(blankState);
  }

  handleTextarea(e, maxLength) {
    const element = e.target;
    const length = e.target.value.length;
    this.setState({ textAreaLength: length });
    setTimeout(() => {
      element.style.cssText = 'height:auto; padding:0';
      element.style.cssText = `height:${element.scrollHeight}px`;
    }, 0);
  }

  handleExecute(e) {
    const value = e.target.value;
    this.executeValidators(value);
  }

  executeValidators(value) {
    const {
      validator,
      customValidator,
    } = this.props;
    this.customValidators = customValidators;

    let isValid = this.state.isValid;
    const validatorRegexp = new RegExp(this.customValidators[validator] || validator);
    const fieldType = this.state.fieldType;

    this.setState({
      onValidatorExecuted: true,
      showError: false,
    });
    switch (this.state.onValidatorExecuted) {
      case true:

        if ((value.length === 0 || !value) && (validator || customValidator)) {
          isValid = true;
          this.setState({
            onValidatorExecuted: false,
            showError: false,
          });
        } else if (!validator && !customValidator) {
          isValid = true;
          this.setState({
            showError: false,
          });
        } else if (validator in this.customValidators) {
          isValid = validatorRegexp.test(value);
          this.props.isValid(isValid, fieldType);
          
          if (isValid === false) {
            this.setState({
              showError: true,
            });
            this.displayError();
          }
        } else if (validator instanceof String || typeof validator === 'string') {
          isValid = validatorRegexp.test(value);
        }
        
        if (customValidator) {
          console.log('customValidator Triggered');
          if (value === customValidator) {
            isValid = true;
          }
          console.log(isValid);
          this.props.isValid(isValid, fieldType);
          
          if (isValid === false) {
            this.setState({
              showError: true,
            });
            this.displayError();
          }
        }
        return isValid;
      default:
        return null;
    }
  }

  displayError() {
    if (this.state.showError) {
      const filteredErrors = errors.find(error => error.type === this.props.validator);
      const errorMessage = filteredErrors.errorMessage;
      return (<div className="tip">{errorMessage}</div>);    
    }
  }

  handleShowPassword() {
    const { showPassword } = this.state;
    
    if (showPassword) {
      this.setState({ showPassword: false });
      this.setTextInputRef.current.type = 'password';
    } else {
      this.setState({ showPassword: true });
      this.setTextInputRef.current.type = 'text';
    }
  }

  render() {
    const { ...props } = this.props;
    const { showPassword, textAreaLength } = this.state;
    const lower = props.name ? lowerCase(props.name) : '';

    return (
      <div className={`${(props.theme)} input-group ${(props.forgot) ? 'has-forgot' : ''}`}>
        {
          (props.theme === 'boxed') ? (
            <label htmlFor={lower}>{props.label}</label>
          ) : null
        }
        {
          (props.forgot) ? (
            <span className="forgot">
              { /* TODO: navigate to forgot page */ }
              <Link route="forgot">
                <a>{ FORGOT_PASSWORD }</a>
              </Link>
            </span>
          ) : null
        }
        <span className={`field ${props.type === 'number' ? 'price' : ''}`}>
          {
            (props.type === 'radio') ? (
              <input 
                ref={this.setTextInputRef}
                type={props.type}
                autoComplete={`new-${lower}`}
                name={lower}
                id={lower}
                value={props.value}
                onChange={e => {
                  this.props.onChange(e);
                }}
                checked={props.checked}
                className={props.classes}
              />
            ) : (props.type === 'textarea') ? (
              <textarea
                ref={this.setTextInputRef}
                type={props.type}
                validatorexecutiondelay={props.validator ? 100 : null}
                autoComplete={`new-${lower}`}
                name={lower}
                id={lower}
                value={props.value}
                placeholder={props.placeholder}
                maxLength={props.maxLength}
                defaultValue={props.defaultValue}
                onChange={e => {
                  this.props.onChange(e);
                  this.props.validator ? this.handleExecute(e) : null;
                  this.props.maxLength ? this.handleTextarea(e, props.maxlength) : null;          
                }}
                onKeyPress={e => {
                  this.props.onKeyPress ? this.props.onKeyPress(e) : null
                }}
                onFocus={props.onFocus}
                required={props.required}
                onBlur={this.handleExecute}
                rows={props.rows}
                readOnly={props.readOnly}
                className={props.classes}
                disabled={props.disabled}
              />
            ) : (props.type === 'select') ? (
              <select
                ref={this.setTextInputRef}
                type={props.type}
                validatorexecutiondelay={props.validator ? 100 : null}
                autoComplete={`new-${lower}`}
                name={lower}
                id={lower}
                value={props.value}
                defaultValue={props.defaultValue}
                onChange={e => {
                  this.props.onChange(e);
                }}
                required={props.required}
                className={props.classes}
              >
                {props.children}
              </select>
            ) : (
              <input 
                ref={this.setTextInputRef}
                type={props.type}
                validatorexecutiondelay={props.validator ? 100 : null}
                autoComplete={`new-${lower}`}
                name={lower}
                id={lower}
                value={props.value}
                placeholder={props.placeholder}
                maxLength={props.maxLength}
                defaultValue={props.defaultValue}
                onChange={e => {
                  this.props.onChange(e);
                  this.props.validator ? this.handleExecute(e) : null;
                }}
                onFocus={props.onFocus}
                required={props.required}
                onBlur={(e) => {
                  this.props.validator ? this.handleExecute(e) : null;
                  this.props.onBlur ? this.props.onBlur() : null
                }}
                onKeyPress={props.onKeyPress}
                onKeyUp={props.onKeyUp}
                disabled={props.disabled}
                min={props.min}
                readOnly={props.readOnly}
                className={props.classes}
              />
            )
          }
          
          {
            (props.showPassword) ? (
              <a className="show-password" onClick={this.handleShowPassword}>
                {
                  (showPassword) ? (
                    <FontAwesomeIcon icon={faEyeSlash} size="xs" />
                  ) : (
                    <FontAwesomeIcon icon={faEye} size="xs" />
                  )
                }
              </a>
            ) : null
          }
          {
            (props.theme === 'line' || props.theme === 'checkbox' || props.theme === 'radio') ? (
              <label htmlFor={lower}>
                {props.label}
                {
                  (props.linkLabel && props.route) ? (
                    <Link route={props.route}>
                      <a target="_blank">{props.linkLabel}</a>
                    </Link>
                  ) : null
                }
              </label>
            ) : null
          }
          {
            (props.type === 'number') ? (
              <span className="currency">{ CURRENCY }</span>
            ) : null
          }
          {
            (props.maxLength) ? (
              <span className="length-counter">{ textAreaLength } of { props.maxLength }</span>
            ) : null
          }
          {
            (props.type === "select") ? (
              <span className="dropdown-icon"><FontAwesomeIcon icon={faChevronDown} size="xs" /></span>
            ) : null
          }
        </span>
        {this.displayError()}
      </div>
    )
  }
}

export default Input;