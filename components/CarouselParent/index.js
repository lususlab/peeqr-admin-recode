import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { Link } from "routes";

import Carousel from '../Carousel';

class CarouselParent extends Component {
  constructor() {
    super();

    this.state = {
    }
  }

  render() {
    const { banners } = this.props;
    return (
      <section id="banners">
        <Carousel>
          {banners.get('results').map((banner) => (
            <div className="banner" order={banner.get('order')} key={banner.get('id')}>
              <Link route={banner.get('link_url')}>
                <a rel="noopener noreferrer" target="_blank">
                  <img alt={banner.get('id')} src={banner.get('banner_url')} />
                </a>
              </Link>
            </div>
          ))}
        </Carousel>
      </section>
    )
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(CarouselParent)
