import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import anchorme from "anchorme";
import renderHTML from 'react-render-html';
import reactStringReplace from 'react-string-replace';
import { fromJS } from 'immutable';
import { Link } from 'routes';

import Pluralize from 'react-pluralize';

import Avatar from "components/Avatar";
import Button from "components/Button";
import Input from "components/Input";
import TimeAgo from "components/TimeAgo";
import LoadMore from "components/LoadMore";
import Spinner from "components/Spinner";
import CommentsMuteBlockButton from "components/CommentsMuteBlockButton"

import {
  loadMoreCommentsRequest,
  postCommentRequest,
  showModal
} from "store/video/actions"

import {
  COMMENT,
  SIGN_IN_TO_COMMENT,
  SIGN_IN_TO_PEEQR,
  WARNING,
  BE_THE_FIRST,
  LIVE_COMMENT,
  GUIDELINES
} from 'helpers/text';

import {
  NO_ACCESS
} from "helpers/errors";

import { videoCurrentTime } from 'helpers/videohelper';
import { createNotification } from 'helpers/createnotification';

class Comments extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userComment: '',
      socketComments: fromJS([]),
      moreActive: false,
    }

    this.input = React.createRef();
    this.onCommentChange = this.onCommentChange.bind(this);
    this.handlePostComment = this.handlePostComment.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleTextareaFocus = this.handleTextareaFocus.bind(this);
    this.renderComment = this.renderComment.bind(this);
    this.handleSocketComment = this.handleSocketComment.bind(this);
    this.handleShowModalActive = this.handleShowModalActive.bind(this);
  }

  componentDidMount() {
    const { socket } = this.props;

    socket.on('comment', data => {
      this.handleSocketComment(data);
    });
  }

  handleSocketComment(data) {
    const { socketComments } = this.state;
    if (data.cmd === "comment") {
      const updatedComments = socketComments.toJS();
      updatedComments.unshift(data.data.datatrans);
      this.setState({ socketComments: fromJS(updatedComments) });
    }
  }

  handleShowModalActive = () => {
    const { showModal } = this.props;
    showModal('', 'guidelines')
  }

  onCommentChange(e) {
    this.setState({ userComment: e.target.value });
  }
  renderComment(item) {
    const {jwt, me, video} = this.props;
    return (
      item.size > 0 ? (
        <div className="comment-item" id={`comment-${item.get('id')}`} key={item.get('id')}>
          <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
            <a target="_blank" title={item.getIn(['user', 'name_display'])}>
              <Avatar size="medium" username={item.getIn(['user', 'name_display'])} photo={item.getIn(['user', 'photo'])} />
            </a>
          </Link>
          <div className="comment-meta">
            <div className="user">
              <Link route="profile" params={{slug: item.getIn(['user', 'username'])}}>
                <a target="_blank" title={item.getIn(['user', 'username'])}>
                  {item.getIn(['user', 'name_display'])}
                </a>
              </Link>
              <TimeAgo date={item.get('created_date')} />
              {
                (item.get('comment_on ') === 'l') ? (
                  <span className='live-comment'>{LIVE_COMMENT}</span>
                ) : null
              }
            </div>
            <div className="comment">
              {renderHTML(anchorme(item.get('text'), {attributes:[{name: "target", value: "_blank"}]}))}
            </div>
          </div>
          {
            me.size > 0 ? (
              <CommentsMuteBlockButton 
                position="top right"
                jwt={jwt}
                videoId={video.get('id')}
                userId={item.getIn(['user', 'id'])}
                username={item.getIn(['user', 'username'])}
                nameDisplay={item.getIn(['user', 'name_display'])}
              />
            ) : null
          }
        </div>
      ) : null
    )
  }
  handlePostComment(e) {
    e.preventDefault();
    const { video, postCommentRequest, playerState, jwt, me, hasAccess, isShow } = this.props;
    const { userComment } = this.state;
    const timestamp = videoCurrentTime(playerState, video.get('merge_status'))

    if (!isShow || hasAccess && isShow) {
      if (me.size > 0) {
        postCommentRequest(video.get('id'), userComment, timestamp, jwt);
      } else {
        createNotification('error', SIGN_IN_TO_COMMENT)
      }
    } else {
      createNotification('error', NO_ACCESS)
    }
    this.setState({userComment: ''})
    this.handleTextareaFocus();
  }
  handleKeyPress(e) {
    const { userComment } = this.state;
    if (userComment !== '' && e.key == 'Enter' && !e.shiftKey) {
      this.handlePostComment(e)
    } else if (userComment === '' && e.key == 'Enter' && !e.shiftKey) {
      e.preventDefault();
    }
  }
  handleTextareaFocus() {
    this.input.current.setTextInputRef.current.focus();
  }
  handleLoadMore = (e) => {
    e.preventDefault();
    const { comments, loadMoreCommentsRequest } = this.props;

    loadMoreCommentsRequest(comments.get('next'))
  }

  render() {
    const { comments, me, loggingIn, loadingMoreComments, orientation, commenting } = this.props;
    const { userComment, socketComments } = this.state;
    return (
      <div className={`comments ${orientation}`}>
        <div className="tip"><Pluralize singular={COMMENT} count={comments.get('count') + socketComments.size} /></div>
        {
          !loggingIn ? (
            <div className="comment-box">
              <Avatar size="medium" username={me.size > 0 ? me.get('name_display') : SIGN_IN_TO_PEEQR} photo={me.size > 0 ? me.get('photo') : ""} />
              <Input 
                ref={this.input}
                type="textarea"
                theme="line"
                placeholder={me.size > 0 ? `${COMMENT}...` : SIGN_IN_TO_COMMENT}
                name={me.size > 0 ? COMMENT : SIGN_IN_TO_COMMENT}
                value={userComment}
                onChange={this.onCommentChange}
                disabled={me.size === 0}
                rows="1"
                maxLength="500"
                onKeyPress={this.handleKeyPress}
              />
              <div className="interact">
                <span className="warning">
                  {reactStringReplace(WARNING, GUIDELINES, (match, i) => (
                    <a key={i} onClick={this.handleShowModalActive}>{match}</a>
                  ))}
                </span>
                {
                  me.size > 0 ? (
                    commenting ? (
                      <Button color="white" background="bggrey" size="small" >
                        <a className="disabled"><Spinner color="white" /></a>
                      </Button>
                    ) : (
                      <Button color="white" background="bglightpurple" size="small">
                        <a onClick={this.handlePostComment}><span>{COMMENT}</span></a>
                      </Button>
                    )
                  ) : (
                    <Button color="white" background="bggrey" size="small" >
                      <a className="disabled"><span>{ COMMENT }</span></a>
                    </Button>
                  )
                }
              </div>
            </div>
          ) : null
        }
        <div className="comments-collection">
          {
            socketComments.size > 0 ? (
              socketComments.map((item) => this.renderComment(item))
            ) : null
          }
          {
            comments.get('count') > 0 ? (
              comments.get('results').map((item) => this.renderComment(item))
            ) : null
          }
          {
            (socketComments.size === 0 && comments.get('count') === 0) ? (
              <div className="centered no-comments">{BE_THE_FIRST}</div>
            ) : null
          }
          {
            comments.get('next') ? (
              <div className="centered">
                <LoadMore size="small" color="white" background="bggrey" onClick={this.handleLoadMore} loading={loadingMoreComments} />
              </div>
            ) : null
          }
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  loggingIn: state.getIn(['user', 'loggingIn']),
  loadingMoreComments: state.getIn(['video', 'loadingMoreComments']),
  me: state.getIn(['user', 'me']),
  commenting: state.getIn(['video', 'commenting'])
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
  {
    loadMoreCommentsRequest,
    postCommentRequest,
    showModal,
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Comments)
