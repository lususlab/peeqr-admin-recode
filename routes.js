const routes = require("next-routes")();

module.exports = routes;

routes
.add("video", "/video/:id")
.add("search", "/search?t=:type&q=:search")
.add("profile", "/profile/:slug")
.add("signin", "/signin")
.add("signout", "/signout")
.add("signup", "/signup")
.add("error", "/error")
.add("notifications", "/notifications")
.add("confirm", "/confirm/:key")
